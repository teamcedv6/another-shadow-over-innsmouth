# Another shadow over Innsmouth - CEDV

# Autores:
# Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
# Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
# David Delgado Lizcano					daviddelgadolizcano@gmail.com

# You can redistribute and/or modify this file under the terms of the
# GNU General Public License ad published by the Free Software
# Foundation, either version 3 of the License, or (at your option)
# and later version. See <http://www.gnu.org/licenses/>.

# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

EXEC := another-shadow-over-innsmouth
MODULES := Managers/ States/ Tools/ Tools/NoesisBindings/ Tools/TinyXml/ Tools/DotScene/ EntityComponentSystem/ EntityComponentSystem/Components/ EntityComponentSystem/Systems/ Events/Actions/ Events/Actions/Actions/ Events/Actions/SubActions/ Events/Triggers/ GameLogic/ Sound/ Conversation/ Loaders/ Events/  Tools/OgreCrowd/ Tools/OgreCrowd/Detour/ Tools/OgreCrowd/DetourCrowd/ Tools/OgreCrowd/DetourTileCache/ Tools/OgreCrowd/Recast/ Tools/OgreCrowd/RecastContrib/fastlz/

DIRSRC := src/ $(addprefix src/,$(MODULES))
DIROBJ := obj/ $(addprefix obj/,$(MODULES))
DIRHEA := include/ $(addprefix include/,$(MODULES))

CXX := g++

# Flags de compilación -----------------------------------------------
CXXFLAGS := $(DEFINETAGS) $(addprefix -I,$(DIRHEA)) -Wall -std=c++11 -fopenmp -D_GLIBCXX_PARALLEL -pthread  `pkg-config --cflags OGRE OIS OgreBullet bullet OGRE-Overlay noesis` 

# Flags del linker ---------------------------------------------------
LDFLAGS := `pkg-config --libs-only-L OGRE noesis`
LDLIBS := `pkg-config --libs-only-l gl OGRE OIS OgreBullet bullet OGRE-Overlay noesis SDL2_mixer` -lstdc++ -lboost_system -lpthread -lgomp -lOgreTerrain

# Modo de compilación (-mode=release -mode=debug) --------------------
ifeq ($(mode), release) 
	CXXFLAGS += -O2 -D_RELEASE
else 
	CXXFLAGS += -g -D_DEBUG
	mode := debug
endif

# Obtención automática de la lista de objetos a compilar -------------
OBJS := $(foreach dir,$(DIRSRC), \
	$(subst src/, obj/, \
	$(patsubst %.cpp, %.o, $(wildcard $(dir)*.cpp))))

# Función para compilar en rutas genéricas ---------------------------
define make-target
obj/%.o : $(subst obj/, src/, $(1))%.cpp	
	$$(CXX) $$(CXXFLAGS) -c $$< -o $$@ 
endef

.PHONY: all clean

all: info dirs $(EXEC)

info:
	@echo '------------------------------------------------------'
	@echo '>>> Using mode $(mode)'
	@echo '    (Please, call "make" with [mode=debug|release])  '
	@echo '------------------------------------------------------'

dirs:
	mkdir -p $(DIROBJ)

# Enlazado -----------------------------------------------------------
$(EXEC): $(OBJS)
	$(CXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

# Crear targets para los distintos .obj gracias a la función ---------
# make-target previamente definida -----------------------------------
$(foreach dir,$(DIROBJ), $(eval $(call make-target,$(dir))))

# Limpieza de temporales ---------------------------------------------
clean:
	rm -f *.log $(EXEC) *~ $(addsuffix *~,$(DIRSRC)) $(addsuffix *~, $(DIRHEA))
	rm -rf $(DIROBJ)
