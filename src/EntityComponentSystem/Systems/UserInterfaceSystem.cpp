#include "UserInterfaceSystem.h"

UserInterfaceSystem::UserInterfaceSystem() {}

UserInterfaceSystem::UserInterfaceSystem(std::shared_ptr<Ogre::RenderWindow> window) : _window(window) {
	_time = 0;
}

UserInterfaceSystem::UserInterfaceSystem(const UserInterfaceSystem & userInterfaceSystem) {}

UserInterfaceSystem::UserInterfaceSystem(const UserInterfaceSystem && userInterfaceSystem) {}

UserInterfaceSystem::~UserInterfaceSystem() {}

UserInterfaceSystem & UserInterfaceSystem::operator= (const UserInterfaceSystem & userInterfaceSystem) { return *this;}

void UserInterfaceSystem::initializeUI() {
	Noesis_Init();
}

void UserInterfaceSystem::terminateUI() {
    Noesis_Shutdown();
}

void UserInterfaceSystem::loadUI(const std::string & name, const std::string & xaml) {
	_uiMap[name] = std::make_shared<uiPart>();
    Noesis_LoadXAML(&_uiMap[name]->root, &_uiMap[name]->renderer, xaml.c_str());
}


/*void UserInterfaceSystem::test(Noesis::BaseComponent * sender, const Noesis::MouseEventArgs & args) {
	auto ui = _uiMap.find("MainMenu");
	std::cout << ((Noesis::UserControl*)sender)->GetName() << "\n";
	auto ani = ((Noesis::UserControl*)ui->second->root.GetPtr())->FindName("new_focus");
	((Noesis::Storyboard *) ani)->Begin();
}*/

void UserInterfaceSystem::loadUI(const Entity::ID id) {}

void UserInterfaceSystem::unloadUI(const std::string & name) {
	auto it = _uiMap.find(name);

	if(it != _uiMap.end()) {
		Noesis::Ptr<Noesis::IRenderer> renderer = _uiMap[name]->renderer;
		_uiMap.erase(it);
		Noesis_UnloadXAML(renderer);
	}
}

void UserInterfaceSystem::unloadUI(const Entity::ID id) {}

void UserInterfaceSystem::update(Ogre::Real deltaTime) {
	for(auto it = _uiMap.begin(); it != _uiMap.end(); it++) {
		Noesis_GPURenderOffscreen(it->second->renderer);
	}

	Noesis_Tick();
	int width = _window->getWidth();
	int height = _window->getHeight();

	_time += deltaTime;

	for(auto it = _uiMap.begin(); it != _uiMap.end(); it++) {
		Noesis_Update(it->second->renderer, _time, width, height);
	}
}

void UserInterfaceSystem::frameRenderingQueued() {

	for(auto it = _uiMap.begin(); it != _uiMap.end(); it++) {
		Noesis_GPURender(it->second->renderer);
	}
}

std::shared_ptr<UserInterfaceSystem::uiPart> UserInterfaceSystem::getUI(const std::string & name) {
	return _uiMap[name];
}

bool UserInterfaceSystem::keyPressed (const OIS::KeyEvent &e) {
	for(auto it = _uiMap.begin(); it != _uiMap.end(); it++) {
		Noesis_KeyDown(it->second->renderer, e.key);
    	Noesis_Char(it->second->renderer, e.text);
	}
}

bool UserInterfaceSystem::keyReleased (const OIS::KeyEvent &e) {
	for(auto it = _uiMap.begin(); it != _uiMap.end(); it++) {
		Noesis_KeyUp(it->second->renderer, e.key);
	}
}

bool UserInterfaceSystem::mouseMoved (const OIS::MouseEvent &e) {
	for(auto it = _uiMap.begin(); it != _uiMap.end(); it++) {
		Noesis_MouseMove(it->second->renderer, (float)e.state.X.abs, (float)e.state.Y.abs);
		Noesis_MouseWheel(it->second->renderer, (float)e.state.X.abs, (float)e.state.Y.abs, e.state.Z.abs);

		if(_mouse != nullptr) {
			_mouse->SetMargin(Noesis::Thickness((float)e.state.X.abs,(float)e.state.Y.abs,(float)0,(float)0));
		}
	}
}

bool UserInterfaceSystem::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
	for(auto it = _uiMap.begin(); it != _uiMap.end(); it++) {
		Noesis_MouseButtonDown(it->second->renderer, (float)e.state.X.abs, (float)e.state.Y.abs, id);
	}
}

bool UserInterfaceSystem::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
	for(auto it = _uiMap.begin(); it != _uiMap.end(); it++) {
		Noesis_MouseButtonUp(it->second->renderer, (float)e.state.X.abs, (float)e.state.Y.abs, id);
	}
}

void UserInterfaceSystem::activateCursor() {
	if(_mouse == nullptr) {
		loadUI("cursor", "Cursor.xaml");
		_mouse = (Noesis::UserControl *) getUI("cursor")->root.GetPtr();
	}
}

void UserInterfaceSystem::deactivateCursor() {
	if(_mouse != nullptr) {
		_mouse = nullptr;
		unloadUI("cursor");
	}
}
