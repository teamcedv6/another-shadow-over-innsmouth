#include "MovementSystem.h"

MovementSystem::MovementSystem() : System(){}

MovementSystem::MovementSystem(const MovementSystem & movementSystem) : System(movementSystem), _physics(movementSystem._physics){}

MovementSystem::MovementSystem(const MovementSystem && movementSystem) : System(movementSystem), _physics(movementSystem._physics) {}

MovementSystem::~MovementSystem() {}

MovementSystem & MovementSystem::operator= (const MovementSystem & movementSystem) { System::operator =(movementSystem); return *this;}

void MovementSystem::setPhysicsSystem(std::shared_ptr<PhysicSystem> physics) {
	_physics = physics;
}

void MovementSystem::setPosition(const Entity::ID id, const std::string & name, const Ogre::Vector3 & position) {
	if(_componentMap[id]->_movableParts[name]._physicalMovement) {
		_physics->updateObject(id, position);
	}
	else {
		_componentMap[id]->_movableParts[name]._node->setPosition(position);
	}
}

void MovementSystem::move(const Entity::ID id, const std::string & name, const Ogre::Vector3 & move) {
	if(_componentMap[id]->_movableParts[name]._physicalMovement) {
		_physics->updateObject(id, _componentMap[id]->_movableParts[name]._node->getPosition() + move);
	}
	else {
	 _componentMap[id]->_movableParts[name]._position += move;
	}
}

void MovementSystem::setOrientation(const Entity::ID id, const std::string & name, const Ogre::Quaternion & orientation) {
	_componentMap[id]->_movableParts[name]._node->setOrientation(orientation);
}

void MovementSystem::update(Ogre::Real deltaTime) {
	/*auto zeroMove = Ogre::Vector3(0, 0, 0);
	for(auto it = _componentMap.begin();   it != _componentMap.end(); it++) {
		for(auto part = it->second->_movableParts.begin(); part != it->second->_movableParts.end(); part++) {
			if(part->second._physicalMovement) {
				if ( part->second._position != zeroMove){
					_physics->updateObject(it->first, part->second._position);
				} else {
					_physics->activeObject(it->first);
				}
			}
			else {
				part->second._node->translate(part->second._position * deltaTime);
				//part->second._node->translate(0.02, 0, 0);//setPosition(0, 0, 0);
				//std::cout << "Name: " <<  part->second._node->getName() << " Pos: " <<part->second._node->getPosition() << "\n";
			}
			part->second._position = zeroMove;
		}
	}*/
}
