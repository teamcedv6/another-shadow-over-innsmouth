#include "PhysicSystem.h"

#include "bullet/btBulletCollisionCommon.h"
#include "bullet/btBulletDynamicsCommon.h"
#include <iostream>
#include <string>
#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include "OgreBulletCollisionsRay.h"

#include "Obj.h"


PhysicSystem::PhysicSystem() : System(){
}

PhysicSystem::PhysicSystem(	std::shared_ptr<Ogre::SceneManager> sceneManager,
							std::shared_ptr<IdNameMapper> mapper,
							std::shared_ptr<Configuration> config,
							std::shared_ptr<ActionsManager> actions,
							std::shared_ptr<SoundManager> soundManager,
							std::shared_ptr<NodeManager> nodes, std::shared_ptr<GraphicSystem> graphic, shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> entities) :
		System(), _sceneManager(sceneManager), _mapper(mapper), _config(config), _actions(actions), _soundManager(soundManager),_nodes(nodes),_graphic(graphic),_entities(entities) {
		_createPhysicWorld();
}

PhysicSystem::PhysicSystem(const PhysicSystem & physicSystem) : System(physicSystem){
	_sceneManager = physicSystem._sceneManager;
	_mapper = physicSystem._mapper;
	_config = physicSystem._config;
	_actions = physicSystem._actions;
	_soundManager = physicSystem._soundManager;
}

PhysicSystem::PhysicSystem(const PhysicSystem && physicSystem) : System(physicSystem) {
	_sceneManager = physicSystem._sceneManager;
	_mapper = physicSystem._mapper;
	_config = physicSystem._config;
	_actions = physicSystem._actions;
	_soundManager = physicSystem._soundManager;
}

PhysicSystem::~PhysicSystem() {
	//_destroyPhysicWorld();
}

PhysicSystem & PhysicSystem::operator= (const PhysicSystem & physicSystem) {

	_sceneManager = physicSystem._sceneManager;
	_mapper = physicSystem._mapper;
	_config = physicSystem._config;
	_actions = physicSystem._actions;
	_soundManager = physicSystem._soundManager;

	return *this;
}

void PhysicSystem::_createPhysicWorld() {

	float wB = _config->getPhysicConfig()->worldBounds;
	float gravity = _config->getPhysicConfig()->gravity;
	bool debug = _config->getPhysicConfig()->debug;

	Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
		Ogre::Vector3 (-wB, -wB, -wB),
		Ogre::Vector3 (wB,  wB,  wB)
	);

	Ogre::Vector3 vGravity = Ogre::Vector3(0, gravity, 0);
	std::shared_ptr<OgreBulletDynamics::DynamicsWorld> smartPhysicWorld(new OgreBulletDynamics::DynamicsWorld(_sceneManager.get(),worldBounds,vGravity));
	_physicWorld = smartPhysicWorld;

	if(debug)_activeDebug();
}

void PhysicSystem::_destroyPhysicWorld() {

}

void PhysicSystem::_activeDebug(){
	std::shared_ptr<OgreBulletCollisions::DebugDrawer> smartDebugDrawer(new OgreBulletCollisions::DebugDrawer());
	_debugDrawer = smartDebugDrawer;
	_debugDrawer->setDrawWireframe(true);
	std::shared_ptr<Ogre::SceneNode> smartNode(_sceneManager->getRootSceneNode()->createChildSceneNode("debugNode", Ogre::Vector3::ZERO));
	_debugNode = smartNode;
	_debugNode->attachObject(static_cast <Ogre::SimpleRenderable *>(_debugDrawer.get()));
	_debugDrawer->setDebugMode(btIDebugDraw::DBG_DrawWireframe);
	_physicWorld->setDebugDrawer (_debugDrawer.get());
	_physicWorld->setShowDebugShapes (true);  // Muestra los collision shapes
}

void PhysicSystem::addPhysicObject(const Entity::ID id,ShapeForm shapeForm,ShapeType shapeType, PhysicComponent::PhysicProperties objProperties) {


	const std::string & name = _mapper->idToName(id);
	const std::string & shapeId = objProperties.shapeId;

	auto findShape = _collisionShapes.count(shapeId);

	if (findShape == 0){
		switch (shapeForm) {
			case ShapeForm::BOUNDING_BOX:
					_newBoundingBoxObject(name,shapeId);
				break;
			case ShapeForm::MESH:
					_newMesh2ShapeObject(name,shapeId);
				break;
			case ShapeForm::PLANE:
					_newStaticPlaneObject(shapeId);
				break;
			default:
				break;
		}
	}

	assert( _collisionShapes[shapeId] );

	auto objShape =_collisionShapes[shapeId];
	//std::cout << "#######SHAPES######" << std::endl;
	//std::cout << _collisionShapes.size() << std::endl;
	//std::cout << "###################" << std::endl;
	std::shared_ptr<OgreBulletDynamics::RigidBody> rigidBody(new OgreBulletDynamics::RigidBody(name, _physicWorld.get()));
	auto parentNode = _sceneManager->getEntity(name)->getParentSceneNode();
	auto pos = _sceneManager->getEntity(name)->getParentSceneNode()->getPosition();
	auto orientation = _sceneManager->getEntity(name)->getParentSceneNode()->getOrientation();

	auto bRestitution = objProperties.bodyRestitution;
	auto bFriction = objProperties.bodyFriction;
	auto bMass = objProperties.bodyMass;

	/*
	 * s -> Stastic shape
	 * d -> Dynamic
	 */
	switch (shapeType) {
		case ShapeType::STATIC:
				rigidBody->setStaticShape(objShape.get(), bRestitution, bFriction,pos, orientation);
			break;
		case ShapeType::DYNAMIC:
				rigidBody->setShape(parentNode, objShape.get(), bRestitution, bFriction, bMass, pos, orientation);
				break;
		default:
			break;
	}

	// Anadimos los objetos a las deques
	_componentMap[id]->_rigidBody = rigidBody;
}

void PhysicSystem::_newBoundingBoxObject(const std::string name, const std::string shapeId) {
	Ogre::Vector3 size = Ogre::Vector3::ZERO;
	Ogre::AxisAlignedBox boundingB = _sceneManager->getEntity(name)->getBoundingBox();
	size = boundingB.getSize();
	size /= 2.0f;   // El tamano en Bullet se indica desde el centro
	std::shared_ptr<OgreBulletCollisions::CollisionShape> smartBodyShape(new OgreBulletCollisions::BoxCollisionShape(size));
	_collisionShapes[shapeId] = smartBodyShape;
}
void PhysicSystem::_newMesh2ShapeObject(const std::string name, const std::string shapeId) {
	OgreBulletCollisions::StaticMeshToShapeConverter* trimeshConverter = NULL;
	trimeshConverter = new OgreBulletCollisions::StaticMeshToShapeConverter(_sceneManager->getEntity(name));
	std::shared_ptr<OgreBulletCollisions::CollisionShape> smartBodyShape(trimeshConverter->createConvex());
	_collisionShapes[shapeId] = smartBodyShape;
	delete trimeshConverter;
}
void PhysicSystem::_newStaticPlaneObject(const std::string shapeId) {
	std::shared_ptr<OgreBulletCollisions::CollisionShape> smartPlaneShape(new OgreBulletCollisions::StaticPlaneCollisionShape
	(Ogre::Vector3(0,1,0), 0));
	_collisionShapes[shapeId] = smartPlaneShape;
}

bool PhysicSystem::_checkEnvironmentCollission(const std::string & name1, const std::string & name2) {

	std::string other;

/*if ( (obA->isStaticObject() && (obOB_A->getName().substr(0,6) != "canon_")) || (obB->isStaticObject() && (obOB_B->getName().substr(0,6) != "canon_"))  ) {
				std::string name;
				if(obA->isStaticObject()) {
					name = obOB_B->getName();
					//delete obOB_A;
				} else {
					name = obOB_A->getName();
					//delete obOB_B;
				}*/
	if(name1 == "terrain") {
		other = name2;
	}
	else if(name2 == "terrain")
	{
		other = name1;
	}
	else return false;

	bool isFood = false;

	//auto it = _config->getFoodList()->begin();

	/*while(!isFood && it != _config->getFoodList()->end()) {
		const std::string & foodName = it->second->name;
		const int foodNameLenght = foodName.length();

		if(other.length() >= foodNameLenght) std::cout << other.substr(0, foodNameLenght) <<  " == "  << foodName << "\n";
		if(other.length() >= foodNameLenght && other.substr(0, foodNameLenght) == foodName) isFood = true;

		it++;
	}*/

	// If is one piece of food, you've just lost a life
	if(isFood) {
		std::shared_ptr<Action> foodHitEnvironment = std::make_shared<Action>();
		//std::shared_ptr<ChangeLifes> changeLifes = std::make_shared<ChangeLifes>(-1, true);
		//foodHitEnvironment->addSubaction(changeLifes);
		_actions->addAction(foodHitEnvironment);
		_soundManager->selectResource(SoundManager::FX,"hit",SoundManager::PLAY,80,false);
	}

	Obj::Options_t options;
	options.addNode = true;
	options.attachNode = true;
	options.addGraphic = true;
	options.addPhysic = true;
	options.typeAct = Obj::DELETE;
	options.entityName = other;

	std::make_shared<Obj>(_config, _nodes,_graphic, this,_actions,  _entities, options);

	//std::cout << "Colision between " << name1 << " and " << name2 << " \n";

	return true;
}

bool PhysicSystem::_checkBulletCollission(const std::string & name1, const std::string & name2) {
	std::string other;
	std::string bullet;
	bool deleteOther = false;

	if(name1.length() >=  4 && name1.substr(0, 4) == "Ammo") {
		bullet = name1;
		other = name2;
	}
	else if(name2.length() >= 4 && name2.substr(0, 4) == "Ammo")
	{
		other = name1;
		bullet = name2;
	}
	else return false;

	//auto it = _config->getFoodList()->begin();

	std::shared_ptr<Configuration::Item_t> item = nullptr;

	/*while(item == nullptr && it != _config->getFoodList()->end()) {
		const std::string & foodName = it->second->name;
		const int foodNameLenght = foodName.length();

		std::cout << "FOOD : : : " << foodName << "\n";
		if(other.length()>= foodNameLenght) {
			std::cout << other.substr(0, foodNameLenght) << " = " << foodName << "\n";
		}

		if(other.length() >= foodNameLenght && other.substr(0, foodNameLenght) == foodName) { item = it->second;  deleteOther = true; }
		it++;

	}*/


	// Is food
	if(item != nullptr) {
		// Add points
		std::shared_ptr<Action> bulletHitFood = std::make_shared<Action>();
		//std::shared_ptr<ChangeScore> changeScore = std::make_shared<ChangeScore>(item->pts, true);
		//bulletHitFood->addSubaction(changeScore);
		_soundManager->selectResource(SoundManager::FX,"impact",SoundManager::PLAY,80,false);
		_actions->addAction(bulletHitFood);
	}
	else {

		//auto it2 = _config->getPowerUpList()->begin();

		/*while(item == nullptr && it2 != _config->getPowerUpList()->end()) {
			const std::string & powerUpName = it2->second->name;
			const int powerUpNameLenght = powerUpName.length();

			if(other.length() >= powerUpNameLenght && other.substr(0, powerUpNameLenght) == powerUpName) { item = it2->second; deleteOther = true; }

			it2++;
		}*/

		// Is power up
		if(item != nullptr) {
			// Take power up effect

			std::shared_ptr<Action> bulletHitPowerUp = std::make_shared<Action>();

			/*if(other.length() > 6 && other.substr(0, 6) == "Bullet") {
				std::shared_ptr<ChangeBullets> changeBullets = std::make_shared<ChangeBullets>(item->value, true);
				bulletHitPowerUp->addSubaction(changeBullets);
			}
			else if(other.length() > 5 && other.substr(0, 5) == "Heart") {
				std::shared_ptr<ChangeLifes> changeLifes = std::make_shared<ChangeLifes>(item->value, true);
				bulletHitPowerUp->addSubaction(changeLifes);
			}
			else if(other.length() > 4 && other.substr(0, 4) == "Bolt") {
				std::shared_ptr<ChangeSpeed> changeSpeed = std::make_shared<ChangeSpeed>(item->value);
				bulletHitPowerUp->addSubaction(changeSpeed);
			}*/

			_soundManager->selectResource(SoundManager::FX,"powerup",SoundManager::PLAY,80,false);
			_soundManager->selectResource(SoundManager::FX,"powerup",SoundManager::PLAY,80,false);
			_actions->addAction(bulletHitPowerUp);
		}
	}

	// Delete food/power up
	if(deleteOther) {
		Obj::Options_t options;
		options.addNode = true;
		options.attachNode = true;
		options.addGraphic = true;
		options.addPhysic = true;
		options.typeAct = Obj::DELETE;
		options.entityName = other;

		std::make_shared<Obj>(_config, _nodes,_graphic, this,_actions,  _entities, options);

		// Delete bullet
		Obj::Options_t options2;
		options2.addNode = true;
		options2.attachNode = true;
		options2.addGraphic = true;
		options2.addPhysic = true;
		options2.typeAct = Obj::DELETE;
		options2.entityName = bullet;

		std::make_shared<Obj>(_config, _nodes,_graphic, this,_actions,  _entities, options2);
	}

	//std::cout << "Colision between " << bullet << " and " << other << " \n";


	return true;
}

void PhysicSystem::deletePhysicObject(Entity::ID id) {

}

void PhysicSystem::updateObject(const Entity::ID id,Ogre::Vector3 position) {


	//_componentMap[id]->_sceneNode->setPosition(position);

	auto body = _componentMap[id]->_rigidBody;

	body->enableActiveState ();
	// If you use Ogre::Vector3 to store the position
		//btTransform transform; //Declaration of the btTransform
	   //transform.setIdentity(); //This function put the variable of the object to default. The ctor of btTransform doesnt do it.
	   //transform.setOrigin(OgreBulletCollisions::OgreBtConverter::to(position)); //Set the new position/origin
	   //body->getBulletRigidBody()->setWorldTransform(transform); //Apply the btTransform to the body

	   const btVector3 & vector = OgreBulletCollisions::OgreBtConverter::to(position);
	   body->getBulletRigidBody()->translate(vector); //Apply the btTransform to the body
}

void PhysicSystem::activeObject(const Entity::ID id) {
	auto body = _componentMap[id]->_rigidBody;
	body->enableActiveState ();
}

void PhysicSystem::checkCollisions() {
	btCollisionWorld *bulletWorld = _physicWorld->getBulletCollisionWorld();
	int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

	for (int i=0;i<numManifolds;i++) {
		btPersistentManifold* contactManifold =
		bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
		btCollisionObject* obA =
		(btCollisionObject*)(contactManifold->getBody0());
		btCollisionObject* obB =
		(btCollisionObject*)(contactManifold->getBody1());

		OgreBulletCollisions::Object *obOB_A = _physicWorld->findObject(obA);
		OgreBulletCollisions::Object *obOB_B = _physicWorld->findObject(obB);

		if(obOB_A && obOB_B){
			/** Interesting collision types:
			* Food   - Environment -> Loses life
			* Bullet - Food    -> Earn points
			* Bullet - PowerUp -> Activate powerup
			* Bullet - Environment -> Disappear bullet
			* PowerUp- Environment -> Disappear powerup
			*/
			//bool hasCollide = _checkEnvironmentCollission(obOB_A->getName(), obOB_B->getName());
			//if(!hasCollide) _checkBulletCollission(obOB_A->getName(), obOB_B->getName());

		}
	}
}

bool PhysicSystem::checkCollisions(const Entity::ID me,const Entity::ID object){

	const std::string & meName = _mapper->idToName(me);
	const std::string & objectName = _mapper->idToName(object);

	btCollisionWorld *bulletWorld = _physicWorld->getBulletCollisionWorld();
	int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

	  for (int i=0;i<numManifolds;i++) {
	    btPersistentManifold* contactManifold =
	      bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
	    btCollisionObject* obA =
	      (btCollisionObject*)(contactManifold->getBody0());
	    btCollisionObject* obB =
	      (btCollisionObject*)(contactManifold->getBody1());

	    Ogre::SceneNode* meSceneNode = _sceneManager->getSceneNode(meName);
	    Ogre::SceneNode* objectSceneNode = _sceneManager->getSceneNode(objectName);

	    OgreBulletCollisions::Object *obMe = _physicWorld->findObject(meSceneNode);
	    OgreBulletCollisions::Object *obObject = _physicWorld->findObject(objectSceneNode);

	    OgreBulletCollisions::Object *obOB_A = _physicWorld->findObject(obA);
	    OgreBulletCollisions::Object *obOB_B = _physicWorld->findObject(obB);

	    if ( ((obOB_A == obMe) && (obOB_B == obObject)) || ((obOB_A == obObject) && (obOB_B == obMe)) ) {
	    	//std::cout << "#########ME_OBJECT_COLLISON###############" << std::endl;
			//std::cout << obOB_A->getName() << std::endl;
			//std::cout << obOB_B->getName() << std::endl;
			//std::cout << "########################" << std::endl;
			return true;
	    }
	  }
	  return false;
}


void PhysicSystem::addImpulse(const Entity::ID id,Ogre::Vector3 vector) {

	if(_componentMap.count(id)) {
		auto body = _componentMap[id]->_rigidBody;
 		float F = 30;

		if (body) {
		  if (!body->isStaticObject()) {
			body->enableActiveState ();
			//Ogre::Vector3 relPos(body->getCenterOfMassPosition());
			//Ogre::Vector3 relPos(0.2,1,0);
			//Ogre::Vector3 impulse (0.2,1,0);
			//std::cout << "### applyImpulse ###" << std::endl;
			body->applyImpulse (vector * F, vector);
		  }
		}
	}

}

void PhysicSystem::update(Ogre::Real deltaTime) {

	checkCollisions();
	_physicWorld->stepSimulation(deltaTime);
}
