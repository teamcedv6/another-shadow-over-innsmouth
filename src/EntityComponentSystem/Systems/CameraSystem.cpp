#include "CameraSystem.h"
#include "GameManager.h"

CameraSystem::CameraSystem() : System() {}

CameraSystem::CameraSystem(std::shared_ptr<GameManager> gameManager) : System(), _gameManager(gameManager) {

}

CameraSystem::CameraSystem(const CameraSystem & cameraSystem) : System(cameraSystem) {}

CameraSystem::CameraSystem(const CameraSystem && cameraSystem) : System(cameraSystem)  {}

CameraSystem::~CameraSystem() {}

CameraSystem & CameraSystem::operator= (const CameraSystem & cameraSystem) {
	return *this;
}

void CameraSystem::initializeViewport(Entity::ID id) {
	if(_viewport == nullptr) {
		auto w = _gameManager->getRoot()->getAutoCreatedWindow();
		auto c = _componentMap[id]->_camera.get();
		auto view = w->addViewport(c);
		_viewport = std::shared_ptr<Ogre::Viewport>(view);

		_activeCamera = _componentMap[id]->_camera;
	}
}

Ogre::Viewport * CameraSystem::getActiveViewport() {
	return _activeCamera->getViewport();
}

std::shared_ptr<Ogre::Camera> CameraSystem::getActiveCamera() {
	return _activeCamera;
}

void CameraSystem::setActiveCamera(Entity::ID id){
	_viewport->setCamera(_componentMap[id]->_camera.get());
	_activeCamera = _componentMap[id]->_camera;
}

void CameraSystem::setFov(Entity::ID id, Ogre::Degree fov) {
	_componentMap[id]->_camera -> setFOVy(fov);
}

void CameraSystem::setProjectionType(Entity::ID id, Ogre::ProjectionType projection) {
	_componentMap[id]->_camera -> setProjectionType(projection);
}

void CameraSystem::setNearDist(Entity::ID id, Ogre::Real nearDist) {
	_componentMap[id]->_camera -> setNearClipDistance(nearDist);
}

void CameraSystem::setFarDist(Entity::ID id, Ogre::Real farDist) {
	_componentMap[id]->_camera -> setFarClipDistance(farDist);
}
