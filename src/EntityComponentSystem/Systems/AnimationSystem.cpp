#include "AnimationSystem.h"

AnimationSystem::AnimationSystem() : System(){
}

AnimationSystem::AnimationSystem(std::shared_ptr<Ogre::SceneManager> sceneManager, std::shared_ptr<IdNameMapper> mapper) :
		System(), _sceneManager(sceneManager), _mapper(mapper) {
}

AnimationSystem::AnimationSystem(const AnimationSystem & animationSystem) : System(animationSystem){
	_sceneManager = animationSystem._sceneManager;
}

AnimationSystem::AnimationSystem(const AnimationSystem && animationSystem) : System(animationSystem) {
	_sceneManager = animationSystem._sceneManager;
}

AnimationSystem::~AnimationSystem() {
}

AnimationSystem & AnimationSystem::operator= (const AnimationSystem & animationSystem) { return *this;}

void AnimationSystem::runAnimation(const Entity::ID id, AnimationType animationType, Ogre::Real duration, bool blend, bool loop) {

	auto component = _componentMap[id];
	auto entity = _componentMap[id]->_entity;
	auto animation = getNameAnimation(animationType);

	  Ogre::AnimationState * newTarget(entity->getAnimationState(animation));
	  newTarget->setLoop(loop);
	  component->_blend = blend;
	  component->_duration = duration;
	  component->_loop = loop;

	  if ((component->_timeLeft <= 0) || (blend == false)){
	    if (component->_source != nullptr) component->_source->setEnabled(false);
	    component->_source = newTarget;
	    component->_source->setEnabled(true);
	    component->_source->setWeight(1);
	    component->_source->setTimePosition(0);
	    component->_timeLeft = component->_source->getLength();
	    component->_target = nullptr;
	  }
	  else {
	    if (component->_source != newTarget) {
	    	component->_target = newTarget;
	    	component->_target->setEnabled(true);
	    	component->_target->setWeight(0);
	    	component->_target->setTimePosition(0);
	    }
	  }
}

void AnimationSystem::update(Ogre::Real deltaTime) {

	for(auto it = _componentMap.begin();   it != _componentMap.end(); it++) {

		auto component = it->second;

		if (component->_source == nullptr) return;   // No hay fuente

		component->_source->addTime(deltaTime);   component->_complete = false;
		component->_timeLeft -= deltaTime;

		if ((component->_timeLeft <= 0) && (component->_target == nullptr)) component->_complete = true;

		if (component->_target != nullptr) {  // Si hay destino
			if (component->_timeLeft <= 0) {
				component->_source->setEnabled(false);  component->_source->setWeight(0);
				component->_source = component->_target;
				component->_source->setEnabled(true);   component->_source->setWeight(1);
				component->_timeLeft = component->_source->getLength();
				component->_target = nullptr;
			} else {   // Queda tiempo en Source... cambiar pesos
				Ogre::Real weight = component->_timeLeft / component->_duration;
				if (weight > 1) weight = 1.0;
				component->_source->setWeight(weight);
				component->_target->setWeight(1.0 - weight);
				if (component->_blend == true) component->_target->addTime(deltaTime);
			}
		}
		if ((component->_timeLeft <= 0) && component->_loop) component->_timeLeft = component->_source->getLength();
	}
}

bool AnimationSystem::isRunning(const Entity::ID id) {
	bool isRunning = false;
	if(_componentMap[id]->_timeLeft > 0)
		isRunning = true;

	return isRunning;
}

std::string AnimationSystem::getNameAnimation(AnimationType animationType) {

	switch (animationType) {
		case AnimationType::IDLE:
				return "IDLE";
			break;
		case AnimationType::WALK:
				return "WALK";
			break;
		case AnimationType::RUN:
				return "RUN";
			break;
		case AnimationType::CRAWL:
				return "STRANGEWALK";
			break;
		case AnimationType::JUMP:
				return "Jump";
			break;
		case AnimationType::USE:
				return "PICK";
			break;
		default:
				return "IDLE";
			break;
	}
}

