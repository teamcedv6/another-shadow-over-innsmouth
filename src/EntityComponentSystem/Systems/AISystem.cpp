#include "AISystem.h"

#include <cmath>

AISystem::AISystem() : System() {
	_recast = nullptr;
	_crowd = nullptr;
	_tileCache = nullptr;
	_debug = false;
	_singleNavMesh = false;
	_config = nullptr;
	_sceneManager = nullptr;
	_mapper = nullptr;
	_animSystem = nullptr;
	_activeHunt = false;
	_soundManager = nullptr;
	_tickTime = 0;
}

AISystem::AISystem(std::shared_ptr<Ogre::SceneManager> sceneManager, std::shared_ptr<Configuration> config,
		std::shared_ptr<MovementSystem> movementSystem, std::shared_ptr<IdNameMapper> mapper,
		std::shared_ptr<NodeManager> nodes, std::shared_ptr<AnimationSystem> animSystem,
		std::shared_ptr<SoundManager> soundManager) : System() {
	_recast = nullptr;
	_crowd = nullptr;
	_tileCache = nullptr;
	_debug = false;
	_singleNavMesh = false;
	_config = config;
	_sceneManager = sceneManager;
	_movementSystem = movementSystem;
	_mapper = mapper;
	_nodes = nodes;
	_animSystem = animSystem;
	_soundManager = soundManager;

	_tickTime = 0;
	_activeHunt = false;
	_configOptions();
}

AISystem::AISystem(const AISystem & aiSystem) : System(aiSystem) {
	_recast = aiSystem._recast;
	_crowd = aiSystem._crowd;
	_tileCache = aiSystem._tileCache;
	_navMesh = aiSystem._navMesh;
	_obstacles = aiSystem._obstacles;
	_debug = aiSystem._debug;
	_singleNavMesh = aiSystem._singleNavMesh;
	_config = aiSystem._config;
	_sceneManager = aiSystem._sceneManager;
	_mapper = aiSystem._mapper;
	_tickTime = aiSystem._tickTime;
	_player = aiSystem._player;
	_animSystem = aiSystem._animSystem;
	_activeHunt = aiSystem._activeHunt;
}

AISystem::AISystem(const AISystem && aiSystem) : System(aiSystem)  {
	_recast = aiSystem._recast;
	_crowd = aiSystem._crowd;
	_tileCache = aiSystem._tileCache;
	_navMesh = aiSystem._navMesh;
	_obstacles = aiSystem._obstacles;
	_debug = aiSystem._debug;
	_singleNavMesh = aiSystem._singleNavMesh;
	_config = aiSystem._config;
	_sceneManager = aiSystem._sceneManager;
	_mapper = aiSystem._mapper;
	_tickTime = aiSystem._tickTime;
	_player = aiSystem._player;
	_animSystem = aiSystem._animSystem;
	_activeHunt = aiSystem._activeHunt;
}

AISystem::~AISystem() {
	deleteAgents();
}

AISystem & AISystem::operator= (const AISystem & aiSystem) {
	_recast = aiSystem._recast;
	_crowd = aiSystem._crowd;
	_tileCache = aiSystem._tileCache;
	_navMesh = aiSystem._navMesh;
	_obstacles = aiSystem._obstacles;
	_debug = aiSystem._debug;
	_singleNavMesh = aiSystem._singleNavMesh;
	_config = aiSystem._config;
	_sceneManager = aiSystem._sceneManager;
	_mapper = aiSystem._mapper;
	_tickTime = aiSystem._tickTime;
	_player = aiSystem._player;

	return *this;
}

void AISystem::_configOptions() {
	_debug = _config->getAiConfig()->debug;
	_singleNavMesh = _config->getAiConfig()->singleNavMesh;
	_player = _config->getAiConfig()->player;
}

void AISystem::createOgreCrowd(std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> navMesh,std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> obstacles) {

	// Cargamos el escenario
	for(auto& kv : (*navMesh)){
		_navMesh.push_back(_sceneManager->getEntity(kv.first));
	}

	// En el caso de ser una NavMesh único (sin actualizaciones en tiempo real) se añaden también los obstáculos
	if(_singleNavMesh) {
		for(auto& kv : (*obstacles)){
			_navMesh.push_back(_sceneManager->getEntity(kv.first));
		}
	}

	// Creamos recast
	_recast = std::make_shared<OgreRecast>(_sceneManager.get());

	if(_singleNavMesh) {

		// En SingleNavMesh
		if(_recast->NavMeshBuild(_navMesh)) {

			if(_debug)
				_recast->drawNavMesh();

		} else {

			Ogre::LogManager::getSingletonPtr()->logMessage("ERROR: could not generate useable navmesh from mesh.");

			return;
		}

	} else {

		// En NavMesh actualizable
		_tileCache = std::make_shared<OgreDetourTileCache>(_recast.get());

		if(!_debug)
			_tileCache->DEBUG_DRAW = false;

		if(_tileCache->TileCacheBuild(_navMesh)) {
			if(_debug)
				_tileCache->drawNavMesh();
		} else {
			Ogre::LogManager::getSingletonPtr()->logMessage("ERROR: could not generate useable navmesh from mesh using detourTileCache.");
			return;
		}

		// Añadimos los obstaculos modificables
		for(auto& kv : (*obstacles)){
			std::shared_ptr<ConvexVolume>  tempObstacle(new ConvexVolume(InputGeom::getWorldSpaceBoundingBox(_sceneManager->getEntity(kv.first)), _recast->getAgentRadius()));
			tempObstacle->area = RC_MESH_NULL_IDX;
			_tileCache->addConvexShapeObstacle(tempObstacle.get());
			if(_debug)
				InputGeom::drawConvexVolume(tempObstacle.get(), _sceneManager.get());
			_obstacles[kv.first] = std::make_tuple(tempObstacle,true);
		}
	}

	// Creamos Crowd
	_crowd = std::make_shared<OgreDetourCrowd>(_recast.get());

}

void AISystem::updateNavMesh(std::string obstacleName) {

	// Añadir de forma dinámica objetos en la NavMesh
	std::shared_ptr<ConvexVolume> actualObstacle;
	bool obstacleState;

	actualObstacle = std::get<0>(_obstacles[obstacleName]);
	obstacleState = std::get<1>(_obstacles[obstacleName]);

	if(obstacleState) {
		// Open gate
		_tileCache->removeConvexShapeObstacle(actualObstacle.get());

		Ogre::Vector3 pos = _sceneManager->getSceneNode(obstacleName)->getPosition();
		pos.y += _crowd->getAgentHeight() + 20;
		_sceneManager->getSceneNode(obstacleName)->setPosition(pos);
		std::get<1>(_obstacles[obstacleName]) = false;
	} else {
		// Close gate
		_tileCache->addConvexShapeObstacle(actualObstacle.get());
		Ogre::Vector3 pos = _sceneManager->getSceneNode(obstacleName)->getPosition();
		pos.y -= _crowd->getAgentHeight() + 20;
		_sceneManager->getSceneNode(obstacleName)->setPosition(pos);
		std::get<1>(_obstacles[obstacleName]) = true;
	}
}

void AISystem::createAgents() {

	// Creamos los agentes
	for (auto& kv : _componentMap) {

		// AgentId
		auto agentId = _createAgent(kv.first);
		kv.second->setId(agentId);

		//Agent
		auto agent = _crowd->getAgent(kv.second->getId());
		kv.second->setAgent(agent);

		// Eyes
		std::ostringstream oss;
		oss << _mapper->idToName(kv.first) << "_eyes" ;
		std::string tmpName = oss.str();

		if(kv.second->getEyes() == nullptr) {
			std::shared_ptr<Ogre::Camera> eyes(_sceneManager->createCamera(tmpName));

			if(_debug) {
				eyes->setVisible(true);
				eyes->setDebugDisplayEnabled(true);
			}

			eyes->setNearClipDistance(0.1);
			eyes->setFarClipDistance(6);
			eyes->setAspectRatio(1.77);

			// Set view
			kv.second->setEyes(eyes);

			// Set correcta direction
			auto parentNode = _nodes->getNode(_mapper->idToName(kv.first));
			auto cameraNode = _nodes->createNode(tmpName);
			cameraNode->attachObject(eyes.get());
			cameraNode->yaw(Ogre::Radian(Ogre::Degree(180)));
			parentNode->addChild(cameraNode);
		}

	}
}

void AISystem::deleteAgents() {
	for (auto& kv : _componentMap) {
		_crowd->removeAgent(kv.second->getId());
	}
}

int AISystem::_createAgent(const Entity::ID entityId){

	if(_crowd->getNbAgents() >= _crowd->getMaxNbAgents()) {
		Ogre::LogManager::getSingletonPtr()->logMessage("Error: Cannot create crowd agent for new character. Limit of "+Ogre::StringConverter::toString(_crowd->getMaxNbAgents())+" reached", Ogre::LML_CRITICAL);
		throw new Ogre::Exception(1, "Cannot create crowd agent for new character. Limit of "+Ogre::StringConverter::toString(_crowd->getMaxNbAgents())+" reached", "OgreRecastApplication::getOrCreateCharacter("+_mapper->idToName(entityId)+")");
	}

	Ogre::Vector3 point = _componentMap[entityId]->getInitPosition();
	_recast->findNearestPointOnNavmesh(point,point);


	float radius = 0.5f;
	float height = 0.5f;
	float maxAcceleration = 4.0f;
	float maxSpeed = 0.0;
	float collisionQueryRange = radius * 2.0f;
	float pathOptimizationRange = radius * 4.0f;

	bool isPlayer = false;
	if(_mapper->idToName(entityId) == _player)
		isPlayer = true;

	return _crowd->addAgent(point,radius,height,maxAcceleration,maxSpeed,collisionQueryRange,pathOptimizationRange,isPlayer);

}

void AISystem::_setDestinationForAllAgents(Ogre::Vector3 destination, bool adjustExistingPath) {

	for (auto& kv : _componentMap) {
		_crowd->setMoveTarget(kv.second->getId(),destination, adjustExistingPath);
		Ogre::Vector3 agentPos;
		OgreRecast::FloatAToOgreVect3(kv.second->getAgent()->npos, agentPos);
		//_calculateAndDrawPath(agentPos, destination, 0, kv.second->getId());
	}

}

void AISystem::_setDestinationForAgentId(Entity::ID entityId, Ogre::Vector3 destination, bool adjustExistingPath) {

	auto component = _componentMap[entityId];
	_crowd->setMoveTarget(component->getId(),destination, adjustExistingPath);
	Ogre::Vector3 agentPos;
	OgreRecast::FloatAToOgreVect3(component->getAgent()->npos, agentPos);
	//_calculateAndDrawPath(agentPos, destination, 0, component->getId());
}

void AISystem::_calculateAndDrawPath(Ogre::Vector3 beginPos, Ogre::Vector3 endPos, int pathNb, int targetId) {
	// Note that this calculated path is not actually used except for debug drawing.
	    // DetourCrowd will take care of calculating a separate path for each of its agents.
	    int ret = _recast->FindPath(beginPos, endPos, pathNb, targetId) ;
	    if( ret >= 0 ){
	    	_recast->CreateRecastPathLine(pathNb,targetId) ; // Draw a line showing path at specified slot
		}else{
			_recast->DeleteRecastPathLine(targetId);
	        Ogre::LogManager::getSingletonPtr()->logMessage("ERROR: could not find a (full) path ("+_recast->getPathFindErrorMsg(ret)+"). It's possible there is a partial path.");
	        // We dont bother with partial paths as this is only for debug drawing. DetourCrowd handles this for us anyway.
		}
}

void AISystem::update(Ogre::Real deltaTime) {

	// Actualizamos la NavMesh
	_tileCache->handleUpdate(deltaTime);

	// Actualizamos cada segundo la comprobación de estados
	_tickTime += deltaTime;

	if(_tickTime >= 1) {
		_tickTime = 0;
		_aiTick();
	}

	// Actualizamos el avance de los agentes
	_crowd->updateTick(deltaTime);

	for (auto& kv : _componentMap) {

		auto agent = kv.second->getAgent();
		Ogre::Vector3 agentPos;
		OgreRecast::FloatAToOgreVect3(agent->npos, agentPos);

		// Actualizamos la posición y dirección de cada personaje según la del agente de ogrecrowd
		// IDLE no hace falta
		if(kv.second->getActualState() != AIComponent::IDLE) {

			// Repintamos la ruta a cada paso
			if(_debug)
				_calculateAndDrawPath(agentPos, kv.second->getNextPosition(), 0, kv.second->getId());

			// Obtenemos el vector dirección para apuntar con la cámara al mismo lugar al que estamos avanzando
			Ogre::Vector3 velocity;
			OgreRecast::FloatAToOgreVect3(kv.second->getAgent()->nvel, velocity);

			auto node = _sceneManager->getSceneNode(_mapper->idToName(kv.first));
			auto orientation = node->getOrientation();
			auto relativeLookingDirection = orientation * -Ogre::Vector3::UNIT_Z;
			// Rotate to look in walking direction
			relativeLookingDirection.y = 0;  // Ignore y direction
			relativeLookingDirection.normalise();
			velocity.y = 0;
			velocity.normalise();

			// Si tenemos a algún agente cazando le apuntamos a el en lugar de la dirección a la que vamos para hacer más complicado el perderlo de vista
			// Y también actualizamos _doHunt para que en lugar de cada segundo lo haga a cada frame, igualmente para no perder al objetivo
			//_activeHunt = true;
			if( (kv.second->getActualState() == AIComponent::HUNT) && _activeHunt)
				_doHunt(kv.first);
				// Al cambiar de altura el personaje principal se giran y terminan tumbados
				//node->lookAt(_sceneManager->getSceneNode(_player)->getPosition(),Ogre::Node::TS_WORLD);
			// Si no seteamos la dirección del camino a seguir
			node->setDirection(velocity,Ogre::Node::TS_WORLD);
			node->yaw(Ogre::Radian(Ogre::Degree(180)));


			// Movemos el personaje
			_movementSystem->setPosition(kv.first,_mapper->idToName(kv.first),agentPos - Ogre::Vector3(0,0.2,0));
		}else {
			_movementSystem->setPosition(kv.first,_mapper->idToName(kv.first),agentPos - Ogre::Vector3(0,0.2,0));
		}
	}
}

void AISystem::_aiTick() {
	for (auto& kv : _componentMap) {

		// Comprobamos si algún agente está viendo al objetivo para cambiarlo a modo hunt
		//_activeHunt = true;
		if( (_mapper->nameToId(_player) != kv.first) && _activeHunt)
			_checkHunt(kv.first);

		int state = kv.second->getActualState();
		switch (state) {
			case AIComponent::IDLE:
					_doIdle(kv.first);
				break;
			case AIComponent::RANDOM_PATROL:
					_doRandomPatrol(kv.first);
				break;
			case AIComponent::PATROL:
					_doPatrol(kv.first);
				break;
			case AIComponent::HUNT:
					_doHunt(kv.first);
				break;
			case AIComponent::GOTO:
					_doGoTo(kv.first);
				break;
			default:
				break;
		}
	}
}

void AISystem::_checkHunt(const Entity::ID id) {

	// Si algún agente ve al objetivo setea su posición y pone a todos los agentes en modo hunt
	// El agente que lo ve lo persigue, el resto pasaría a modo GOTO e irian a la última posición en la que lo vio el agente
	auto component = _componentMap[id];

	auto mainCharacter = _sceneManager->getSceneNode(_player);
	bool seeCharacter = component->getEyes()->isVisible(mainCharacter->getPosition());

	if(seeCharacter) {
		_soundManager->selectResource(SoundManager::FX,"view",SoundManager::PLAY,80,0);
		// Alert all agents
		//setHuntAgent(id);
		for(auto& agents : _componentMap) {

			Ogre::Vector3 newPoint = _recast->getRandomNavMeshPointInCircle(mainCharacter->getPosition(),10);
			agents.second->setLastView(newPoint);
			setHuntAgent(agents.first);
		}
	}
}

void AISystem::_doIdle(const Entity::ID id) {

}

void AISystem::_doRandomPatrol(const Entity::ID id) {

	// Vemos si estamos en el punto de destino y si es así ponemos rumbo a otro
	auto component = _componentMap[id];

	Ogre::Vector3 destinationPoint = component->getNextPosition();
	_recast->findNearestPointOnNavmesh(destinationPoint,destinationPoint);
	Ogre::Vector3 actualPoint;
	OgreRecast::FloatAToOgreVect3(component->getAgent()->npos, actualPoint);

	if(destinationPoint.positionEquals(actualPoint,0.5)){
		Ogre::Vector3 newPoint = _recast->getRandomNavMeshPointInCircle(component->getInitRandomPosition(),3);
		component->setNextPosition(newPoint);
		_setDestinationForAgentId(id,newPoint,false);
	}
}

void AISystem::_doPatrol(const Entity::ID id) {

	// Vemos si estamos en el punto de destino y si es así vamos al siguiente. Si llegamos al último empezamos de nuevo
	auto component = _componentMap[id];

	auto destinationPoint = component->getNextPosition();
	_recast->findNearestPointOnNavmesh(destinationPoint,destinationPoint);
	Ogre::Vector3 actualPoint;
	OgreRecast::FloatAToOgreVect3(component->getAgent()->npos, actualPoint);

	if(destinationPoint.positionEquals(actualPoint,0.5)){


		for (std::vector<Ogre::Vector3>::iterator it=component->_patrolPoints.begin(); it!=component->_patrolPoints.end(); it++){
			std::vector<Ogre::Vector3>::iterator auxIt = it;
			auxIt++;
			if(auxIt == component->_patrolPoints.end()) {
				Ogre::Vector3 auxComponent = (*component->_patrolPoints.begin());
				_recast->findNearestPointOnNavmesh(auxComponent,auxComponent);
				component->setNextPosition(auxComponent);
				_setDestinationForAgentId(id,auxComponent,false);
				break;
			}else {

				setIdleAgent(id);
				break;

				Ogre::Vector3 point = (*auxIt);
				_recast->findNearestPointOnNavmesh(point,point);
				Ogre::Vector3 comparePoint = (*it);
				_recast->findNearestPointOnNavmesh(comparePoint,comparePoint);
				if(comparePoint.positionEquals(actualPoint,0.5)){
					component->setNextPosition(point);
					_setDestinationForAgentId(id,point,false);
					break;
				}
			}
		}
	}
}

void AISystem::_doHunt(const Entity::ID id) {

	// Si estamos viendo al objetivo, lo perseguimos y si lo hemos perdido de vista vamos hasta su ultima posición en la que lo vimos
	auto component = _componentMap[id];

	auto mainCharacter = _sceneManager->getSceneNode(_player);
	bool seeCharacter = component->getEyes()->isVisible(mainCharacter->getPosition());

	if(seeCharacter) {
		auto positionCharacter = mainCharacter->getPosition();
		_recast->findNearestPointOnNavmesh(positionCharacter,positionCharacter);
		component->setNextPosition(positionCharacter);
		component->setLastView(positionCharacter);
		_setDestinationForAgentId(id,positionCharacter,false);
	} else {
		setGotoAgent(id,component->getLastView());
	}
}

void AISystem::_doGoTo(const Entity::ID id) {

	// Comprobamos si hemos llegado y si es así nos ponemos en modo random patrol
	auto component = _componentMap[id];

	auto destinationPoint = component->getNextPosition();
	_recast->findNearestPointOnNavmesh(destinationPoint,destinationPoint);
	Ogre::Vector3 actualPoint;
	OgreRecast::FloatAToOgreVect3(component->getAgent()->npos, actualPoint);

	if(destinationPoint.positionEquals(actualPoint,0.5)) {
		//setIdleAgent(id);
		if(_mapper->idToName(id) != _player){
			setRandomAgent(id);
		}else {
			setIdleAgent(id);
		}

	}
}

void AISystem::setIdleAgent(const Entity::ID entityId) {

	auto component = _componentMap[entityId];
	component->setActualState(AIComponent::IDLE);

	bool isRunning = _animSystem->isRunning(entityId);
	_animSystem->runAnimation(entityId, AnimationSystem::IDLE, 0.5, isRunning, true);
}

void AISystem::setRandomAgent(const Entity::ID entityId) {

	auto name = _mapper->idToName(entityId);
	if((name != "drunk") && (name != "seller")){
		auto component = _componentMap[entityId];
		component->setActualState(AIComponent::RANDOM_PATROL);

		Ogre::Vector3 actualPosition = _sceneManager->getSceneNode(_mapper->idToName(entityId))->getPosition();
		component->setInitRandomPosition(actualPosition);
		component->setNextPosition(actualPosition);

		_recast->findNearestPointOnNavmesh(actualPosition,actualPosition);

		_setDestinationForAgentId(entityId,actualPosition,false);

		AnimationSystem::AnimationType type = AnimationSystem::WALK;
		if(_activeHunt && (_mapper->idToName(entityId) != _player))
			type = AnimationSystem::CRAWL;

		bool isRunning = _animSystem->isRunning(entityId);
		_animSystem->runAnimation(entityId, type, 0.5, isRunning, true);
	}
}

void AISystem::setPatrolAgent(const Entity::ID entityId) {

	auto name = _mapper->idToName(entityId);
	if((name != "drunk") && (name != "seller")){
		auto component = _componentMap[entityId];
		component->setActualState(AIComponent::PATROL);

		Ogre::Vector3 firstPoint = (*component->getPatrolPositions().begin());

		_recast->findNearestPointOnNavmesh(firstPoint,firstPoint);

		component->setNextPosition(firstPoint);
		_setDestinationForAgentId(entityId,firstPoint,false);

		AnimationSystem::AnimationType type = AnimationSystem::WALK;
		if(_activeHunt && (_mapper->idToName(entityId) != _player))
			type = AnimationSystem::CRAWL;

		bool isRunning = _animSystem->isRunning(entityId);
		_animSystem->runAnimation(entityId, type, 0.5, isRunning, true);
	}
}

void AISystem::setHuntAgent(const Entity::ID entityId) {

	auto name = _mapper->idToName(entityId);
	if((name != "drunk") && (name != "seller")){
		auto component = _componentMap[entityId];
		component->setActualState(AIComponent::HUNT);

		AnimationSystem::AnimationType type = AnimationSystem::WALK;
		if(_activeHunt && (_mapper->idToName(entityId) != _player))
			type = AnimationSystem::CRAWL;

		bool isRunning = _animSystem->isRunning(entityId);
		_animSystem->runAnimation(entityId, type, 0.5, isRunning, true);
	}
}

void AISystem::setGotoAgent(const Entity::ID entityId, Ogre::Vector3 newPosition) {

	auto name = _mapper->idToName(entityId);
	if((name != "drunk") && (name != "seller")){
		auto component = _componentMap[entityId];
		component->setActualState(AIComponent::GOTO);

		_recast->findNearestPointOnNavmesh(newPosition,newPosition);

		component->setNextPosition(newPosition);
		_setDestinationForAgentId(entityId,newPosition,false);

		AnimationSystem::AnimationType type = AnimationSystem::WALK;
		if(_activeHunt && (_mapper->idToName(entityId) != _player))
			type = AnimationSystem::CRAWL;

		bool isRunning = _animSystem->isRunning(entityId);
		_animSystem->runAnimation(entityId, type, 0.5, isRunning, true);
	}

}

void AISystem::setAllState(AIComponent::State state, Ogre::Vector3 position) {
	for(auto& agent : _componentMap) {
		if(agent.first != _mapper->nameToId(_player)) {
			switch (state) {
				case AIComponent::IDLE :
						setIdleAgent(agent.first);
					break;
				case AIComponent::RANDOM_PATROL :
						setRandomAgent(agent.first);
					break;
				case AIComponent::PATROL :
						setPatrolAgent(agent.first);
					break;
				case AIComponent::HUNT :
						setHuntAgent(agent.first);
					break;
				case AIComponent::GOTO :
						setGotoAgent(agent.first,position);
					break;
				default:
					setIdleAgent(agent.first);
					break;
			}
		}
	}
}

void AISystem::huntMode(bool hunt) {
	_activeHunt = hunt;
}

void AISystem::resetEnd(bool andGame) {
	deleteAgents();
	for(auto& agent : _componentMap) {

		if(agent.first == _mapper->nameToId(_player)){
			Ogre::Vector3 endPosition = Ogre::Vector3(74.69957,0.0000094,-96.44366);
			if(andGame)
				endPosition = Ogre::Vector3(agent.second->getInitPosition());

			_sceneManager->getSceneNode(_player)->setPosition(endPosition);
			agent.second->setInitPosition(endPosition);
		} else {
			_sceneManager->getSceneNode(_mapper->idToName(agent.first))->setPosition(agent.second->getInitPosition());
		}
	}

	createAgents();

	if(!andGame)
		huntMode(true);

	setIdleAgent(_mapper->nameToId(_player));
	setAllState(AIComponent::PATROL,Ogre::Vector3::ZERO);

}

std::shared_ptr<OgreRecast> AISystem::getRecast() {
	return _recast;
}

std::shared_ptr<OgreDetourCrowd> AISystem::getCrowd() {
	return _crowd;
}

std::shared_ptr<OgreDetourTileCache> AISystem::getTileCache() {
	return _tileCache;
}

void AISystem::changeDebug(bool debug) {
	_debug = debug;

	for(auto& agents : _componentMap) {
		agents.second->getEyes()->setVisible(debug);
		agents.second->getEyes()->setDebugDisplayEnabled(debug);
	}
}
