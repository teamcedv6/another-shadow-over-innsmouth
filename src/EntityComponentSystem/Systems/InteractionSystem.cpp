#include <InteractionSystem.h>

InteractionSystem::InteractionSystem() : System() {}

InteractionSystem::InteractionSystem(const std::shared_ptr<GameManager> gameManager, const std::shared_ptr<UserInterfaceSystem> userInterface, const std::shared_ptr<CameraSystem> cameras, bool showInteractionOptions) :  System(), _ui(userInterface), _rootUI(nullptr),
		_cameras(cameras), _mainCharacter(nullptr), _enterAnim(nullptr), _exitAnim(nullptr), _active(nullptr), _elapsedTime(0), _showInteractionOptions(showInteractionOptions), _id(-1),
		_givingObject(false), _usingObject(false), _interactingId(-1), _gameManager(gameManager), _actions(gameManager->getActionsManager()) {

	for(int i = 0; i < 4; i++) {
			_interactionsActives[i] = false;
			_interactionsF[i] = [](int i){};
	}

	_talkF = std::bind(&InteractionSystem::talk, this, std::placeholders::_1);
	_lookF = std::bind(&InteractionSystem::look, this, std::placeholders::_1);
	_pickF = std::bind(&InteractionSystem::pick, this, std::placeholders::_1);
	_useF = std::bind(&InteractionSystem::use, this, std::placeholders::_1);
	_giveF = std::bind(&InteractionSystem::give, this, std::placeholders::_1);

}

InteractionSystem::InteractionSystem(const InteractionSystem & interactionSystem) : _ui(interactionSystem._ui), _rootUI (interactionSystem._rootUI), _mainCharacter(interactionSystem._mainCharacter),
		_cameras(interactionSystem._cameras), _enterAnim(interactionSystem._enterAnim), _active(interactionSystem._active), _exitAnim(interactionSystem._exitAnim), _elapsedTime(interactionSystem._elapsedTime),
		_showInteractionOptions(interactionSystem._showInteractionOptions), _id(-1), _givingObject(interactionSystem._givingObject), _usingObject(interactionSystem._usingObject), _interactingId(interactionSystem._interactingId) {
	std::copy(std::begin(interactionSystem._interactionsF), std::end(interactionSystem._interactionsF), std::begin(_interactionsF));
	std::copy(std::begin(interactionSystem._interactionsActives), std::end(interactionSystem._interactionsActives), std::begin(_interactionsActives));

}

InteractionSystem::InteractionSystem(const InteractionSystem && interactionSystem) : _ui(interactionSystem._ui), _rootUI (interactionSystem._rootUI), _mainCharacter(interactionSystem._mainCharacter),
		_cameras(interactionSystem._cameras), _enterAnim(interactionSystem._enterAnim), _active(interactionSystem._active), _exitAnim(interactionSystem._exitAnim), _elapsedTime(interactionSystem._elapsedTime),
		_showInteractionOptions(interactionSystem._showInteractionOptions), _id(interactionSystem._id), _givingObject(interactionSystem._givingObject), _usingObject(interactionSystem._usingObject), _interactingId(interactionSystem._interactingId) {
	std::copy(std::begin(interactionSystem._interactionsF), std::end(interactionSystem._interactionsF), std::begin(_interactionsF));
	std::copy(std::begin(interactionSystem._interactionsActives), std::end(interactionSystem._interactionsActives), std::begin(_interactionsActives));
}

InteractionSystem::~InteractionSystem () {}

InteractionSystem & InteractionSystem::operator= (const InteractionSystem & interactionSystem) {
	_showInteractionOptions = interactionSystem._showInteractionOptions;
	_rootUI = interactionSystem._rootUI;
	_mainCharacter = interactionSystem._mainCharacter;
	_elapsedTime = interactionSystem._elapsedTime;
	_enterAnim = interactionSystem._enterAnim;
	_exitAnim = interactionSystem._exitAnim;
	_active = interactionSystem._active;
	_id = interactionSystem._id;
	std::copy(std::begin(interactionSystem._interactionsF), std::end(interactionSystem._interactionsF), std::begin(_interactionsF));
	std::copy(std::begin(interactionSystem._interactionsActives), std::end(interactionSystem._interactionsActives), std::begin(_interactionsActives));

	return *this;
}

void InteractionSystem::setMainCharacter(Ogre::SceneNode * character) {
	_mainCharacter = character;
}

void InteractionSystem::setInteractionOptionsVisibility(bool show) {
	_showInteractionOptions = show;
}

void InteractionSystem::update(Ogre::Real deltaTime) {
	//_elapsedTime += deltaTime;
	_waiting -= deltaTime;


	/*
	 * IsPlaying always return false, implemented manual waited
	 * if(!_exitAnim->IsPlaying() && !_enterAnim->IsPlaying()) { //_elapsedTime >= 0.25) {
	 */
	//std::cout << "W: " << _waiting << "\n";
	if(_waiting <= 0.0f) {
		//_elapsedTime = 0;
		// Initial minimumDistance value establish a maximum threshold in interaction activation (not in showing interaction)
		float minimunDistance = 3;
		float currentDistance;
		int id;
		shared_ptr<InteractionComponent> nearest = nullptr;
		shared_ptr<InteractionComponent> comp;

		_waiting = 0.0f;

		for(auto it = _componentMap.begin(); it != _componentMap.end(); it++) {
			comp = it->second;
			currentDistance = (_mainCharacter->getPosition() - comp->_node->getPosition()).length();

			if(_showInteractionOptions && currentDistance <= comp->_activateDistance) {
				// Activate interaction posibility clues
			}

			if(currentDistance <= comp->_interactionDistance && currentDistance <= minimunDistance) {
				minimunDistance = currentDistance;
				nearest = comp;
				id = it->first;
			}
		}

		// If there's a change, show interaction options
		//if(nearest == nullptr) std::cout << "nearest == nullptr ";
		//if(_active == nullptr) std::cout << "active == nullptr ";
		//if(nearest != nullptr && _active != nullptr)
		//std::cout << nearest->_node->getName() << "!=" << _active->_node->getName() << " " << (nearest != _active) << "\n";
		//else std::cout<< "\n";

		if(nearest != _active) {

			// If there was an active, play gently exit animation
			if(_active != nullptr) {
				_exitAnim->Begin();
				_waiting = 0.6f;
			}

			// If there is a new one, play enter one
			//if(!_exitAnim->IsPlaying() && nearest != nullptr) {
			if(_waiting <= 0.0f && nearest != nullptr) {

				std::string actionsNames[4];

				switch(nearest->_type) {
					case InteractionComponent::PERSON_TALKABLE:
						_interactionsActives[0] = true; _interactionsActives[1] = true; _interactionsActives[2] = true; _interactionsActives[3] = true;
						_interactionsF[0] = _lookF; _interactionsF[1] = _giveF;_interactionsF[2] = _talkF;_interactionsF[3] = _useF;
						actionsNames[0] = "Mirar"; actionsNames[1]= "Dar"; actionsNames[2] = "Hablar"; actionsNames[3] = "Usar";
						break;
					case InteractionComponent::PERSON_NON_TALKABLE:

						break;
					case InteractionComponent::OBJECT_PICKABLE:
						_interactionsActives[0] = true; _interactionsActives[1] = false; _interactionsActives[2] = true; _interactionsActives[3] = true;
						_interactionsF[0] = _lookF; _interactionsF[1] = [](int i){};_interactionsF[2] = _pickF;_interactionsF[3] = _useF;
						actionsNames[0] = "Mirar"; actionsNames[1]= ""; actionsNames[2] = "Coger"; actionsNames[3] = "Usar";
						break;
					case InteractionComponent::OBJECT_NON_PICKABLE:
						_interactionsActives[0] = true; _interactionsActives[1] = false; _interactionsActives[2] = false; _interactionsActives[3] = true;
						_interactionsF[0] = _lookF; _interactionsF[1] = [](int i){};_interactionsF[2] = [](int i){};_interactionsF[3] = _useF;
						actionsNames[0] = "Mirar"; actionsNames[1]= ""; actionsNames[2] = ""; actionsNames[3] = "Usar";
						break;
				}


				std::cout << "NEAREST -> " << nearest->_node->getName() <<  " " << nearest->_node->getPosition() << "\n";
				Ogre::Vector3 worldPos = nearest->_node->getPosition();
				std::shared_ptr<Ogre::Camera> pCamera = _cameras->getActiveCamera();
				Noesis::Grid * grid =(Noesis::Grid*) _rootUI->FindName("mainGrid");

				 Ogre::Vector4 p(worldPos.x,worldPos.y,worldPos.z,1);

				p = pCamera->getViewMatrix()*p;
				p = pCamera->getProjectionMatrix()*p;

				Noesis::TextBlock * actionTextBlock;
				for(int i = 0; i < 4; i++) {
					std::stringstream ss;
					ss << "accion" << i+1;
					actionTextBlock = (Noesis::TextBlock *)grid->FindName(ss.str().c_str());

					if(_interactionsActives[i]) {
						actionTextBlock->SetVisibility(Noesis::Visibility::Visibility_Visible);
						actionTextBlock->SetText(actionsNames[i].c_str());
					}
					else {
						actionTextBlock->SetVisibility(Noesis::Visibility::Visibility_Hidden);
					}
				}

				Ogre::Vector2 screenPos(p.x * 0.5f/p.w + 0.5f, p.y * 0.5f /p.w + 0.5f);
				float x = screenPos.x * pCamera->getViewport()->getActualWidth() - 185;
				float y = screenPos.y * pCamera->getViewport()->getActualHeight() - 260;

				grid->SetMargin(Noesis::Thickness(x, y,(float)0,(float)0));
				std::cout << "ScreenPos:  " << screenPos << " " << screenPos.x * pCamera->getViewport()->getActualWidth() << " " << screenPos.y * pCamera->getViewport()->getActualHeight() << "\n";

				_enterAnim->Begin();
				_waiting = 0.6f;
				_id = id;

			}

			_active = nearest;
		}
	}

}

void InteractionSystem::talk(int id) {
	conversations->setConversation(id);
	conversations->startConversation();
}

void InteractionSystem::look(int id) {
	std::string str = _gameManager->getMapper()->idToName(id);
	std::stringstream ss;

	ss << "look_" << str << ".xml";
	conversations->loadConversation(ss.str());
	conversations->startConversation();
}

void InteractionSystem::pick(int id) {
	std::string name = _gameManager->getMapper()->idToName(id);
	DeleteEntity deleteSubaction(id, _gameManager);
	deleteSubaction.update(0);

	InventoryChange pickObject(name, _gameManager->getInventory(), 1);
	pickObject.update(0);
}

void InteractionSystem::use(int id) {
	_usingObject = true;
	_interactingId = id;

	((InventoryState *)_gameManager->getState(GameState::INVENTORY).get())->setItemSelectionMode(true, InventoryState::SelectionType::USE);
	_gameManager->pushState(_gameManager->getState(GameState::INVENTORY));
}

void InteractionSystem::give(int id) {
	_givingObject = true;
	_interactingId = id;

	((InventoryState *)_gameManager->getState(GameState::INVENTORY).get())->setItemSelectionMode(true, InventoryState::SelectionType::GIVE);
	_gameManager->pushState(_gameManager->getState(GameState::INVENTORY));
}

void InteractionSystem::resume() {
	std::shared_ptr<GameState> invState = _gameManager->getState(GameState::INVENTORY);
	std::shared_ptr<Inventory::InventoryObject> selectedItem = (static_cast<InventoryState*>(invState.get()))->getSelectedItem();

	if(selectedItem != nullptr) {
		if(_givingObject) {
			_actions->giveObjectTo(selectedItem->name, _gameManager->getMapper()->idToName(_interactingId));
			_givingObject = false;
		}
		else if(_usingObject) {
			_actions->useObjectWith(selectedItem->name, _gameManager->getMapper()->idToName(_interactingId));
			_usingObject = false;
		}
	}

	_interactingId = -1;
}

void InteractionSystem::startInterface() {
	_ui->loadUI(_uiName, "GameUI.xaml");
	_rootUI = (Noesis::UserControl *)_ui->getUI(_uiName)->root.GetPtr();
	_enterAnim = (Noesis::Storyboard *)_rootUI->FindName("enter");
	_exitAnim = (Noesis::Storyboard *)_rootUI->FindName("exit");
}

void InteractionSystem::clearInterface() {
	_rootUI = nullptr;
	_active = nullptr;
	_enterAnim = nullptr;
	_exitAnim = nullptr;
	_ui->unloadUI(_uiName);
}

void InteractionSystem::action(int n) {
	if(_active != nullptr && _interactionsActives[n]) {
		_interactionsF[n](_id);
	}
}

