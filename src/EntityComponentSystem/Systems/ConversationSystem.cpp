#include "ConversationSystem.h"
#include "ConversationLoader.h"

ConversationSystem::ConversationSystem() : System(),  _currentNodeType(ConversationNode::NODE_EMPTY) {}

ConversationSystem::ConversationSystem(const std::shared_ptr<UserInterfaceSystem> userInterface, const std::shared_ptr<GameManager> gameManager) :  System(), _currentNodeType(ConversationNode::NODE_EMPTY), _ui(userInterface), _gameManager(gameManager) {}

ConversationSystem::ConversationSystem(const ConversationSystem & conversationSystem) : _gameManager(conversationSystem._gameManager), _ui(conversationSystem._ui) {
	_currentNodeType = conversationSystem._currentNodeType;
	_conversation = conversationSystem._conversation;
}

ConversationSystem::ConversationSystem(const ConversationSystem && conversationSystem) : _gameManager(conversationSystem._gameManager), _ui(conversationSystem._ui) {
	_currentNodeType = conversationSystem._currentNodeType;
	_conversation = conversationSystem._conversation;
}

ConversationSystem::~ConversationSystem () {}

ConversationSystem & ConversationSystem::operator= (const ConversationSystem & conversationSystem) {
	ConversationSystem tmp(conversationSystem._ui, conversationSystem._gameManager);
	return tmp;
}

void ConversationSystem::up() {
	bool continueNode = true;

	continueNode = _conversation->getNode()->up();
	if(!continueNode) _updateNextNode();
}

void ConversationSystem::down() {
	bool continueNode = true;

	continueNode = _conversation->getNode()->down();
	if(!continueNode) _updateNextNode();
}

void ConversationSystem::push() {
	bool continueNode = true;

	continueNode = _conversation->getNode()->push();
	if(!continueNode) _updateNextNode();
}

void ConversationSystem::back() {
	bool continueNode = true;

	continueNode = _conversation->getNode()->back();
	if(!continueNode) _updateNextNode();

}

void ConversationSystem::_updateNextNode() {
	// If there is another node, update the UI (if necessary) and make the node load its information
	bool cont = _conversation->nextNode();

	if(cont) {
		_updateUI();
		bool continueNode = _conversation->getNode()->execute();

		if(!continueNode) _updateNextNode();
	}
	// Otherwise, finish conversation
	else {
		finishConversation();
	}
}

void ConversationSystem::startConversation() {
	if(_conversation != nullptr) {
		_conversation->initConversation();
		// Change state
		std::cout << "Conv starting with hijack " << _conversation->hasInputHijacked() << "\n";


		if(_conversation->hasInputHijacked() == true) {
			_gameManager->pushState(_gameManager->getState(GameState::States::CONVERSATION));
		}
		// Load ui
		_ui->loadUI("Iz-Conversation", "ConversationBackground.xaml");
		auto uiComp = _ui->getUI("Iz-Conversation");

		auto ani = ((Noesis::UserControl*)uiComp->root.GetPtr())->FindName("enter");
		((Noesis::Storyboard *) ani)->Begin();

		// Establecer valores estaticos para interfaz de usuario
		_updateUI();
		bool continueNode = _conversation->getNode()->execute();
		if(!continueNode) _updateNextNode();
		std::cout << "Conv started\n";
	}
}

void ConversationSystem::_updateUI() {

	std::shared_ptr<ConversationNode> node = _conversation->getNode();
	std::string nodeXAML = "";
	Noesis::BaseComponent * ani;

	if(node->getNodeType() != _currentNodeType) {

		if(_currentNodeType != ConversationNode::NODE_EMPTY &&
				node->getNodeType() != ConversationNode::NodeType::NODE_ACTION &&
				node->getNodeType() != ConversationNode::NodeType::NODE_TRIGGER &&
				node->getNodeType() != ConversationNode::NodeType::NODE_LINK	) {
			auto uiComp = _ui->getUI(_nodeUIName);
			ani = ((Noesis::UserControl*)uiComp->root.GetPtr())->FindName("exit");
			((Noesis::Storyboard *) ani)->Begin();

			_ui->unloadUI(_nodeUIName);
		}

		switch(node->getNodeType()) {
			case ConversationNode::NodeType::NODE_TEXT:
				_nodeUIName = "NormalText";
				nodeXAML = "NormalText.xaml";
				break;
			case ConversationNode::NodeType::NODE_QUESTION:
				_nodeUIName = "QuestionText";
				nodeXAML = "QuestionText.xaml";
				break;
			case ConversationNode::NodeType::NODE_THOUGHT:
				_nodeUIName = "ThoughtText";
				nodeXAML = "ThoughtText.xaml";
				break;
			case ConversationNode::NodeType::NODE_EMPTY:
				break;
		}

		std::cout << "TYPE " << node->getNodeType() << " " << _nodeUIName << "\n";
		if(node->getNodeType() != ConversationNode::NodeType::NODE_ACTION &&
			node->getNodeType() != ConversationNode::NodeType::NODE_TRIGGER &&
			node->getNodeType() != ConversationNode::NodeType::NODE_LINK) {
			_ui->loadUI(_nodeUIName,nodeXAML);

			auto uiComp = _ui->getUI(_nodeUIName);
			ani = ((Noesis::UserControl*)uiComp->root.GetPtr())->FindName("enter");
			((Noesis::Storyboard *) ani)->Begin();

			ConversationNode::setTextRoot((Noesis::UserControl * )_ui->getUI(_nodeUIName)->root.GetPtr());

			_currentNodeType = node->getNodeType();
		}
	}

}

void ConversationSystem::finishConversation() {
	// Limpiar interfaz de usuario
	if(_currentNodeType != ConversationNode::NODE_EMPTY) {
		auto uiComp = _ui->getUI(_nodeUIName);
		auto ani = ((Noesis::UserControl*)uiComp->root.GetPtr())->FindName("exit");
		((Noesis::Storyboard *) ani)->Begin();

		_ui->unloadUI(_nodeUIName);
		_currentNodeType = ConversationNode::NODE_EMPTY;
	}

	auto uiComp = _ui->getUI("Iz-Conversation");
	auto ani = ((Noesis::UserControl*)uiComp->root.GetPtr())->FindName("exit");
	((Noesis::Storyboard *) ani)->Begin();

	ConversationNode::setTextRoot(nullptr);

	_ui->unloadUI("Iz-Conversation");

	if(_conversation->hasInputHijacked() == true) _gameManager->popState();

	_conversation->clear();
	_conversation = nullptr;
}

void ConversationSystem::setConversation(std::shared_ptr<Conversation> conversation) {
	_conversation = conversation;
}

void ConversationSystem::setConversation(Entity::ID id) {
	if(_componentMap.count(id) > 0) {
		_conversation = _componentMap[id]->_conversation->clone();
	}
}

void ConversationSystem::loadConversation(const std::string & conversationName) {
	std::shared_ptr<Conversation> conv = _gameManager->getConversationLoader()->loadConversation(conversationName);
	_conversation = conv;
}
