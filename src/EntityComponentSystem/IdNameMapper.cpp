#include "IdNameMapper.h"
#include <iostream>

IdNameMapper::IdNameMapper() : _allocatedIds(0), _nextId(0) {
	_maxIds = 50000;
}

IdNameMapper::IdNameMapper(const IdNameMapper & mapper) {
	_allocatedIds = mapper._allocatedIds;
	_maxIds = mapper._maxIds;
	_nextId = mapper._nextId;
	_idNameMap = mapper._idNameMap;
	_nameIdMap = mapper._nameIdMap;
}

IdNameMapper::IdNameMapper(IdNameMapper && mapper) {
	_allocatedIds = mapper._allocatedIds;
	_maxIds = mapper._maxIds;
	_nextId = mapper._nextId;
	_idNameMap = mapper._idNameMap;
	_nameIdMap = mapper._nameIdMap;
}

IdNameMapper::~IdNameMapper() {}

IdNameMapper & IdNameMapper::operator=(const IdNameMapper & mapper) {
	_allocatedIds = mapper._allocatedIds;
	_maxIds = mapper._maxIds;
	_nextId = mapper._nextId;
	_idNameMap = mapper._idNameMap;
	_nameIdMap = mapper._nameIdMap;

	return *this;
}

const int IdNameMapper::createId(const std::string & name) {
	int tryNumber = 0;
	bool found = false;

	assert(_allocatedIds < _maxIds);

	// Buscamos el primer id que este libre

	while(tryNumber < _maxIds && !found) {

		if(_idNameMap.count(tryNumber) == 0) {
			found = true;
		}
		else {
			tryNumber++;
		}

	}

	/* Si no hemos encontrado un id libre significa que tenemos creadas mas entidades del maximo.
	 * Si existe alguna entrada por ese nombre significa que el nombre de la entidad no es unico.
	 **/
	assert(found);
	//std::cout << name << std::endl;
	assert(_nameIdMap.count(name) == 0);

	//const std::string n(name);
	_idNameMap.insert(std::make_pair(tryNumber, name));
	//_idNameMap[tryNumber] = n;

	_nameIdMap.insert(std::make_pair(name, tryNumber));
	//_nameIdMap[name] = tryNumber;
	_allocatedIds++;

	return tryNumber;
}

const int IdNameMapper::nameToId(const std::string & name) {
	if(_nameIdMap.count(name) == 1) {
		return _nameIdMap[name];
	}
	else {
		return -1;
	}
}

const std::string & IdNameMapper::idToName(const int id) {
	if(_idNameMap.count(id) == 1) {
			return _idNameMap[id];
	}
	else {
		return "*";
	}
}

void IdNameMapper::removeId(const int id) {
	//_idNameMap.erase()
	const std::string name = _idNameMap[id];
	_idNameMap.erase(_idNameMap.find(id));
	_nameIdMap.erase(_nameIdMap.find(name));
}
