#include "AnimationComponent.h"

AnimationComponent::AnimationComponent(Ogre::Entity* entity): _entity(entity){
	Ogre::AnimationStateSet* set = _entity->getAllAnimationStates();
	Ogre::AnimationStateIterator it = set->getAnimationStateIterator();
	// Inicializamos los AnimationState de la entidad
	while(it.hasMoreElements()) {
		Ogre::AnimationState* anim = it.getNext();
		anim->setEnabled(false);
		anim->setWeight(0);
		anim->setTimePosition(0);
	}
	_source = nullptr; _target = nullptr; _blend = false; _loop = false; _duration = 0.0; _timeLeft = 0.0; _complete = true;

}

AnimationComponent::AnimationComponent(const AnimationComponent & animationComponent){
	_entity = animationComponent._entity;
	_source = animationComponent._source;
	_target = animationComponent._target;
	_blend = animationComponent._blend;
	_loop = animationComponent._loop;
	_duration = animationComponent._duration;
	_timeLeft = animationComponent._timeLeft;
	_complete = animationComponent._complete;
}

AnimationComponent::AnimationComponent(const AnimationComponent && animationComponent){
	_entity = animationComponent._entity;
	_source = animationComponent._source;
	_target = animationComponent._target;
	_blend = animationComponent._blend;
	_loop = animationComponent._loop;
	_duration = animationComponent._duration;
	_timeLeft = animationComponent._timeLeft;
	_complete = animationComponent._complete;
}

AnimationComponent::~AnimationComponent(){

}

AnimationComponent & AnimationComponent::operator= (const AnimationComponent & animationComponent){
	_entity = animationComponent._entity;
	_source = animationComponent._source;
	_target = animationComponent._target;
	_blend = animationComponent._blend;
	_loop = animationComponent._loop;
	_duration = animationComponent._duration;
	_timeLeft = animationComponent._timeLeft;
	_complete = animationComponent._complete;

	return  *this;
}

