#include "GraphicComponent.h"

#include "Ogre.h"

std::shared_ptr<Ogre::SceneManager> GraphicComponent::_sceneManager;

GraphicComponent::GraphicComponent() { assert(_sceneManager != nullptr); }

GraphicComponent::GraphicComponent(const GraphicComponent & graphics) : _entities(graphics._entities) {}

GraphicComponent::GraphicComponent(const GraphicComponent && graphics) : _entities(graphics._entities){}

GraphicComponent::~GraphicComponent(){
	for(auto it = _entities.begin(); it != _entities.end(); it++) {
		it->second->getParentSceneNode()->detachObject(it->second);
		_sceneManager->destroyEntity(it->second);
	}
}

GraphicComponent & GraphicComponent::operator= (const GraphicComponent & graphics){
	_entities = graphics._entities;
	return  *this;
}

void GraphicComponent::addGraphic(const std::string & name, const std::string & mesh, Ogre::SceneNode * node, bool castShadows) {

	// Problema con Ogre1.10 exporter
	//Load Mesh
	Ogre::MeshPtr mMesh = Ogre::MeshManager::getSingleton().load(mesh, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	//Build Edge List
	mMesh->buildEdgeList();

	Ogre::Entity * entity = _sceneManager->createEntity(name, mMesh);

	if(name == "Plane") {
		entity->setMaterialName("Ground");
	}

	node->attachObject(entity);
	entity->setCastShadows(castShadows);
	_entities[name] = entity;
}

void GraphicComponent::addStaticGraphic(const std::string & name, const std::string & mesh, bool castShadows) {
	Ogre::StaticGeometry * staticGeom = _sceneManager->createStaticGeometry(name);
	Ogre::Entity * staticEnt = _sceneManager->createEntity(name, mesh);

	staticEnt->setCastShadows(castShadows);

	staticGeom->addEntity(staticEnt, Ogre::Vector3(0,0,0));
	staticGeom->build();  // Operacion para construir la geometria

}

void GraphicComponent::removeGraphic(const std::string & name) {
	Ogre::Entity * entity = _entities[name];
	//std::remove()
	entity->getParentSceneNode()->detachObject(entity);
	_sceneManager->destroyEntity(entity);

}

void GraphicComponent::setSceneManager(std::shared_ptr<Ogre::SceneManager> sceneManager) {
	_sceneManager = sceneManager;
}
