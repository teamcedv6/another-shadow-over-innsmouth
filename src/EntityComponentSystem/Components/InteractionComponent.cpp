#include "InteractionComponent.h"

InteractionComponent::InteractionComponent() : _interactionDistance(0.5), _activateDistance(5), _type(InteractionType::OBJECT_NON_PICKABLE) { }

InteractionComponent::InteractionComponent(InteractionType type, Ogre::SceneNode * node, float activateDistance, float interactionDistance) :
		_type(type), _node(node), _activateDistance(activateDistance), _interactionDistance(interactionDistance) {}

InteractionComponent::InteractionComponent(const InteractionComponent & interactionComponent) {
	_type = interactionComponent._type;
	_node = interactionComponent._node;
	_activateDistance = interactionComponent._activateDistance;
	_interactionDistance = interactionComponent._interactionDistance;
}

InteractionComponent::InteractionComponent(const InteractionComponent && interactionComponent) {
	_type = interactionComponent._type;
	_node = interactionComponent._node;
	_activateDistance = interactionComponent._activateDistance;
	_interactionDistance = interactionComponent._interactionDistance;
}

 InteractionComponent & InteractionComponent::operator= (const InteractionComponent & interactionComponent) {
	 _type = interactionComponent._type;
	_node = interactionComponent._node;
	_activateDistance = interactionComponent._activateDistance;
	_interactionDistance = interactionComponent._interactionDistance;
	return *this;
 }

InteractionComponent::~InteractionComponent() {}

