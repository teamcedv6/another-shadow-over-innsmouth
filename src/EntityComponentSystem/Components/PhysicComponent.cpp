#include "PhysicComponent.h"

PhysicComponent::PhysicComponent() : _rigidBody(nullptr) {}

PhysicComponent::PhysicComponent(const PhysicComponent & physicComponent) :
		_rigidBody(physicComponent._rigidBody){}

PhysicComponent::PhysicComponent(const PhysicComponent && physicComponent) :
		_rigidBody(physicComponent._rigidBody){}

PhysicComponent::~PhysicComponent(){

}

PhysicComponent & PhysicComponent::operator= (const PhysicComponent & physicComponent){
	return  *this;
}

