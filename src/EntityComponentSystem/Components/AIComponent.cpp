#include "AIComponent.h"

AIComponent::AIComponent(Ogre::Vector3 position) {
	_agentID = -1;
    _agent = nullptr;
    _eyes = nullptr;
    _initPosition = position;
    _actualState = AIComponent::IDLE;
}

AIComponent::AIComponent(const AIComponent & aiComponent) {
	_agentID = aiComponent._agentID;
	_agent = aiComponent._agent;
	_initPosition = aiComponent._initPosition;
	_initRandomPosition = aiComponent._initRandomPosition;
	_patrolPoints = aiComponent._patrolPoints;
	_nextPosition = aiComponent._nextPosition;
	_actualState = aiComponent._actualState;
}

AIComponent::AIComponent(const AIComponent && aiComponent) {
	_agentID = aiComponent._agentID;
	_agent = aiComponent._agent;
	_initPosition = aiComponent._initPosition;
	_initRandomPosition = aiComponent._initRandomPosition;
	_patrolPoints = aiComponent._patrolPoints;
	_nextPosition = aiComponent._nextPosition;
	_actualState = aiComponent._actualState;
}

AIComponent::~AIComponent() {}

AIComponent & AIComponent::operator= (const AIComponent & aiComponent) {
	_agentID = aiComponent._agentID;
	_agent = aiComponent._agent;
	_initPosition = aiComponent._initPosition;
	_initRandomPosition = aiComponent._initRandomPosition;
	_patrolPoints = aiComponent._patrolPoints;
	_nextPosition = aiComponent._nextPosition;
	_actualState = aiComponent._actualState;

	return *this;
}

int AIComponent::getId() {
	return _agentID;
}

const dtCrowdAgent* AIComponent::getAgent() {
	return _agent;
}

Ogre::Vector3 AIComponent::getInitPosition() {
	return _initPosition;
}

Ogre::Vector3 AIComponent::getInitRandomPosition() {
	return _initPosition;
}

std::vector<Ogre::Vector3> AIComponent::getPatrolPositions() {
	return _patrolPoints;
}

Ogre::Vector3 AIComponent::getNextPosition() {
	return _nextPosition;
}

AIComponent::State AIComponent::getActualState() {
	return _actualState;
}

std::shared_ptr<Ogre::Camera> AIComponent::getEyes() {
	return _eyes;
}

Ogre::Vector3 AIComponent::getLastView() {
	return _lastView;
}

void AIComponent::setId(int agentId) {
	_agentID = agentId;
}

void AIComponent::setAgent(const dtCrowdAgent* agent) {
	_agent = agent;
}

void AIComponent::setInitPosition(Ogre::Vector3 position) {
	_initPosition = position;
}

void AIComponent::setInitRandomPosition(Ogre::Vector3 position) {
	_initPosition = position;
}

void AIComponent::setPatrolPositions(Ogre::Vector3 newPoint) {
	_patrolPoints.push_back(newPoint);
}

void AIComponent::setNextPosition(Ogre::Vector3 nextPosition) {
	_nextPosition = nextPosition;
}

void AIComponent::setActualState(AIComponent::State newState) {
	_actualState = newState;
}

void AIComponent::setEyes(std::shared_ptr<Ogre::Camera> eyes) {
	_eyes = eyes;
}

void AIComponent::setLastView(Ogre::Vector3 position) {
	_lastView = position;
}
