#include "ConversationComponent.h"

ConversationComponent::ConversationComponent() {  }

ConversationComponent::ConversationComponent(std::shared_ptr<Conversation> conversation) : _conversation(conversation){

}

ConversationComponent::ConversationComponent(const ConversationComponent & conversationComponent) {
	_conversation = conversationComponent._conversation;
}

ConversationComponent::ConversationComponent(const ConversationComponent && conversationComponent) {
	_conversation = conversationComponent._conversation;
}

ConversationComponent::~ConversationComponent() {}

