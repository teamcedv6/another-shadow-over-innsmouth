#include "CameraComponent.h"

std::shared_ptr<Ogre::SceneManager> CameraComponent::_sceneManager;

CameraComponent::CameraComponent() { assert(false); }

CameraComponent::CameraComponent(const std::string & name, Ogre::SceneNode * node) {
	assert(_sceneManager != nullptr);
	_camera = std::shared_ptr<Ogre::Camera>(_sceneManager->createCamera(name));
	_camera->setPosition(0,0,0);
	_camera->setOrientation(Ogre::Quaternion(1, -1, 0, 0));
	node->attachObject(_camera.get());
}

CameraComponent::CameraComponent(const CameraComponent & camera) {
	_camera = camera._camera;
}

CameraComponent::CameraComponent(const CameraComponent && camera) {
	_camera = camera._camera;
}

CameraComponent::~CameraComponent() {}

void CameraComponent::setSceneManager(std::shared_ptr<Ogre::SceneManager> sceneManager){
	_sceneManager = sceneManager;
}
