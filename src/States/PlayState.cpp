 #include "PlayState.h"
#include "MainMenuState.h"

void PlayState::setLose(bool lose) {
	_lose = lose;
}

void PlayState::aiDebug() {
	if(_aiDebug) {
		_aiDebug = false;
		_ai->changeDebug(_aiDebug);
	} else {
		_aiDebug = true;
		_ai->changeDebug(_aiDebug);
	}
}

void PlayState::enter () {
	_nodes = _gameManager->getNodeManager();
	_actions = _gameManager->getActionsManager();
	_move = _gameManager->getMovementSystem();
	_cmSystem = _gameManager->getCameraSystem();
	_phSystem = _gameManager->getPhysicsSystem();
	_aSystem = _gameManager->getAnimationSystem();
	_grSystem = _gameManager->getGraphicsSystem();
	_ui = _gameManager->getUserInterfaceSystem();
	_interactionSystem = _gameManager->getInteractionSystem();
	_ai = _gameManager->getAISystem();
	_aiDebug = _gameManager->getConfig()->getAiConfig()->debug;

	_player = _gameManager->getConfig()->getAiConfig()->player;

	auto sM = _gameManager->getSceneManager();

	_gameLogic = _gameManager->getGameLogic();

	_finish = false;

	if(_character){
		_cmSystem->setActiveCamera(_gameManager->getMapper()->nameToId("tunnel"));
		// HUNT
		_ai->resetEnd(true);
		setDaytime(0);
		_gameManager->getSoundManager()->selectResource(SoundManager::FX,"rain",SoundManager::STOP,60,-1);
	} else {

		// Cargamos la escena
		auto dotSceneLoader = std::make_shared<Ogre::DotSceneLoader>(_gameManager,_gameManager->getConfig());
		dotSceneLoader->parseDotScene("innsmouth.scene","ASOI",sM);

		// Shadow, Background, Ambientlight
		sM->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
		_cmSystem->setActiveCamera(_gameManager->getMapper()->nameToId("tunnel"));
		auto camera = _cmSystem->getActiveCamera();

		// Sun
		_sun = _gameManager->getSceneManager()->createParticleSystem("SunParticle", "sun");
		auto sunNode = _nodes->createNode("SunParticleNode");
		_nodes->attachNode(sunNode,"root");
		sunNode->attachObject(_sun);

		// Moon
		_moon = _gameManager->getSceneManager()->createParticleSystem("MoonParticle", "moon");
		auto moonNode = _nodes->createNode("MoonParticleNode");
		_nodes->attachNode(moonNode,"root");
		moonNode->attachObject(_moon);
		moonNode->setVisible(false);

		_rain = nullptr;

		_ai->createOgreCrowd(_gameManager->getNavEntities(GameManager::NAV_MESH),_gameManager->getNavEntities(GameManager::OBSTACLES));
		_ai->createAgents();

		_ui->deactivateCursor();

		_character = _gameManager->getEntity(_player);


		Ogre::SceneNode * node = _nodes->getNode(_player);

		_interactionSystem->setMainCharacter(node);
		_interactionSystem->startInterface();

		_interactionSystem->conversations = _gameManager->getConversationSystem();

		setDaytime(0);
	}


}

void PlayState::exit () {
	_interactionSystem->setMainCharacter(nullptr);
	_interactionSystem->clearInterface();
	delete _sun;
	delete _moon;
	delete _rain;

	//_ui->unloadUI("GameUI");
}

void PlayState::pause() {

}

void PlayState::resume() {
	// Do any pending interaction (give or use object)
	_interactionSystem->resume();
}

bool PlayState::frameStarted (const Ogre::FrameEvent& evt) {
	auto deltaTime = evt.timeSinceLastFrame;

	_moveMainCharacter();

	_move->update(deltaTime);
	_aSystem->update(deltaTime);
	_interactionSystem->update(deltaTime);
	_ui->update(deltaTime);
	_actions->update(deltaTime);
	_ai->update(deltaTime);

	_time += deltaTime;
	if((_time >= 1) && _rain) {
		_rain->fastForward(_time);
		_time = 0;
	}

	return true;
}

bool PlayState::frameEnded (const Ogre::FrameEvent& evt) {
	//auto deltaTime = evt.timeSinceLastFrame;
    //_phSystem->update(deltaTime);
	
	return true;
}

void PlayState::movePoV(const OIS::MouseEvent & e) {
	float rotx = e.state.X.rel * -1;
	float roty = e.state.Y.rel * -1;

	shared_ptr<Action> action = std::make_shared<Action>();
	shared_ptr<MovePoV> pov = std::make_shared<MovePoV>( _fov,_nodes, rotx, roty, 0);
	action->addSubaction(pov);
	std::vector<std::pair<shared_ptr<DistanceTrigger>, shared_ptr<Action>>> _shortestDistanceEvents;
	_actions->addAction(action);
}

void PlayState::_moveMainCharacter() {
	if(_movingX || _movingY) {
		// Transform from quaternion to euelr angles as defined in http://www.ogre3d.org/tikiwiki/Euler+Angle+Class
		Ogre::Matrix3 rotmat;
		Ogre::Radian x, y, z;

		_cmSystem->getActiveCamera()->getParentNode()->getOrientation().ToRotationMatrix(rotmat);

		rotmat.ToEulerAnglesXYZ(x, y, z);

		const Ogre::Vector3 & d = Ogre::Vector3(_vX, 0, _vY);
		const float vFactor = 10;
		Ogre::Quaternion q(-z, Ogre::Vector3::UNIT_Y);
		Ogre::Vector3 pointMain = Ogre::Vector3(q * d /** vFactor*/);

		//_move->move(_character->getId(), "mainCharacter", q * d * vFactor);

		Ogre::Vector3 newPosition = Ogre::Vector3(q * d ); // * vFactor
		auto agentMain = _gameManager->getMapper()->nameToId(_player);
		auto actualPosition = _gameManager->getSceneManager()->getSceneNode(_player)->getPosition();

		_ai->setGotoAgent(agentMain,newPosition + actualPosition);

		if(!_previouslyMoving) {
		_aSystem->runAnimation(_character->getId(), AnimationSystem::WALK, 0.5, false, true);
			_previouslyMoving = true;
		}

	}
	else {
		if(_previouslyMoving) {
		_aSystem->runAnimation(_character->getId(), AnimationSystem::IDLE, 0.5, true, true);
			_previouslyMoving = false;
		}
	}

}

void PlayState::moveMainCharacterKeyboard(InputBindings::PushState state, int x, int y) {
	if(state == InputBindings::PushState::PUSH) {
		if(x != 0) {
			_movingX = true;
			_vX = x;
		}
		if(y != 0 ) {
			_movingY = true;
			_vY = y;
		}
	}
	else {
		if(x != 0) {
			_movingX = false;
			_vX = 0;
		}
		if(y != 0) {
			_movingY = false;
			_vY = 0;
		}

	}
}

void PlayState::moveMainCharacterGamepad(int id, const OIS::JoyStickEvent & e, InputBindings::PushState state) {
	/* Range goes from -32768 to 32767. 128 is equals to no movement
	 * So it's mapped to [-32896 - 32639] by substracting 128, then this value got normalized to -1.007 - 1,
	 * because it's getting divided by 32639. Every value under -1 is rounded up to -1.
	 */
	if(id == 0 | id == 1) {
		float vGamePad = e.state.mAxes[id].abs - 128;
		vGamePad = vGamePad / 32639.0;
		if (vGamePad < -1) vGamePad = -1;

		if(id == 0) {
			if(vGamePad == 0)
				_movingX = false;
			else
				_movingX = true;

			_vX = vGamePad;
		}
		else {
			if(vGamePad == 0)
				_movingY = false;
			else
				_movingY = true;

			_vY = vGamePad;
		}

	}


}

void PlayState::openInventory(InputBindings::PushState state) {
	if(state == InputBindings::PushState::RELEASE) {
		_gameManager->pushState(_gameManager->getState(GameState::States::INVENTORY));
	}
}

void PlayState::actionPush(InputBindings::PushState state, int n) {
	if(state == InputBindings::RELEASE) {
		if(n >= 0 && n <= 3)
		_interactionSystem->action(n);
	}
}

void PlayState::exitGame(InputBindings::PushState pushState) {

	if(pushState == InputBindings::PushState::RELEASE){
		_exitGame = true;
		//_ui->terminateUI();
		//_aSystem->runAnimation(_character->getId(), AnimationSystem::IDLE, 0, false, false);
	}
}

void PlayState::setDaytime(int n) {

	auto interlude = _gameManager->getState(GameState::States::INTERLUDE);
	((InterludeState *)interlude.get())->setDayTime(n);

	bool go = true;
	if( (n==3) && !_finish)
		go = false;

	// Para saltarnos el trigger de la entrada si no es de noche
	if(go)
		_gameManager->pushState(interlude);

	auto agentMain = _gameManager->getMapper()->nameToId(_player);
	_ai->setIdleAgent(agentMain);


	auto drunk = _gameManager->getMapper()->nameToId("drunk");
	_ai->setIdleAgent(drunk);

	auto seller = _gameManager->getMapper()->nameToId("seller");
	_ai->setIdleAgent(seller);


	auto sM = _gameManager->getSceneManager();
	auto sunNode = _nodes->getNode("SunParticleNode");
	auto moonNode = _nodes->getNode("MoonParticleNode");

	if(n == 0) {
		_gameManager->getSoundManager()->selectResource(SoundManager::MUSIC,"day",SoundManager::PLAY,80,-1);
		// Dia
		sM->getLight("Sun")->setDiffuseColour(Ogre::ColourValue(0.4, 0.4, 0.2));
		sM->getLight("Sun")->setSpecularColour(Ogre::ColourValue(0.4, 0.4, 0.2));
		sM->setAmbientLight(Ogre::ColourValue(0.75, 0.75, 0.75));
		sM->getLight("Sun")->setDirection(Ogre::Vector3(-1, -1,0));
		sunNode->setPosition(372.89786,250,-135);
		sM->setSkyDome(true, "skyDome",5,8);
		_ai->huntMode(false);
		_ai->setAllState(AIComponent::PATROL,_gameManager->getSceneManager()->getSceneNode(_player)->getPosition());

	}
	else if(n == 1) {
		_gameManager->getSoundManager()->selectResource(SoundManager::MUSIC,"afternoon",SoundManager::PLAY,80,-1);
		// Tarde
		sM->getLight("Sun")->setDiffuseColour(Ogre::ColourValue(0.4, 0, 0));
		sM->getLight("Sun")->setSpecularColour(Ogre::ColourValue(0.4, 0, 0));
		sM->setAmbientLight(Ogre::ColourValue(0.6, 0.6,0.6));
		sM->getLight("Sun")->setDirection(Ogre::Vector3(-1, -1, 0));
		sM->setSkyDome(true, "skyDome",5,8);
		sunNode->setPosition(372.89786,55.46298,-135);
		
		_ai->huntMode(false);
		_ai->setAllState(AIComponent::RANDOM_PATROL,Ogre::Vector3::ZERO);
	}
	else if(n == 2) {
		_gameManager->getSoundManager()->selectResource(SoundManager::MUSIC,"night",SoundManager::PLAY,80,-1);
		// Noche
		sM->getLight("Sun")->setDiffuseColour(Ogre::ColourValue(0, 0, 0.9));
		sM->getLight("Sun")->setSpecularColour(Ogre::ColourValue(0, 0, 0.9));
		sM->setAmbientLight(Ogre::ColourValue(0.4, 0.4,0.4));
		sM->getLight("Sun")->setDirection(Ogre::Vector3(-1, -1,0));
		_cmSystem->getActiveCamera()->getViewport()->setBackgroundColour(Ogre::ColourValue(0,0,0));
		sM->setSkyDome(false, "skyDome");
		_cmSystem->getActiveCamera()->getViewport()->setBackgroundColour(Ogre::ColourValue(0,0,0));
		sunNode->setVisible(false);
		moonNode->setVisible(true);
		moonNode->setPosition(372.89786,13.17219,-178.575);

		// Create a rainstorm
		if(_rain == nullptr) {
			_rain = sM->createParticleSystem("rain", "Examples/Rain");
			auto rainNode = _nodes->createNode("rainNode");
			rainNode->attachObject(_rain);
			_nodes->attachNode(rainNode,"root");
			rainNode->translate(rainNode->getPosition() + Ogre::Vector3(0,40,0));
			// Fast-forward the rain so it looks more natural
			_gameManager->getSoundManager()->selectResource(SoundManager::FX,"rain",SoundManager::PLAY,60,-1);
		}

		_cmSystem->setActiveCamera(_gameManager->getMapper()->nameToId("districtBig8"));


		// HUNT
		//_ai->resetEnd(false);
		_finish = true;
		_ai->huntMode(true);
		_ai->setAllState(AIComponent::RANDOM_PATROL,Ogre::Vector3::ZERO);

	}
	else if((n == 3) && _finish) {
		_gameManager->changeState(_gameManager->getState(GameState::MAIN_MENU));
	}
	else if(n == 4) {

	}
}

