#include "MainMenuState.h"

void MainMenuState::enter () {


	_ui = _gameManager->getUserInterfaceSystem();
	_gameManager->getSoundManager()->selectResource(SoundManager::MUSIC,"day",SoundManager::PLAY,80,-1);
	_loadGui();
}

void MainMenuState::exit() {
	//_ui->terminateUI();
}

void MainMenuState::pause () {
	_ui->unloadUI("MainMenu");
}

void MainMenuState::resume () {
	_loadGui();
}

void MainMenuState::_loadGui() {
	_ui->loadUI("Title", "Title.xaml");
	Noesis::UserControl * root = (Noesis::UserControl *)_ui->getUI("Title")->root.GetPtr();
	_anim = (Noesis::Storyboard *) root->FindName("focus");
	_anim->Begin();
	_timer = 1.31;
}

bool MainMenuState::frameStarted (const Ogre::FrameEvent& evt)  {

	auto deltaTime = evt.timeSinceLastFrame;
	_timer -= deltaTime;

	_ui->update(deltaTime);

	if(_timer <= 0) {
		_anim->Begin();
		_timer = 1.31;
	}

//	_gameManager->changeState(_gameManager->getState(States::PLAY));

	return true;
}

bool MainMenuState::frameEnded (const Ogre::FrameEvent& evt) {
	if (_exitGame) {
		_ui->terminateUI();
		return false;
	}


	return true;
}

void MainMenuState::goAIState(InputBindings::PushState pushState) {

	if(pushState == InputBindings::PushState::RELEASE){
		_gameManager->changeState(_gameManager->getState(States::AI));
	}
}

void MainMenuState::startGame(InputBindings::PushState pushState) {

	if(pushState == InputBindings::PushState::RELEASE){
		_ui->unloadUI("Title");
		_gameManager->changeState(_gameManager->getState(States::PLAY));
	}
}

void MainMenuState::exitGame(InputBindings::PushState pushState) {

	if(pushState == InputBindings::PushState::RELEASE){
		//_gameManager->changeState(_gameManager->getState(States::PLAY));
		_exitGame = true;
	}

}

