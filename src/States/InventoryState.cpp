#include "InventoryState.h"

// State Actions
void InventoryState::enter () {
	_ui = _gameManager->getUserInterfaceSystem();
	_conversations = _gameManager->getConversationSystem();
	_selectedRow = 0;
	_selectedColumn = 0;
	_selected = nullptr;

	_inventory = _gameManager->getInventory();

	_ui->loadUI("Inventory", "Inventory.xaml");
	_rootUI = (Noesis::UserControl*) _ui->getUI("Inventory")->root.GetPtr();

	Noesis::Storyboard *_enterAnim = (Noesis::Storyboard *)_rootUI->FindName("enter");

	_waitTime = 0.75;
	_exitState = false;

	_reloadUI();

	_enterAnim->Begin();
}

void InventoryState::exit () {
	_rootUI = nullptr;
	_ui->unloadUI("Inventory");
}

void InventoryState::pause () {

}

void InventoryState::resume () {

}

// Game Loop
bool InventoryState::frameStarted (const Ogre::FrameEvent& evt) {
	auto deltaTime = evt.timeSinceLastFrame;

	if(_waitTime > 0) { _waitTime -= deltaTime; }
	else {
		if(_exitState) {
			_gameManager->popState();
		}
	}

	_ui->update(deltaTime);

	return true;
}

bool InventoryState::frameEnded (const Ogre::FrameEvent& evt) {
	return true;
}

void InventoryState::up(InputBindings::PushState pushState) {
	if(_waitTime <= 0 && !_exitState) {
		if(_selectedRow - 1 >= 0) {
			_unfocus();
			_selectedRow--;
			_focus();
			_waitTime = 0.3;
		}
	}
}

void InventoryState::left(InputBindings::PushState pushState) {
	if(_waitTime <= 0 && !_exitState) {
		if(_selectedColumn - 1 >= 0) {
			_unfocus();
			_selectedColumn--;
			_focus();
			_waitTime = 0.3;
		}
	}
}

void InventoryState::right(InputBindings::PushState pushState) {
	if(_waitTime <= 0 && !_exitState &&
			(_selectedRow * _rowLenght + (_selectedColumn + 1) < _nObjects)) {
		if(_selectedColumn + 1 < _rowLenght) {
			_unfocus();
			_selectedColumn++;
			_focus();
			_waitTime = 0.3;
		}
	}
}

void InventoryState::down(InputBindings::PushState pushState) {
	if(_waitTime <= 0 && !_exitState) {
		if(_selectedRow + 1 < _rowNumber &&
				((_selectedRow + 1) * _rowLenght + _selectedColumn < _nObjects)) {
			_unfocus();
			_selectedRow++;
			_focus();
			_waitTime = 0.3;
		}
	}
}

void InventoryState::look(InputBindings::PushState pushState) {
	if(_nObjects > 0 && _waitTime <= 0 && !_exitState) {
		std::stringstream ss;
		ss << "look_" << _inventory->getAllObjects()->at(_selectedRow * _rowLenght + _selectedColumn)->name << ".xml";
		_conversations->loadConversation(ss.str());
		_conversations->startConversation();
	}
}

void InventoryState::select(InputBindings::PushState pushState) {
	if(_nObjects > 0 && _waitTime <= 0 && !_exitState) {
		_selected = _inventory->getAllObjects()->at(_selectedRow * _rowLenght + _selectedColumn);
		_waitTime = 0.75;
		_exitState = true;
		Noesis::Storyboard * exitAnim = (Noesis::Storyboard *)_rootUI->FindName("exit");
		exitAnim->Begin();
	}
}

void InventoryState::back(InputBindings::PushState pushState) {
	if(_waitTime <= 0 && !_exitState) {
		_waitTime = 0.75;
		_exitState = true;
		Noesis::Storyboard * exitAnim = (Noesis::Storyboard *)_rootUI->FindName("exit");
		exitAnim->Begin();
	}
}

void InventoryState::gamePadMove(int id, const OIS::JoyStickEvent & e, InputBindings::PushState pushState) {
	int value = e.state.mAxes[id].abs;

	if(value >= 17000) {
		if(id == 0)
			down(InputBindings::PushState::RELEASE);
		else if(id == 1)
			right(InputBindings::PushState::RELEASE);

	}
	else if(value <= -17000) {
		if(id == 0)
			up(InputBindings::PushState::RELEASE);
		else if(id == 1)
			left(InputBindings::PushState::RELEASE);
	}
}

void InventoryState::setItemSelectionMode(bool selection,  SelectionType type) {
	_itemSelection = selection;
	_selectionType = type;
}

std::shared_ptr<Inventory::InventoryObject> InventoryState::getSelectedItem() {
	shared_ptr<Inventory::InventoryObject> tmp = _selected;
	_selected = nullptr;
	_itemSelection = false;
	return tmp;
}

void InventoryState::_focus() {
	std::stringstream ss;
	ss << "focus" << _selectedRow << _selectedColumn;

	// Selection marks
	Noesis::UserControl * us1 = (Noesis::UserControl*)_rootUI->FindName("selected1");
	Noesis::UserControl * us2 = (Noesis::UserControl*)_rootUI->FindName("selected2");

	((Noesis::Grid*)_rootUI)->SetRow(us1, 2 * _selectedRow + 1);
	((Noesis::Grid*)_rootUI)->SetColumn(us1, 2 * _selectedColumn + 1);

	((Noesis::Grid*)_rootUI)->SetRow(us2, 2 * _selectedRow + 1);
	((Noesis::Grid*)_rootUI)->SetColumn(us2, 2 * _selectedColumn + 1);

	Noesis::Storyboard * anim = (Noesis::Storyboard*)_rootUI->FindName(ss.str().c_str());
	anim->Begin();
}

void InventoryState::_unfocus() {
	std::stringstream ss;
	ss << "unfocus" << _selectedRow << _selectedColumn;

	Noesis::Storyboard * anim = (Noesis::Storyboard*)_rootUI->FindName(ss.str().c_str());
	anim->Begin();
}

void InventoryState::_reloadUI() {
	auto _objects = _inventory->getAllObjects();
	_nObjects = _objects->size();

	if(_nObjects == 0) {
		Noesis::UserControl * us1 = (Noesis::UserControl*)_rootUI->FindName("selected1");
		Noesis::UserControl * us2 = (Noesis::UserControl*)_rootUI->FindName("selected2");

		us1->SetVisibility(Noesis::Visibility::Visibility_Hidden);
		us2->SetVisibility(Noesis::Visibility::Visibility_Hidden);
	}

	for(int i = 0; i < _rowNumber; i++) {
		for(int j = 0; j < _rowLenght; j++) {
			std::stringstream ss;
			ss << "item" << i << j;
			Noesis::Grid * itemUI = (Noesis::Grid*)_rootUI->FindName(ss.str().c_str());

			// if there's an object for this position (we haven't loop over all inventory objects yet
			if(i * _rowLenght + j < _nObjects) {
				auto inventoryObject = _objects->at(i * _rowLenght + j);

				itemUI->SetVisibility(Noesis::Visibility::Visibility_Visible);

				// Fulfill this item
				std::stringstream ss2;
				std::stringstream ss3;
				std::stringstream ss4;
				ss2 << "image" << i << j;
				ss3 << "name" << i << j;
				ss4 << "Assets/" << inventoryObject->picture;

				Noesis::Image * itemImage = (Noesis::Image*) _rootUI->FindName(ss2.str().c_str());
				Noesis::ImageSource * source = new Noesis::TextureSource(ss4.str().c_str());
				itemImage->SetSource(source);

				Noesis::TextBlock * itemName = (Noesis::TextBlock *) _rootUI->FindName(ss3.str().c_str());

				itemName->SetText(inventoryObject->name.c_str());
			}
			else {
				itemUI->SetVisibility(Noesis::Visibility::Visibility_Hidden);
			}
		}

	}
}
