#include "ScoreState.h"

void ScoreState::enter () {

	_gameLogic = _gameManager->getGameLogic();
	_ui = _gameManager->getUserInterfaceSystem();
	_ui->activateCursor();

	int type = _type;
	switch ( type ) {
		// De MainState a LevelState
		case READ:
			loadScore();
		  break;
		// De LevelState a PlayState
		case WRITE:
			writeScore();
		  break;

		default:
		  std::cout << "selección erronea\n";
		  break;
	  }

}



void ScoreState::exit () {
	_ui->unloadUI("Score");
}

void ScoreState::pause() {

}

void ScoreState::resume() {

}

void ScoreState::setType(ScoreType type) {
	_type = type;
}

bool ScoreState::frameStarted (const Ogre::FrameEvent& evt) {
	auto deltaTime = evt.timeSinceLastFrame;
		_ui->update(deltaTime);
	return true;

}

bool ScoreState::frameEnded (const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void ScoreState::loadScore() {

	// Cargamos la interfaz
	_ui->loadUI("Score", "Score.xaml");

	// Load  menu events handlers for each button (actions focus, unfocus and click
	auto uiRoot = _ui->getUI("Score")->root.GetPtr();
	auto button = ((Noesis::UserControl*)uiRoot)->FindName("backButton");
	((Noesis::Button*)button)->MouseDown() += Noesis::MakeDelegate(this, &ScoreState::menuClick);

	_proccessScore();

}

void ScoreState::writeScore() {


	// Cargamos la interfaz
	_ui->loadUI("PlayerName", "PlayerName.xaml");

	// Load  menu events handlers for each button (actions focus, unfocus and click
	auto uiRoot = _ui->getUI("PlayerName")->root.GetPtr();
	auto button = ((Noesis::UserControl*)uiRoot)->FindName("okButton");
	((Noesis::Button*)button)->MouseDown() += Noesis::MakeDelegate(this, &ScoreState::menuClick);
}

void ScoreState::menuClick(Noesis::BaseComponent * sender, const Noesis::MouseButtonEventArgs & args) {

	//if(args.)
	std::stringstream ss;
	const std::string & btnName = ((Noesis::UserControl*)sender)->GetName();


	// Click sound
	auto soundManager = _gameManager->getSoundManager();
	//soundManager->selectResource(SoundManager::FX,"click",SoundManager::PLAY,50);

	if(btnName == "backButton") {
		popState(_gameManager);
	}
	else if(btnName == "okButton") {

		// Obtenemos el nombre y rescatamos los datos para el score
		auto uiRoot = _ui->getUI("PlayerName")->root.GetPtr();
		auto textBox = (Noesis::TextBox*)((Noesis::UserControl*)uiRoot)->FindName("inputText");

		std::string name = textBox->GetText();

		if(name=="")
		name = "???";

		ScoreSave_t score;
		score.name = name;
		//score.points = _gameLogic->getScore();

		_writeXml(score);

		// Reseteamos el juego para nueva partida una vez tenemos guardado el record actual
		_gameManager->getGameLogic()->resetGame();

		// Pasamos a la vista de scores
		_ui->unloadUI("PlayerName");
		_type = ScoreType::READ;
		loadScore();

	}

}

void ScoreState::_writeXml(ScoreState::ScoreSave_t scoreSave) {
	auto xmlEditor = std::make_shared<XmlEditor>();
	xmlEditor->loadDocument("score.xml","Score","score");

	assert(xmlEditor->isLoad());

	auto scoreXML = xmlEditor->getXmlDoc();
	xmlEditor->writeDocument(scoreXML,"score/","score.xml",true);

	TiXmlElement * user = new TiXmlElement( "user" );

	user->SetAttribute("key",1);
	user->SetAttribute("name",scoreSave.name.c_str());
	user->SetAttribute("points",scoreSave.points);

	scoreXML->RootElement()->LinkEndChild(user);


	xmlEditor->writeDocument(scoreXML,"score/","score.xml",false);
}

void ScoreState::_proccessScore() {
	auto xmlEditor = std::make_shared<XmlEditor>();
	xmlEditor->loadDocument("score.xml","Score","score");

	assert(xmlEditor->isLoad());

	// Obtenemos la listBox
	auto uiRoot = (Noesis::UserControl*)_ui->getUI("Score")->root.GetPtr();
	auto list_box = (Noesis::ListBox*)uiRoot->FindName("listBox");

	// Creamos una nueva colección
	Noesis::Gui::Collection* collection = new Noesis::Collection();

	// Ordenamos por puntos TODO: Los datos están guardados integros, se puede ordenar de más formas
	auto root = xmlEditor->getXmlDoc()->RootElement();
	std::map<int,std::tuple<std::string,std::string>> listScores;
	for(TiXmlElement* e = root->FirstChildElement("user"); e != NULL; e = e->NextSiblingElement("user"))
		listScores[atoi(e->Attribute("points"))] = std::make_tuple(e->Attribute("name"),e->Attribute("points"));


	std::map<int,std::tuple<std::string,std::string>>::iterator itEnd = listScores.end();
	std::map<int,std::tuple<std::string,std::string>>::iterator itBegin = listScores.begin();
	itEnd--;
	// Rellenamos el listBox
	for(;itEnd != itBegin ; ) {
		std::ostringstream oss;
		oss << "Name: " << std::get<0>(itEnd->second) << " Points: " <<  std::get<1>(itEnd->second);
		std::string data = oss.str();
		collection->Add(data.c_str());
		//collection->Add("");
		itEnd--;
	}

	list_box->SetItemsSource(collection);
}


