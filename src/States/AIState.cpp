#include "AIState.h"
#include "MainMenuState.h"
void AIState::setLose(bool lose) {
	_lose = lose;
}

void AIState::enter () {

	_nodes = _gameManager->getNodeManager();
	_actions = _gameManager->getActionsManager();
	_move = _gameManager->getMovementSystem();
	_cmSystem = _gameManager->getCameraSystem();
	_phSystem = _gameManager->getPhysicsSystem();
	_aSystem = _gameManager->getAnimationSystem();
	_grSystem = _gameManager->getGraphicsSystem();
	_ui = _gameManager->getUserInterfaceSystem();
	_ai = _gameManager->getAISystem();

	_player = _gameManager->getConfig()->getAiConfig()->player;

	auto sM = _gameManager->getSceneManager();

	// Cargamos la escena
	auto dotSceneLoader = std::make_shared<Ogre::DotSceneLoader>(_gameManager,_gameManager->getConfig());
	dotSceneLoader->parseDotScene("innsmouth.scene","ASOI",sM);



	// Shadow, Background, Ambientlight
	sM->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	_cmSystem->setActiveCamera(_gameManager->getMapper()->nameToId("tunnel"));

	// Noche
	//sM->getLight("Sun")->setDiffuseColour(Ogre::ColourValue(0, 0, 0.9));
	//sM->getLight("Sun")->setSpecularColour(Ogre::ColourValue(0, 0, 0.9));
	//sM->setAmbientLight(Ogre::ColourValue(0.4, 0.4,0.4));


	// Tarde
	//sM->getLight("Sun")->setDiffuseColour(Ogre::ColourValue(0.4, 0, 0));
	//sM->getLight("Sun")->setSpecularColour(Ogre::ColourValue(0.4, 0, 0));
	//sM->setAmbientLight(Ogre::ColourValue(0.6, 0.6,0.6));


	// Dia
	sM->getLight("Sun")->setDiffuseColour(Ogre::ColourValue(0.4, 0.4, 0.2));
	sM->getLight("Sun")->setSpecularColour(Ogre::ColourValue(0.4, 0.4, 0.2));
	sM->setAmbientLight(Ogre::ColourValue(0.75, 0.75, 0.75));
	sM->getLight("Sun")->setDirection(Ogre::Vector3(-1, -1, 0));


	// NavMesh and Obstacles
	_ai->createOgreCrowd(_gameManager->getNavEntities(GameManager::NAV_MESH),_gameManager->getNavEntities(GameManager::OBSTACLES));
	_ai->createAgents();

	// Main Agent
	auto agentMain = _gameManager->getMapper()->nameToId(_player);
	_ai->setIdleAgent(agentMain);

	// Agent
	auto agent1 = _gameManager->getMapper()->nameToId("Cylinder");
	_ai->setRandomAgent(agent1);
	//_ai->setIdleAgent(agent1);

	// Agent Points
	/*auto agent3 = _gameManager->getMapper()->nameToId("agent3");

	Ogre::Vector3 point1 = Ogre::Vector3(-6.12375,0.00036,-1.64085);
	Ogre::Vector3 point2 = Ogre::Vector3(-5.31593,0.00021,-12.39056);
	Ogre::Vector3 point3 = Ogre::Vector3(-19.89943,0.00013,-25.97841);
	_ai->getComponent(agent3)->setPatrolPositions(point1);
	_ai->getComponent(agent3)->setPatrolPositions(point2);
	_ai->getComponent(agent3)->setPatrolPositions(point3);

	//_ai->setPatrolAgent(agent3);
	_ai->setIdleAgent(agent3);
	 */


	//_navMeshUpdater("Suzanne1");
	//_navMeshUpdater("Suzanne2");
	//_navMeshUpdater("Suzanne3");
	//_navMeshUpdater("Suzanne4");

}

void AIState::exit () {
	_ui->unloadUI("GameUI");
}

void AIState::pause() {

}

void AIState::resume() {

}

bool AIState::frameStarted (const Ogre::FrameEvent& evt) {

	auto deltaTime = evt.timeSinceLastFrame;
	_move->update(deltaTime);
	_moveMainCharacter();
	_actions->update(deltaTime);
	_ai->update(deltaTime);

	return true;
}

bool AIState::frameEnded (const Ogre::FrameEvent& evt) {

	return true;
}

void AIState::movePoV(const OIS::MouseEvent & e) {
	float rotx = e.state.X.rel * -1;
	float roty = e.state.Y.rel * -1;

	shared_ptr<Action> action = std::make_shared<Action>();
	shared_ptr<MovePoV> pov = std::make_shared<MovePoV>( _fov,_nodes, rotx, roty, 0);
	action->addSubaction(pov);

	_actions->addAction(action);
}

void AIState::obstacle1(InputBindings::PushState pushState) {
	if(pushState == InputBindings::PushState::RELEASE){
		_navMeshUpdater("Suzanne1");
	}
}

void AIState::obstacle2(InputBindings::PushState pushState) {
	if(pushState == InputBindings::PushState::RELEASE){
		_navMeshUpdater("Suzanne2");
	}
}

void AIState::obstacle3(InputBindings::PushState pushState) {
	if(pushState == InputBindings::PushState::RELEASE){
		_navMeshUpdater("Suzanne3");
	}
}

void AIState::obstacle4(InputBindings::PushState pushState) {
	if(pushState == InputBindings::PushState::RELEASE){
		_navMeshUpdater("Suzanne4");
	}
}

void AIState::_moveMainCharacter() {
	if(_movingX || _movingY) {


		// Transform from quaternion to euelr angles as defined in http://www.ogre3d.org/tikiwiki/Euler+Angle+Class
		Ogre::Matrix3 rotmat;
		Ogre::Radian x, y, z;

		_cmSystem->getActiveCamera()->getParentNode()->getOrientation().ToRotationMatrix(rotmat);

		rotmat.ToEulerAnglesXYZ(x, y, z);

		const Ogre::Vector3 & d = Ogre::Vector3(-_vX, 0, _vY);
		const float vFactor = 10;
		Ogre::Quaternion q(-z, Ogre::Vector3::UNIT_Y);

		//_move->move(_character->getId(), _player, q * d * vFactor);

		Ogre::Vector3 newPosition = Ogre::Vector3(q * d ); // * vFactor
		auto agentMain = _gameManager->getMapper()->nameToId(_player);
		auto actualPosition = _gameManager->getSceneManager()->getSceneNode(_player)->getPosition();
		_ai->setGotoAgent(agentMain,newPosition + actualPosition);
	}

}

void AIState::moveMainCharacterKeyboard(InputBindings::PushState state, int x, int y) {
	if(state == InputBindings::PushState::PUSH) {
		if(x != 0) {
			_movingX = true;
			_vX = x;
		}
		if(y != 0 ) {
			_movingY = true;
			_vY = y;
		}
	}
	else {
		if(x != 0) {
			_movingX = false;
			_vX = 0;
		}
		if(y != 0) {
			_movingY = false;
			_vY = 0;
		}

	}
}

void AIState::moveMainCharacterGamepad(int id, const OIS::JoyStickEvent & e, InputBindings::PushState state) {
	/* Range goes from -32768 to 32767. 128 is equals to no movement
	 * So it's mapped to [-32896 - 32639] by substracting 128, then this value got normalized to -1.007 - 1,
	 * because it's getting divided by 32639. Every value under -1 is rounded up to -1.
	 */

	cout << e.state.mAxes[id].abs << endl;

	if(id == 0 || id == 1) {
		float vGamePad = e.state.mAxes[id].abs - 128;
		vGamePad = vGamePad / 32639.0;
		if (vGamePad < -1) vGamePad = -1;

		if(id == 0) {
			if(vGamePad == 0)
				_movingX = false;
			else
				_movingX = true;

			_vX = vGamePad;
		}
		else {
			if(vGamePad == 0)
				_movingY = false;
			else
				_movingY = true;

			_vY = vGamePad;
		}

	}


}

void AIState::_navMeshUpdater(string name) {
	_ai->updateNavMesh(name);
}

void AIState::exitGame(InputBindings::PushState pushState) {

	if(pushState == InputBindings::PushState::RELEASE){
		_exitGame = true;
		_ui->terminateUI();
	}

}

