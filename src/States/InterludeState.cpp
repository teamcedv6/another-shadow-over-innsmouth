#include "InterludeState.h"

void InterludeState::enter () {
	_conversationSystem = _gameManager->getConversationSystem();
	_ui = _gameManager->getUserInterfaceSystem();

	_internalState = 0;
	_ui->loadUI("Interlude", "Interlude.xaml");

	_root = (Noesis::UserControl*) _ui->getUI("Interlude")->root.GetPtr();
	Noesis::Storyboard * anim = (Noesis::Storyboard*)_root->FindName("enter");
	anim->Begin();

	Noesis::TextBlock * textBlock = (Noesis::TextBlock *)_root->FindName("text");

	if(_daytime == 0) {
			_conversationSystem->loadConversation("intro.xml");
			_conversationSystem->startConversation();
			_timer = 3;
	}
	else if(_daytime == 1) {
		_timer = 0.3;
		textBlock->SetText("Mas tarde, ese dia...");
	}
	else if(_daytime == 2) {
		_timer = 0.3;
		textBlock->SetText("Esa misma noche...");
	}
	else if(_daytime == 3) {
		_timer = 0.3;
		textBlock->SetText("La aventura llega a su fin");
	}
}

void InterludeState::exit () {

}

void InterludeState::pause() {

}

void InterludeState::resume() {

}

bool InterludeState::frameStarted (const Ogre::FrameEvent& evt) {
	auto deltaTime = evt.timeSinceLastFrame;

	if(_timer > 0) {
		_timer -= deltaTime;
	}

	_ui->update(deltaTime);

	if(_daytime == 0) {
			if(_timer <= 0) {
				if(_internalState < 4) {
					_conversationSystem->push();

					if(_internalState == 0) {
						_timer = 15;
					}
					if(_internalState == 1) {
						_timer = 7;
					}
					if(_internalState == 2) {
						_timer = 7;
					}
					if(_internalState == 3) {
					}
				}
				else if(_internalState == 4){
					Noesis::Storyboard * anim = (Noesis::Storyboard*)_root->FindName("exit");
					anim->Begin();
					_timer = 0.3;
				}
				else {
					_gameManager->popState();
				}
				_internalState++;
			}
	}
	else if(_daytime == 1 || _daytime == 2 || _daytime == 3) {
		if(_timer < 0) {
			if(_internalState == 0) {
				Noesis::Storyboard * anim = (Noesis::Storyboard*)_root->FindName("enterText");
				anim->Begin();
				_timer = 1.5;
			}
			else if(_internalState == 1) {
				_timer = 3.0;
			}
			else if(_internalState == 2) {
				Noesis::Storyboard * anim = (Noesis::Storyboard*)_root->FindName("exit");
				anim->Begin();
				_timer = 0.3;
			}
			else if(_internalState == 3) {
				_gameManager->popState();
			}
			_internalState++;
		}
	}

	return true;
}

bool InterludeState::frameEnded (const Ogre::FrameEvent& evt) {
	return true;
}

void InterludeState::up(InputBindings::PushState pushState) { if(pushState == InputBindings::PushState::RELEASE) _conversationSystem->up(); }

void InterludeState::down(InputBindings::PushState pushState) { if(pushState == InputBindings::PushState::RELEASE) _conversationSystem->down(); }

void InterludeState::push(InputBindings::PushState pushState) { if(pushState == InputBindings::PushState::RELEASE) _conversationSystem->push(); }

void InterludeState::back(InputBindings::PushState pushState) { if(pushState == InputBindings::PushState::RELEASE) _conversationSystem->back(); }

void InterludeState::gamePadUpDown(int id, const OIS::JoyStickEvent & e, InputBindings::PushState pushState) {
	int value = e.state.mAxes[id].abs;

	if(value >= 17000) {
		down(InputBindings::PushState::RELEASE);
	}
	else if(value <= -17000) {
		up(InputBindings::PushState::RELEASE);
	}
}

void InterludeState::setDayTime(int n) { _daytime = n; }
