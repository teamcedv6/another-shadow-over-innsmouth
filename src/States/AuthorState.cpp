#include "AuthorState.h"

void AuthorState::enter () {

	_ui = _gameManager->getUserInterfaceSystem();
	// Cargamos la interfaz
	_ui->loadUI("Author", "Author.xaml");

	// Load  menu events handlers for each button (actions focus, unfocus and click
	auto uiRoot = _ui->getUI("Author")->root.GetPtr();
	auto button = ((Noesis::UserControl*)uiRoot)->FindName("backButton");
	((Noesis::Button*)button)->MouseDown() += Noesis::MakeDelegate(this, &AuthorState::menuClick);
}

void AuthorState::exit () {
	_ui->unloadUI("Author");
}

void AuthorState::pause() {

}

void AuthorState::resume() {

}

bool AuthorState::frameStarted (const Ogre::FrameEvent& evt) {
	auto deltaTime = evt.timeSinceLastFrame;
	_ui->update(deltaTime);

	return true;

}

bool AuthorState::frameEnded (const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}

void AuthorState::menuClick(Noesis::BaseComponent * sender, const Noesis::MouseButtonEventArgs & args) {

	// Click sound
	auto soundManager = _gameManager->getSoundManager();
	//soundManager->selectResource(SoundManager::FX,"click",SoundManager::PLAY,50);

	popState(_gameManager);
}
