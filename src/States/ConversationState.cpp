#include "ConversationState.h"

void ConversationState::enter () {
	_conversationSystem = _gameManager->getConversationSystem();

	_move = _gameManager->getMovementSystem();
	_aSystem = _gameManager->getAnimationSystem();
	_ui = _gameManager->getUserInterfaceSystem();
	_actions = _gameManager->getActionsManager();

	std::cout << "Conv state started\n";
}

void ConversationState::exit () {

}

void ConversationState::pause() {

}

void ConversationState::resume() {

}

bool ConversationState::frameStarted (const Ogre::FrameEvent& evt) {
	auto deltaTime = evt.timeSinceLastFrame;

	//_move->update(deltaTime);
	//_aSystem->update(deltaTime);
	_ui->update(deltaTime);

	//_actions->update(deltaTime);

	return true;
}

bool ConversationState::frameEnded (const Ogre::FrameEvent& evt) {
	return true;
}

void ConversationState::up(InputBindings::PushState pushState) { if(pushState == InputBindings::PushState::RELEASE) _conversationSystem->up(); }

void ConversationState::down(InputBindings::PushState pushState) { if(pushState == InputBindings::PushState::RELEASE) _conversationSystem->down(); }

void ConversationState::push(InputBindings::PushState pushState) { if(pushState == InputBindings::PushState::RELEASE) _conversationSystem->push(); }

void ConversationState::back(InputBindings::PushState pushState) { if(pushState == InputBindings::PushState::RELEASE) _conversationSystem->back(); }

void ConversationState::gamePadUpDown(int id, const OIS::JoyStickEvent & e, InputBindings::PushState pushState) {
	int value = e.state.mAxes[id].abs;

	if(value >= 17000) {
		down(InputBindings::PushState::RELEASE);
	}
	else if(value <= -17000) {
		up(InputBindings::PushState::RELEASE);
	}
}
