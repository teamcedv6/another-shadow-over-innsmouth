#include "GameManager.h"
#include "EventFactory.h"
#include "EntityExtraLoader.h"

EntityExtraLoader::EntityExtraLoader() {

}

EntityExtraLoader::EntityExtraLoader(std::shared_ptr<GameManager> gameManager) {
	_conversationLoader = gameManager->getConversationLoader();
	_conversationSystem = gameManager->getConversationSystem();
	_interactionSystem = gameManager->getInteractionSystem();
	_actionsManager = gameManager->getActionsManager();
	_eventFactory = gameManager->getEventFactory();
	_mapper = gameManager->getMapper();
	_inventory = gameManager->getInventory();
	_aiSystem = gameManager->getAISystem();
}

EntityExtraLoader::EntityExtraLoader(const EntityExtraLoader & entityExtraLoader) {

}

EntityExtraLoader::EntityExtraLoader(const EntityExtraLoader && entityExtraLoader) {

}

EntityExtraLoader::~EntityExtraLoader() {

}

EntityExtraLoader& EntityExtraLoader::operator = (const EntityExtraLoader &entityExtraLoader) {
	return *this;
}

void EntityExtraLoader::preloadConfiguration() {
	_preloadExtraConfig();
	_loadInventory();

}

void EntityExtraLoader::_preloadExtraConfig() {
	std::cout << "INIT EXTRACONFIG PRELOAD\n";

	auto xmlEditor = std::make_shared<XmlEditor>();
	xmlEditor->loadDocument("extraConfig.xml","Config","ExtraConfig");

	assert(xmlEditor->isLoad());

	const TiXmlElement * root = xmlEditor->getXmlDoc()->RootElement();
	const TiXmlElement * pElement = root->FirstChildElement("ConfigEntity");


	while(pElement) {
		std::cout << pElement->Value();
		mEntConf = std::make_shared<EntityConfigurations>();
		std::string name = pElement->Attribute("name");

		std::cout << " name: " << name << "\n";

		const TiXmlElement * childElement = pElement->FirstChildElement();

		while(childElement) {
			std::cout << "\t " << childElement->Value() << "\n";
			std::string nodeValue = (std::string) childElement->Value();
			if(nodeValue == "Event") {
				_processEvent(childElement);
			} else if(nodeValue == "Conversation") {
				_processConversation(childElement);
			} else if(nodeValue == "Interaction") {
				_processInteraction(childElement);
			}else if(nodeValue == "Patrol") {
				_processPatrol(childElement);
			}

			childElement = childElement->NextSiblingElement();
		}

		pElement = pElement->NextSiblingElement("ConfigEntity");

		_entitiesConfMap[name] = mEntConf;
	}

	std::cout << "FINISH EXTRACONFIG PRELOAD\n";
}

void EntityExtraLoader::_loadInventory() {
	std::cout << "INIT INVENTORY LOAD\n";
	auto xmlEditor = std::make_shared<XmlEditor>();
	xmlEditor->loadDocument("inventory.xml","Config","InventoryObjectsList");

	assert(xmlEditor->isLoad());

	const TiXmlElement * root = xmlEditor->getXmlDoc()->RootElement();
	const TiXmlElement * pElement = root->FirstChildElement("InventoryObject");

	while(pElement) {
		std::cout << pElement->Value();

		std::string name = (std::string)pElement->Attribute("name");
		std::string picture = (std::string)pElement->Attribute("picture");

		_inventory->createObject(name, picture);
		pElement = pElement->NextSiblingElement("InventoryObject");
	}

	std::cout << "FINISH INVENTORY LOAD\n";
}

void EntityExtraLoader::loadConversationComponent(const std::string & name) {
	auto entityConf = _entitiesConfMap[name];
	auto conversation = _conversationLoader->loadConversation(entityConf->converConf.name);
	std::shared_ptr<ConversationComponent> convComp = std::make_shared<ConversationComponent>(conversation);

	_conversationSystem->addComponent(_mapper->nameToId(name), convComp);
}

void EntityExtraLoader::loadPatrolPoints(const std::string & name) {

	auto entityConf = _entitiesConfMap[name];

	for(auto& patrol : entityConf->aiConf->agentPatrol) {
		_aiSystem->getComponent(_mapper->nameToId(name))->setPatrolPositions(patrol);
	}

}

void EntityExtraLoader::loadInteractionComponent(std::shared_ptr<Ogre::DotSceneLoader::Extra> extraConf) {
	std::cout << "LOAD INTERACTION map size: "<<  _entitiesConfMap.size()<< " name: " << extraConf->name <<"\n";
	auto entityConf = _entitiesConfMap[extraConf->name];
	auto interComp = std::make_shared<InteractionComponent>(entityConf->interactionConf->type, extraConf->node,
			entityConf->interactionConf->activeDistance, entityConf->interactionConf->interactionDistance);

	_interactionSystem->addComponent(_mapper->nameToId(extraConf->name), interComp);
}

void EntityExtraLoader::loadEvents(std::shared_ptr<Ogre::DotSceneLoader::Extra> extraConf) {
	std::cout << "SIZE: "<< _entitiesConfMap.size() << "\n";
	auto a = _entitiesConfMap[extraConf->name];
	std::cout << "SIZE: "<< _entitiesConfMap.size() << " " << a->eventConfs.size() << " " << extraConf->name << "\n";
	auto entityConf = _entitiesConfMap[extraConf->name];
	std::cout << "SIZE: "<< _entitiesConfMap.size() << " " << a->eventConfs.size() << " " << extraConf->name << "\n";

	for(auto it = entityConf->eventConfs.begin(); it != entityConf->eventConfs.end(); it++) {
		shared_ptr<Trigger> trigger;
		if(_eventFactory != nullptr)
		 trigger = _eventFactory->createTrigger(*(*it)->trigger);
		std::cout<< "in loop\n";
		shared_ptr<Action> action = make_shared<Action>();

		for (auto it2 = (*it)->subactions.begin(); it2 != (*it)->subactions.end(); it2++) {
			shared_ptr<Subaction> subaction = _eventFactory->createSubaction(**it2);
			action->addSubaction(subaction);
		}

		if((*it)->type == "give") {
			_actionsManager->addGiveEvent((*it)->source, (*it)->target, trigger, action);
		} else if((*it)->type == "use") {
			_actionsManager->addUseEvent((*it)->source, (*it)->target, trigger, action);

		} else if((*it)->type == "normal") {
			_actionsManager->addEvent(trigger, action);
		}
	}

}

void EntityExtraLoader::_processConversation(const TiXmlElement * pElement) {
	mEntConf->converConf.name =	pElement->GetText();
}

void EntityExtraLoader::_processEvent(const TiXmlElement * pElement) {
	std::string eventType = (std::string) pElement->Attribute("type");

	std::shared_ptr<EventConf> eventConf = make_shared<EventConf>();
	eventConf->type = eventType;

	if(eventType == "give" || eventType == "use") {
		eventConf->source = (std::string) pElement->FirstChildElement("Source")->GetText();
		eventConf->target = (std::string) pElement->FirstChildElement("Target")->GetText();
	}

	const TiXmlElement * triggerElement = pElement->FirstChildElement("Trigger");

	eventConf->trigger = _processTrigger(triggerElement);

	const TiXmlElement * actionElement = pElement->FirstChildElement("Action");
	const TiXmlElement * subactionElement = actionElement->FirstChildElement("Subaction");

	while(subactionElement) {
		eventConf->subactions.push_back(_processSubaction(subactionElement));
		subactionElement = subactionElement->NextSiblingElement("Subaction");
	}

	mEntConf->eventConfs.push_back(eventConf);
}

std::shared_ptr<EntityExtraLoader::EventPart> EntityExtraLoader::_processTrigger(const TiXmlElement * triggerElement) {
	std::shared_ptr<EventPart> trigger = std::make_shared<EventPart>();
	trigger->name = (std::string) triggerElement->Attribute("name");

	const TiXmlElement * paramElement = triggerElement->FirstChildElement("Param");

	Param param;

	while(paramElement) {
			_processParam(paramElement, param);
			trigger->params.push_back(param);
			paramElement = paramElement->NextSiblingElement("Param");
	}

	return trigger;
}

std::shared_ptr<EntityExtraLoader::EventPart> EntityExtraLoader::_processSubaction(const TiXmlElement * subactionElement) {
	const TiXmlElement * paramElement = subactionElement->FirstChildElement("Param");
	Param param;
	paramElement = subactionElement->FirstChildElement("Param");

	std::shared_ptr<EventPart> subaction = make_shared<EventPart>();
	subaction->name = (std::string) subactionElement->Attribute("name");

	while(paramElement) {
		Param param;
		_processParam(paramElement, param);
		subaction->params.push_back(param);
		paramElement = paramElement->NextSiblingElement("Param");
	}

	return subaction;

}

void EntityExtraLoader::_processInteraction(const TiXmlElement * pElement) {
	std::istringstream cc1((std::string)pElement->Attribute("activeDistance"));
	std::istringstream cc2((std::string)pElement->Attribute("interactionDistance"));

	float activeDistance;
	float interactionDistance;

	assert(cc1 >> activeDistance);
	assert(cc2 >> interactionDistance);

	std::string interactionTypeStr = (std::string) pElement->Attribute("type");
	InteractionComponent::InteractionType type;

	if(interactionTypeStr == "object_pickable") {
		type = InteractionComponent::InteractionType::OBJECT_PICKABLE;
	} else if(interactionTypeStr == "object_non_pickable") {
		type = InteractionComponent::InteractionType::OBJECT_NON_PICKABLE;
	} else if(interactionTypeStr == "person_talkable") {
		type = InteractionComponent::InteractionType::PERSON_TALKABLE;
	} else if(interactionTypeStr == "person_non_talkable") {
		type = InteractionComponent::InteractionType::PERSON_NON_TALKABLE;
	}

	mEntConf->interactionConf = std::make_shared<InteractionConf>();
	mEntConf->interactionConf->activeDistance = activeDistance;
	mEntConf->interactionConf->interactionDistance = interactionDistance;
	mEntConf->interactionConf->type = type;
}

void EntityExtraLoader::_processPatrol(const TiXmlElement * pointElement) {

	const TiXmlElement * childElement = pointElement->FirstChildElement("Point");
	mEntConf->aiConf = make_shared<AIConf>();
	while(childElement) {
			Param param;
			const TiXmlElement *paramElement = childElement->FirstChildElement("Param");

			std::vector<float> points;

			while(paramElement) {
				Param param;
				_processParam(paramElement, param);
				points.push_back(std::atof(paramElement->GetText()));
				paramElement = paramElement->NextSiblingElement("Param");
			}

			Ogre::Vector3 patrol = Ogre::Vector3(points.at(0),points.at(1),points.at(2));
			mEntConf->aiConf->agentPatrol.push_back(patrol);

		childElement = childElement->NextSiblingElement();
	}
}

void EntityExtraLoader::_processParam(const TiXmlElement * paramElement, Param & param) {
	std::string type = (std::string)paramElement->Attribute("type");
	param.type = type;

	std::istringstream iss((std::string)paramElement->GetText());

	if(type == "int") {
		assert (iss >> param.paramValue.intValue);
	} else if(type == "float") {
		assert (iss >> param.paramValue.floatValue);
	} else if(type == "str") {
		param.paramValue.strValue = iss.str();
	}

}
