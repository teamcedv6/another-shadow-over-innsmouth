#include "ConversationLoader.h"
#include "EntityExtraLoader.h"
#include "EventFactory.h"

ConversationLoader::ConversationLoader() : _fullLoad(false), _conversation(nullptr) {}

ConversationLoader::ConversationLoader(bool fullLoad, std::shared_ptr<EventFactory> factory) : _fullLoad(fullLoad), _loader(nullptr), _factory(factory), _conversation(nullptr) {}

ConversationLoader::ConversationLoader(const ConversationLoader & conversationLoader) : _fullLoad(conversationLoader._fullLoad), _loader(conversationLoader._loader), _factory(conversationLoader._factory), _conversation(_conversation) {
	_conversation = std::make_shared<Conversation>(true);
}

ConversationLoader::ConversationLoader(const ConversationLoader && conversationLoader) : _fullLoad(conversationLoader._fullLoad), _loader(conversationLoader._loader),  _factory(conversationLoader._factory), _conversation(_conversation) {}

ConversationLoader::~ConversationLoader() {}

ConversationLoader& ConversationLoader::operator = (const ConversationLoader &conversationLoader) {
	return *this;
}

void ConversationLoader::setExtraLoader(std::shared_ptr<EntityExtraLoader> loader) {_loader = loader;}

std::shared_ptr<Conversation> ConversationLoader::loadConversation(const std::string & name) {
	std:cout << "Loading " << name << "\n";
	auto xmlEditor = std::make_shared<XmlEditor>();
	xmlEditor->loadDocument(name,"Config","Conversation");

	assert(xmlEditor->isLoad());

	_proccessConversation(xmlEditor->getXmlDoc()->RootElement());
	return _conversation;
}

std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> ConversationLoader::loadConversationNodes(const std::string & name, std::shared_ptr<Conversation> conversation) {
	std:cout << "Loading " << name << "\n";
	auto xmlEditor = std::make_shared<XmlEditor>();
	xmlEditor->loadDocument(name,"Config","ConversationNodes");

	assert(xmlEditor->isLoad());

	_conversation = conversation;
	_proccessNodesVector(xmlEditor->getXmlDoc()->RootElement());
	return _nodesVector;
}

void ConversationLoader::_proccessConversation(TiXmlElement* conversation) {

	bool hijacking;
	std::string hijackingStr = (std::string)conversation->Attribute("hijacking");
	hijacking = (hijackingStr == "true");

	_conversation = std::make_shared<Conversation>(hijacking);
	auto pElement = conversation->FirstChildElement();

	while(pElement)
	{
		_conversation->addNode(_processNode(pElement));
		pElement = pElement->NextSiblingElement();
	}
}

void ConversationLoader::_proccessNodesVector(TiXmlElement* firstNode) {

	_nodesVector = std::make_shared<std::vector<std::shared_ptr<ConversationNode>>>();
	auto pElement = firstNode->FirstChildElement();

	while(pElement)
	{
		_nodesVector->push_back(_processNode(pElement));
		pElement = pElement->NextSiblingElement();
	}
}

shared_ptr<ConversationNode> ConversationLoader::_processNode(TiXmlElement * node) {
	shared_ptr<ConversationNode> convNode;

	std::cout << "CONV NODE VALUE " << node->Value() << "\n";
	std::string nodeValue = node->Value();

	if(nodeValue == "NormalText") {
		convNode = _processNormalText(node);
	} else if(nodeValue == "ThoughtText") {
		convNode = _processThoughtText(node);
	} else if(nodeValue == "QuestionText") {
		convNode = _processQuestionNode(node);
	} else if(nodeValue == "LinkNode") {
		convNode = _processLinkNode(node);
	} else if(nodeValue == "ActionNode") {
		convNode = _processActionNode(node);
	} else if(nodeValue == "TriggerNode") {
		convNode = _processTriggerNode(node);
	} else if(nodeValue == "Include") {
		convNode = _processTriggerNode(node);
	}


	return convNode;
}

shared_ptr<NormalText> ConversationLoader::_processNormalText(TiXmlElement * node) {
	std::string name = (std::string)node->Attribute("name");
	std::string picture = (std::string)node->Attribute("picture");
	std::string text = (std::string)node->GetText();

	std::cout << "LOADER NormalText " << name << " " << picture << " " << text << " \n";
	return std::make_shared<NormalText>(name, text, picture);
}

shared_ptr<ThoughtNode> ConversationLoader::_processThoughtText(TiXmlElement * node) {
	std::string text = (std::string)node->GetText();
	std::cout << "LOADER ThoughtNode " << text << " \n";
	return std::make_shared<ThoughtNode>(text);
}

shared_ptr<TriggerNode> ConversationLoader::_processTriggerNode(TiXmlElement * node) {
	std::shared_ptr<Trigger> trigger;

	const TiXmlElement * triggerElement = node->FirstChildElement("Trigger");
	std::shared_ptr<EntityExtraLoader::EventPart> triggerConf = _loader->_processTrigger(triggerElement);
	trigger = _factory->createTrigger(*triggerConf);

	std::shared_ptr<TriggerNode> triggerNode = std::make_shared<TriggerNode>(trigger, _conversation);

	std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> trueVector = nullptr;
	std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> falseVector = nullptr;

	const TiXmlElement * trueElement = node->FirstChildElement("true");
	const TiXmlElement * falseElement = node->FirstChildElement("false");

	std::cout << "LOADER TriggerNode \n";
	std::string truePath = (std::string)trueElement->Attribute("path");
	std::string falsePath = (std::string)falseElement->Attribute("path");

	std::cout << "\t LOADER true " << truePath << " "  << falsePath << " \n";

	if(_fullLoad) {
		auto currentNodesVector = _nodesVector;
		trueVector = loadConversationNodes(truePath , _conversation);
		falseVector = loadConversationNodes(falsePath , _conversation);
		_nodesVector = currentNodesVector;
	}

	triggerNode->addTruPath(trueVector, truePath);
	triggerNode->addFalsePath(falseVector, falsePath);

	return triggerNode;
}

shared_ptr<ActionNode> ConversationLoader::_processActionNode(TiXmlElement * node) {
	std::shared_ptr<Action> action = std::make_shared<Action>();
	const TiXmlElement * subactionElement = node->FirstChildElement("Subaction");
	std::shared_ptr<EntityExtraLoader::EventPart> subactionConf;

	while(subactionElement) {
		subactionConf = _loader->_processSubaction(subactionElement);
		action->addSubaction(_factory->createSubaction(*subactionConf));

		subactionElement = subactionElement->NextSiblingElement("Subaction");
	}

	return std::make_shared<ActionNode>(action);
}

shared_ptr<QuestionText> ConversationLoader::_processQuestionNode(TiXmlElement * node) {

	std::shared_ptr<QuestionText> questionText = std::make_shared<QuestionText>(_conversation);
	std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> answerVector = nullptr;

	const TiXmlElement * optionElement = node->FirstChildElement("option");

	std::cout << "LOADER QuestionNode \n";
	while(optionElement) {
		std::string path = (std::string)optionElement->Attribute("path");
		std::string text = (std::string)optionElement->GetText();

		std::cout << "\t LOADER option " << path << " " << text << " \n";

		if(_fullLoad) {
			auto currentNodesVector = _nodesVector;
			answerVector = loadConversationNodes(path , _conversation);
			_nodesVector = currentNodesVector;
		}
		questionText->addAnswer(text,path,answerVector);

		optionElement = optionElement->NextSiblingElement("option");
	}


	return questionText;
}

shared_ptr<LinkNode> ConversationLoader::_processLinkNode(TiXmlElement * node) {
	std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> linkNodes = nullptr;
	std::string path = (std::string)node->Attribute("path");

	if(_fullLoad) {
		auto currentNodesVector = _nodesVector;
		linkNodes = loadConversationNodes(path , _conversation);
		_nodesVector = currentNodesVector;
	}

	return std::make_shared<LinkNode>(path, linkNodes, _conversation);
}
