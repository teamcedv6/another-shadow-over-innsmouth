#include "ActionsManager.h"

#include "PlayState.h"

ActionsManager::ActionsManager(const ActionsManager & actionsManager) {}

ActionsManager::ActionsManager(const ActionsManager && actionsManager) {}

ActionsManager::~ActionsManager () {}

ActionsManager & ActionsManager::operator= (const ActionsManager & actionsManager) { return *this; }

void ActionsManager::addEvent(shared_ptr<Trigger> trigger, shared_ptr<Action> action) {
	auto pair = std::make_pair(trigger, action);
	_events.push_back(pair);
}

void ActionsManager::addDistanceEvent(shared_ptr<DistanceTrigger> trigger, shared_ptr<Action> action) {

}

void ActionsManager::addGiveEvent(std::string object, std::string person, shared_ptr<Trigger> trigger, shared_ptr<Action> action) {
	std::pair<std::string, std::string> givenPair = std::make_pair(object,person);
	std::pair<shared_ptr<Trigger>, shared_ptr<Action>> event = std::make_pair(trigger,action);

	_giveInteractions[givenPair] = event;
}

void ActionsManager::addUseEvent(std::string object, std::string target, shared_ptr<Trigger> trigger, shared_ptr<Action> action){
	std::pair<std::string, std::string> usedPair = std::make_pair(object,target);
	std::pair<shared_ptr<Trigger>, shared_ptr<Action>> event = std::make_pair(trigger,action);

	_useInteractions[usedPair] = event;
}

void ActionsManager::addAction(shared_ptr<Action> action) { _actions.push_back(action);}

void ActionsManager::clearActions() {}
void ActionsManager::clearEvents() {}
void ActionsManager::clearGiveEvents() {}
void ActionsManager::clearUseEvents() {}

void ActionsManager::positionUpdate() {

}

void ActionsManager::giveObjectTo(std::string object, std::string person) {
	std::pair<std::string, std::string> givenPair = std::make_pair(object,person);
	std::pair<std::string, std::string> givenPairNoSource = std::make_pair("_any_",person);
	std::pair<std::string, std::string> givenPairNoTarget = std::make_pair(object,"_any_");
	std::pair<std::string, std::string> givenPairGeneric = std::make_pair("_any_","_any_");

	if(_giveInteractions.count(givenPair) > 0) {
		auto event = _giveInteractions[givenPair];

		if(event.first->check()) {
			_actions.push_back(std::make_shared<Action>(*event.second));
		}
	}
	else if (_giveInteractions.count(givenPairNoSource) > 0) {
		auto event = _giveInteractions[givenPairNoSource];

		if(event.first->check()) {
			_actions.push_back(std::make_shared<Action>(*event.second));
		}
	}
	else if (_giveInteractions.count(givenPairNoTarget) > 0) {
		auto event = _giveInteractions[givenPairNoTarget];

		if(event.first->check()) {
			_actions.push_back(std::make_shared<Action>(*event.second));
		}
	}
	else if(_giveInteractions.count(givenPairGeneric) > 0) {
		auto event = _giveInteractions[givenPairGeneric];

		if(event.first->check()) {
			_actions.push_back(std::make_shared<Action>(*event.second));
		}
	}
}

void ActionsManager::useObjectWith(std::string object, std::string target) {
	std::pair<std::string, std::string> usedPair = std::make_pair(object,target);
	std::pair<std::string, std::string> usedPairNoSource = std::make_pair("_any_",target);
	std::pair<std::string, std::string> usedPairNoTarget = std::make_pair(object,"_any_");
	std::pair<std::string, std::string> usedPairGeneric = std::make_pair("_any_","_any_");

	if(_giveInteractions.count(usedPair) > 0) {
			auto event = _giveInteractions[usedPair];

			if(event.first->check()) {
				_actions.push_back(std::make_shared<Action>(*event.second));
			}
		}
		else if (_giveInteractions.count(usedPairNoSource) > 0) {
			auto event = _giveInteractions[usedPairNoSource];

			if(event.first->check()) {
				_actions.push_back(std::make_shared<Action>(*event.second));
			}
		}
		else if (_giveInteractions.count(usedPairNoTarget) > 0) {
			auto event = _giveInteractions[usedPairNoTarget];

			if(event.first->check()) {
				_actions.push_back(std::make_shared<Action>(*event.second));
			}
		}
		else if(_giveInteractions.count(usedPairGeneric) > 0) {
			auto event = _giveInteractions[usedPairGeneric];

			if(event.first->check()) {
				_actions.push_back(std::make_shared<Action>(*event.second));
			}
		}

}

void ActionsManager::update(Ogre::Real deltaTime) {
	_updateEvents(deltaTime);
	_updateActions(deltaTime);
}

void ActionsManager::_updateActions(Ogre::Real deltaTime) {
	vector<int> finished;
		Action::ActionResult result;

		finished.reserve(_actions.size());
		int i = 0;

		for(auto it = _actions.begin(); it != _actions.end(); it++, i++) {
			result = (*it)->update(deltaTime);
			//result = Action::CONTINUE;

			switch(result){
				case Action::CONTINUE:
				break;
				case Action::PLAYER_WIN:
				break;
				case Action::PLAYER_LOST:
					finished.push_back(i);
					static_cast<PlayState*>(_playState.get())->setLose(true);
					std::cout << "*********************************** GAME OVER *********************************************\n";
				break;
				case Action::ACTION_FINISHED:
					finished.push_back(i);
				break;
			}

		}

		for(int i : finished) {
			_actions.erase(_actions.begin() + i);
		}
}

void ActionsManager::_updateEvents(Ogre::Real deltaTime) {
	auto it = _events.begin();

		while(it != _events.end()) {
			// If event meet condition, it's triggered
			if(it->first->check()) {
				// Add action to action queue
				_actions.push_back(it->second);
				it = _events.erase(it);
				std::cout << "EVNTS size: " << _events.size() << "\n";
			}
			else {
				it++;
			}
		}
}

void ActionsManager::_updateDistanceEvents() {

}

