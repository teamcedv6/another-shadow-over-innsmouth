#include "ConversationSystem.h"
#include "InteractionSystem.h"
#include "ConversationLoader.h"
#include "EntityExtraLoader.h"
#include "EventFactory.h"
#include "GameManager.h"
#include "GameState.h"


GameManager::~GameManager () {
	while (!_states.empty()) {
		_states.top()->exit();
		_states.pop();
	}
	_existingStates.clear();
}

void GameManager::start (shared_ptr<GameState> state) {

	// Puntero inteligente a si mismo
	shared_ptr<GameManager> smartThis(this);

	// ROOT
	_root = make_shared<Ogre::Root>();

	// SCENE MANAGER
	shared_ptr<Ogre::SceneManager> sM(_root->createSceneManager(Ogre::ST_GENERIC, "SceneManager"));
	_sceneManager = sM;

	// Config
	loadResources();
	if (!configure())
		return;

	// CONFIG XML
	_config = std::make_shared<Configuration>();
	_config->loadConfig();
	//_config->writeConfig();

	// Initialize entities id mapper
	_idMapper = std::make_shared<IdNameMapper>();

	Entity::setMapper(_idMapper);

	_entities = std::make_shared<std::map<const std::string, std::shared_ptr<Entity>>>();
	_navMeshEntities = std::make_shared<std::map<const std::string, std::shared_ptr<Entity>>>();
	_navObstacleEntities = std::make_shared<std::map<const std::string, std::shared_ptr<Entity>>>();
	
	// Initialize systems
	_nodes = std::make_shared<NodeManager>(_sceneManager);
	_actions = std::make_shared<ActionsManager>(_existingStates[GameState::PLAY]);
	_sound = std::make_shared<SoundManager>();
	_sound->start(_config);
	_graphics = std::make_shared<GraphicSystem>(_sceneManager);
	_physics = std::make_shared<PhysicSystem>(_sceneManager,_idMapper,_config,_actions,_sound,_nodes,_graphics,_entities);
	_animations = std::make_shared<AnimationSystem>(_sceneManager, _idMapper);
	_cameras = std::make_shared<CameraSystem>(smartThis);
	_movement = std::make_shared<MovementSystem>();
	_ai = std::make_shared<AISystem>(_sceneManager,_config,_movement,_idMapper,_nodes,_animations,_sound);

    _ui = std::make_shared<UserInterfaceSystem>(_renderWindow);
    _ui ->initializeUI();

    _conversations = std::make_shared<ConversationSystem>(_ui, smartThis);
    _interactions = std::make_shared<InteractionSystem>(smartThis, _ui, _cameras, false);

	_movement->setPhysicsSystem(_physics);

	GraphicComponent::setSceneManager(_sceneManager);
	CameraComponent::setSceneManager(_sceneManager);

	_inventory = std::make_shared<Inventory>(20);

	auto conver = std::make_shared<Conversation>(true);

	//GameLogic
	_gameLogic = std::make_shared<GameLogic>();
	_eventFactory = std::make_shared<EventFactory>(smartThis);

	// Loaders
	_conversationLoader = std::make_shared<ConversationLoader>(true, _eventFactory);
	_entityExtraLoader = std::make_shared<EntityExtraLoader>(smartThis);
	_conversationLoader->setExtraLoader(_entityExtraLoader);

	_entityExtraLoader->preloadConfiguration();


	// INPUT MANAGER
	_inputManager = make_shared<InputManager>(_config);
	_inputManager->initialise(_renderWindow);

	// Add listen
	_inputManager->addKeyListener(smartThis, "GameManager");
	_inputManager->addMouseListener(smartThis, "GameManager");
	_inputManager->addKeyListener(_ui, "UI");
    _inputManager->addMouseListener(_ui, "UI");
    _inputManager->addJoystickListener(smartThis, "GameManager");

    // Initialize conversation
    ActionNode::setActionManager(_actions);


	// ROOT Listen
	_root->addFrameListener(this);

	// Init to first state
	changeState(state);

	// Start Game Loop
	_root->startRendering();

}

void GameManager::changeState (shared_ptr<GameState> state) {
  // Clear current state
  if (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }

  // Go to new State
  _states.push(state);
  _states.top()->enter();

}

void GameManager::pushState (shared_ptr<GameState> state) {
  // Pause current State
	if (!_states.empty())
		_states.top()->pause();
  
  // Go to new State
  _states.push(state);
  _states.top()->enter();

}

void GameManager::popState () {
  // Clear current State
	if (!_states.empty()) {
		_states.top()->exit();
		_states.pop();
	}

  // Go to back State
  if (!_states.empty())
	  _states.top()->resume();
}

void GameManager::loadResources () {
  
	Ogre::ConfigFile cf;
	cf.load("resources.cfg");

	Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
	Ogre::String sectionstr, typestr, datastr;
	while (sI.hasMoreElements()) {
		sectionstr = sI.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i) {
			typestr = i->first;    datastr = i->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation
			(datastr, typestr, sectionstr);
		}
	}

}

bool GameManager::configure () {

	if (!_root->restoreConfig()) {
		if (!_root->showConfigDialog()) {
			return false;
		}
	}

	shared_ptr<Ogre::RenderWindow> rW(_root->initialise(true, "Basic Game Structure"));
	_renderWindow = rW;

	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

	return true;
}

bool GameManager::frameStarted (const Ogre::FrameEvent& evt) {
  _inputManager->capture();
  return _states.top()->frameStarted(evt);
}

bool GameManager::frameEnded (const Ogre::FrameEvent& evt) {
  return _states.top()->frameEnded(evt);
}

bool GameManager::frameRenderingQueued(const Ogre::FrameEvent & evt) {
        _ui->frameRenderingQueued();
        return true;
}

bool GameManager::keyPressed (const OIS::KeyEvent &e) {
  _states.top()->keyPressed(e);
  return true;
}

bool GameManager::keyReleased (const OIS::KeyEvent &e) {
  _states.top()->keyReleased(e);
  return true;
}

bool GameManager::mouseMoved (const OIS::MouseEvent &e) {
  _states.top()->mouseMoved(e);
  return true;
}

bool GameManager::mousePressed  (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
  _states.top()->mousePressed(e, id);
  return true;
}

bool GameManager::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
  _states.top()->mouseReleased(e, id);
  return true;
}

bool GameManager::povMoved( const OIS::JoyStickEvent &e, int pov ) {
  _states.top()->povMoved(e, pov);
  return true;
}

bool GameManager::axisMoved( const OIS::JoyStickEvent &e, int axis ) {
	_states.top()->axisMoved(e, axis);
	return true;
}

bool GameManager::sliderMoved( const OIS::JoyStickEvent &e, int sliderID ) {
	_states.top()->sliderMoved(e, sliderID);
	return true;
}

bool GameManager::buttonPressed( const OIS::JoyStickEvent &e, int button ) {
	_states.top()->buttonPressed(e, button);
	return true;
}

bool GameManager::buttonReleased( const OIS::JoyStickEvent &e, int button ) {
	_states.top()->buttonReleased(e, button);
	return true;
}

void GameManager::addNewState(int id, shared_ptr<GameState> state) {
	_existingStates[id] = state;
}

shared_ptr<GameState> GameManager::getState(int id) {
	return _existingStates[id];
}

shared_ptr<Ogre::Root> GameManager::getRoot() {
	return _root;
}
shared_ptr<Ogre::SceneManager> GameManager::getSceneManager() {
	return _sceneManager;
}
shared_ptr<Ogre::RenderWindow> GameManager::getRenderWindow() {
	return _renderWindow;
}
shared_ptr<Ogre::Viewport> GameManager::getViewport() {
	return _viewport;
}
shared_ptr<Ogre::Camera> GameManager::getCamera() {
	std::cout << _camera.use_count() << " camera gm\n";
	return _camera;
}

shared_ptr<NodeManager> GameManager::getNodeManager() {
	return _nodes;
}

shared_ptr<ActionsManager> GameManager::getActionsManager() {
	return _actions;
}

shared_ptr<SoundManager> GameManager::getSoundManager() {
	return _sound;
}

shared_ptr<ConversationSystem> GameManager::getConversationSystem() {
 return _conversations;
}

shared_ptr<Configuration> GameManager::getConfig() {
	return _config;
}

std::shared_ptr<ConversationLoader> GameManager::getConversationLoader() {
	return _conversationLoader;
}

std::shared_ptr<EntityExtraLoader> GameManager::getEntityExtraLoader() {
	return _entityExtraLoader;
}

shared_ptr<GraphicSystem> GameManager::getGraphicsSystem() {
	//std::cout << _graphics.use_count() << " graphics\n";
	return _graphics;
}

shared_ptr<PhysicSystem> GameManager::getPhysicsSystem() {
	return _physics;
}

shared_ptr<AnimationSystem> GameManager::getAnimationSystem() {
	return _animations;
}

shared_ptr<CameraSystem> GameManager::getCameraSystem() {
	return _cameras;
}

shared_ptr<MovementSystem> GameManager::getMovementSystem() {
	return _movement;
}

shared_ptr<UserInterfaceSystem> GameManager::getUserInterfaceSystem() {
	return _ui;
}

shared_ptr<InteractionSystem> GameManager::getInteractionSystem() {
	return _interactions;
}

shared_ptr<GameLogic> GameManager::getGameLogic() {
	return _gameLogic;
}

shared_ptr<AISystem> GameManager::getAISystem() {
	return _ai;
}

shared_ptr<Inventory> GameManager::getInventory() {
	return _inventory;
}

shared_ptr<EventFactory> GameManager::getEventFactory() {
	return _eventFactory;
}

std::shared_ptr<IdNameMapper> GameManager::getMapper() {
	return _idMapper;
}

void GameManager::setCamera(shared_ptr<Ogre::Camera> camera) {
	_camera = camera;
}
void GameManager::setViewPort(shared_ptr<Ogre::Viewport> viewport) {
	_viewport = viewport;
}
void GameManager::setGameLogic(shared_ptr<GameLogic> gameLogic) {
	_gameLogic = gameLogic;
}
