#include "InputBindings.h"


InputBindings::InputBindings() {
	_mouseHook = [](const OIS::MouseEvent & event) {};
}

InputBindings::InputBindings(const InputBindings & bindings) {}
InputBindings::InputBindings(const InputBindings && bindings) {}
InputBindings::~InputBindings () {}

InputBindings & InputBindings::operator= (const InputBindings & bindings) { return *this; }
InputBindings & InputBindings::operator= (const InputBindings && bindings) { return *this; }

void InputBindings::setHook(OIS::KeyCode code, function<void(PushState)> f) { _keyHooks[code] = f; }
void InputBindings::setHook(OIS::MouseButtonID id, function<void(const OIS::MouseEvent &, PushState)> f) { _mouseButtonsHooks[id] = f; }
void InputBindings::setHook(JoystickEventType eventType, int id, function<void(int id, const OIS::JoyStickEvent &, PushState)> f) {
	std::pair<JoystickEventType, int> p = std::make_pair(eventType, id);
	_joystickHooks[p] = f;
}
void InputBindings::setHook(function<void(const OIS::MouseEvent &)> f) { _mouseHook = f; }


void InputBindings::executeHook(OIS::KeyCode code, PushState pushState) {
	if(_keyHooks.count(code) > 0)
		_keyHooks[code](pushState);
}

void InputBindings::executeHook(const OIS::MouseEvent & event, OIS::MouseButtonID id, PushState pushState) {
	if(_mouseButtonsHooks.count(id) > 0)
		_mouseButtonsHooks[id](event, pushState);
}

void InputBindings::executeHook(const OIS::JoyStickEvent & event, JoystickEventType eventType, int id, PushState pushState) {
	std::pair<JoystickEventType, int> p = std::make_pair(eventType, id);

	if(_joystickHooks.count(p) > 0)
		_joystickHooks[p](id, event, pushState);
}

void InputBindings::executeHook(const OIS::MouseEvent & event) { _mouseHook(event); }



void InputBindings::removeHook(OIS::KeyCode code) {}

void InputBindings::removeHook(OIS::MouseButtonID id) {}

void InputBindings::removeHook(std::pair<JoystickEventType, int> eventType) {}

void InputBindings::removeMouseHook() {}

void InputBindings::clearHooks() { }
