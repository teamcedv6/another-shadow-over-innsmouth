#include "NodeManager.h"


NodeManager::NodeManager() { assert(false); }

NodeManager::NodeManager(std::shared_ptr<Ogre::SceneManager> sceneManager) : _sceneManager(sceneManager) {}

NodeManager::NodeManager(const NodeManager & nodeManager) {}

NodeManager::NodeManager(const NodeManager && nodeManager) {}

NodeManager::~NodeManager () {}

NodeManager & NodeManager::operator= (const NodeManager & nodeManager) { return *this; }

Ogre::SceneNode * NodeManager::createNode(const std::string & name) {
	//std::cout << "Node " << name << " created \n";
	return _sceneManager->createSceneNode(name);
}

Ogre::SceneNode * NodeManager::getNode (const std::string & name) {
	return _sceneManager->getSceneNode(name);
}

void NodeManager::attachNode(Ogre::SceneNode * node, const std::string & parentName) {
	Ogre::SceneNode * parent;

	if(parentName == "root") {
		parent = _sceneManager->getRootSceneNode();
	}
	else {
		parent = _sceneManager->getSceneNode(parentName);
	}

	parent->addChild(node);
}

void NodeManager::dettachNode(Ogre::SceneNode * node) {
	node->getParent()->removeChild(node);
}

void NodeManager::deleteNode(Ogre::SceneNode * node, const bool recursive) {
	node->detachAllObjects();
	_sceneManager->destroySceneNode(node);
}
