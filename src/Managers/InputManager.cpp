#include "InputManager.h"

InputManager::~InputManager () {
	if (_inputSystem) {
		if (_keyboard) {
			_inputSystem->destroyInputObject(_keyboard.get());
			_keyboard = 0;
		}

		if (_mouse) {
			_inputSystem->destroyInputObject(_mouse.get());
			_mouse = 0;
		}

		OIS::InputManager::destroyInputSystem(_inputSystem);

		_inputSystem = 0;

		// Limpiar todos los listeners.
		_keyListeners.clear();
		_mouseListeners.clear();
	}
}

void InputManager::initialise (shared_ptr<Ogre::RenderWindow> renderWindow) {

	if(!_inputSystem) {

		// Setup basic variables
		OIS::ParamList paramList;
		size_t windowHnd = 0;
		std::ostringstream windowHndStr;

		renderWindow->getCustomAttribute("WINDOW", &windowHnd);
		// Fill parameter list
		windowHndStr << windowHnd;
		paramList.insert(std::make_pair(std::string( "WINDOW"),
		windowHndStr.str()));

		// Hide the OS Mouse Cursor
		paramList.insert(std::make_pair("x11_mouse_hide", std::string("true")));
		// To prevent OIS from kidnapping the mouse ..
		#if defined OIS_WIN32_PLATFORM
		paramList.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND" )));
		paramList.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
		paramList.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
		paramList.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));
		#elif defined OIS_LINUX_PLATFORM
		paramList.insert(std::make_pair(std::string("x11_mouse_grab"), _configuration->getMouseGrab()));
		paramList.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
		paramList.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));
		#endif

		// Create inputsystem
		_inputSystem = OIS::InputManager::createInputSystem(paramList);

		shared_ptr<OIS::Keyboard> k(static_cast<OIS::Keyboard*>(_inputSystem->createInputObject(OIS::OISKeyboard, true)));
		_keyboard = k;
		_keyboard->setEventCallback(this);

		shared_ptr<OIS::Mouse> m(static_cast<OIS::Mouse*>(_inputSystem->createInputObject(OIS::OISMouse, true)));
		_mouse = m;
		_mouse->setEventCallback(this);

		try
		  {

			shared_ptr<OIS::JoyStick> j(static_cast<OIS::JoyStick*>(_inputSystem->createInputObject(OIS::OISJoyStick, true)));
			_joystick = j;
			if(_joystick) _joystick->setEventCallback(this);
		  }
		  catch (OIS::Exception & e)
		  {
		    std::cout << "Gamepad not found. Using only keyboard and mouse...\n";
		  }


		// Get window size
		unsigned int width, height, depth;
		int left, top;
		renderWindow->getMetrics(width, height, depth, left, top);

		// Set mouse region
		this->setWindowExtents( width, height );
	}
}

void InputManager::capture () {
	// Capturar y actualizar cada frame.
	if (_mouse)
		_mouse->capture();

	if (_keyboard)
		_keyboard->capture();

	if(_joystick)
		_joystick->capture();
}

void InputManager::addKeyListener (shared_ptr<OIS::KeyListener> keyListener, const std::string& instanceName) {
	if (_keyboard) {
		// Comprobar si el listener existe.
		itKeyListener = _keyListeners.find(instanceName);
		if (itKeyListener == _keyListeners.end()) {
			_keyListeners[instanceName] = keyListener;
		}
		else {
			// Elemento duplicado; no hacer nada.
		}
	}
}

void InputManager::addMouseListener (shared_ptr<OIS::MouseListener> mouseListener, const std::string& instanceName) {
	if (_mouse) {
		// Comprobar si el listener existe.
		itMouseListener = _mouseListeners.find(instanceName);
		if (itMouseListener == _mouseListeners.end()) {
			_mouseListeners[instanceName] = mouseListener;
		}
		else {
			// Elemento duplicado; no hacer nada.
		}

	}
}

void InputManager::addJoystickListener(shared_ptr<OIS::JoyStickListener> joystickListener, const std::string& instanceName) {
	if (_joystick) {
		// Comprobar si el listener existe.
		itJoystickListener = _joystickListeners.find(instanceName);
		if (itJoystickListener == _joystickListeners.end()) {
			_joystickListeners[instanceName] = joystickListener;
		}
		else {
			// Elemento duplicado; no hacer nada.
		}
	}
}

void InputManager::removeKeyListener (const std::string& instanceName) {
	// Comprobar si el listener existe.
	itKeyListener = _keyListeners.find(instanceName);
	if (itKeyListener != _keyListeners.end()) {
		_keyListeners.erase(itKeyListener);
	}
	else {
		// No hacer nada.
	}
}

void InputManager::removeMouseListener  (const std::string& instanceName) {
	// Comprobar si el listener existe.
	itMouseListener = _mouseListeners.find(instanceName);
	if (itMouseListener != _mouseListeners.end()) {
		_mouseListeners.erase(itMouseListener);
	}
	else {
		// No hacer nada.
	}
}

void InputManager::removeJoystickListener(const std::string& instanceName) {
	// Comprobar si el listener existe.
	itJoystickListener = _joystickListeners.find(instanceName);
	if (itJoystickListener != _joystickListeners.end()) {
		_joystickListeners.erase(itJoystickListener);
	}
	else {
		// No hacer nada.
	}
}

void InputManager::removeKeyListener (shared_ptr<OIS::KeyListener> keyListener) {
	itKeyListener = _keyListeners.begin();
	itKeyListenerEnd = _keyListeners.end();
	for (; itKeyListener != itKeyListenerEnd; ++itKeyListener) {
		if (itKeyListener->second == keyListener) {
			_keyListeners.erase(itKeyListener);
			break;
		}
	}
}

void InputManager::removeMouseListener (shared_ptr<OIS::MouseListener> mouseListener) {
	itMouseListener = _mouseListeners.begin();
	itMouseListenerEnd = _mouseListeners.end();
	for (; itMouseListener != itMouseListenerEnd; ++itMouseListener) {
		if (itMouseListener->second == mouseListener) {
			_mouseListeners.erase(itMouseListener);
			break;
		}
	}
}


void InputManager::removeJoystickListener(shared_ptr<OIS::JoyStickListener> joystickListener) {
	itJoystickListener = _joystickListeners.begin();
	itJoystickListenerEnd = _joystickListeners.end();
	for (; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener) {
		if (itJoystickListener->second == joystickListener) {
			_joystickListeners.erase(itJoystickListener);
			break;
		}
	}
}

void InputManager::removeAllListeners() {
	_keyListeners.clear();
	_mouseListeners.clear();
}

void InputManager::removeAllKeyListeners () {
	_keyListeners.clear();
}

void InputManager::removeAllMouseListeners () {
	_mouseListeners.clear();
}

void InputManager::removeAllJoystickListener () {
	_joystickListeners.clear();
}

void InputManager::setWindowExtents (int width, int height) {
	// Establecer la región del ratón.
	// Llamar al hacer un resize.
	const OIS::MouseState &mouseState = _mouse->getMouseState();
	mouseState.width = width;
	mouseState.height = height;
}

shared_ptr<OIS::Keyboard> InputManager::getKeyboard () {
	return _keyboard;
}

shared_ptr<OIS::Mouse> InputManager::getMouse () {
	return _mouse;
}

shared_ptr<OIS::JoyStick> InputManager::getJoystick () {
	return _joystick;
}

bool InputManager::keyPressed (const OIS::KeyEvent &e) {
	itKeyListener = _keyListeners.begin();
	itKeyListenerEnd = _keyListeners.end();
	// Delega en los KeyListener añadidos.
	for (; itKeyListener != itKeyListenerEnd; ++itKeyListener) {
		itKeyListener->second->keyPressed(e);
	}

	return true;
}

bool InputManager::keyReleased (const OIS::KeyEvent &e) {
	itKeyListener = _keyListeners.begin();
	itKeyListenerEnd = _keyListeners.end();
	// Delega en los KeyListener añadidos.
	for (; itKeyListener != itKeyListenerEnd; ++itKeyListener) {
		itKeyListener->second->keyReleased( e );
	}

	return true;
}

bool InputManager::mouseMoved (const OIS::MouseEvent &e) {
	itMouseListener = _mouseListeners.begin();
	itMouseListenerEnd = _mouseListeners.end();
	// Delega en los MouseListener añadidos.
	for (; itMouseListener != itMouseListenerEnd; ++itMouseListener) {
		itMouseListener->second->mouseMoved( e );
	}

	return true;
}

bool InputManager::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
	itMouseListener = _mouseListeners.begin();
	itMouseListenerEnd = _mouseListeners.end();
	// Delega en los MouseListener añadidos.
	for (; itMouseListener != itMouseListenerEnd; ++itMouseListener) {
		itMouseListener->second->mousePressed( e, id );
	}

	return true;
}

bool InputManager::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
	itMouseListener = _mouseListeners.begin();
	itMouseListenerEnd = _mouseListeners.end();
	// Delega en los MouseListener añadidos.
	for (; itMouseListener != itMouseListenerEnd; ++itMouseListener) {
		itMouseListener->second->mouseReleased( e, id );
	}

	return true;
}

bool InputManager::povMoved( const OIS::JoyStickEvent &e, int pov ) {
	//std::cout << "povMoved " << pov << " " << e.state.mPOV[pov].direction << " E: " << e.state.mPOV[pov].East << " W: " << e.state.mPOV[pov].West <<
			//" N: " << e.state.mPOV[pov].North << " S: " << e.state.mPOV[pov].South << " Cent. " << e.state.mPOV[pov].Centered << "\n";
	itJoystickListener = _joystickListeners.begin();
	itJoystickListenerEnd = _joystickListeners.end();
	// Delega en los MouseListener añadidos.
	for (; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener) {
		itJoystickListener->second->povMoved(e, pov);
	}

	return true;
}

bool InputManager::axisMoved( const OIS::JoyStickEvent &e, int axis ) {
	//std::cout << "axisMoved " << axis << " " << e.state.mAxes[axis].abs << "\n";
	itJoystickListener = _joystickListeners.begin();
	itJoystickListenerEnd = _joystickListeners.end();
	// Delega en los MouseListener añadidos.
	for (; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener) {
		itJoystickListener->second->axisMoved(e, axis);
	}

	return true;
}

bool InputManager::sliderMoved( const OIS::JoyStickEvent &e, int sliderID ) {
	//std::cout << "sliderMoved " << sliderID << "\n";
	itJoystickListener = _joystickListeners.begin();
	itJoystickListenerEnd = _joystickListeners.end();
	// Delega en los MouseListener añadidos.
	for (; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener) {
		itJoystickListener->second->sliderMoved(e, sliderID);
	}

	return true;
}

bool InputManager::buttonPressed( const OIS::JoyStickEvent &e, int button ) {
	std::cout << "buttonPressed " << button << "\n";
	itJoystickListener = _joystickListeners.begin();
	itJoystickListenerEnd = _joystickListeners.end();
	// Delega en los MouseListener añadidos.
	for (; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener) {
		itJoystickListener->second->buttonPressed(e, button);
	}

	return true;
}

bool InputManager::buttonReleased( const OIS::JoyStickEvent &e, int button ) {
	//std::cout << "buttonReleased " << button << "\n";
	itJoystickListener = _joystickListeners.begin();
	itJoystickListenerEnd = _joystickListeners.end();
	// Delega en los MouseListener añadidos.
	for (; itJoystickListener != itJoystickListenerEnd; ++itJoystickListener) {
		itJoystickListener->second->buttonReleased(e, button);
	}

	return true;
}

