#include "SoundManager.h"

SoundManager::SoundManager(){
	// Sounds Managers
	std::shared_ptr<TrackManager> smartTrackManager(new TrackManager);
	_pTrackManager = smartTrackManager;

	std::shared_ptr<SoundFXManager> smartSoundFxManager(new SoundFXManager);
	_pSoundFXManager = smartSoundFxManager;

	// Resources
	std::shared_ptr< std::map<std::string,TrackPtr> > smartMusicList(new std::map<std::string, TrackPtr>);
	_musicList = smartMusicList;

	std::shared_ptr< std::map<std::string, SoundFXPtr> > smartFxList(new std::map<std::string, SoundFXPtr>);
	_fxList = smartFxList;
}

SoundManager::~SoundManager () {}

SoundManager::SoundManager(const SoundManager & soundManager) {

	_pTrackManager = soundManager._pTrackManager;
	_pSoundFXManager = soundManager._pSoundFXManager;
	_musicList = soundManager._musicList;
	_fxList = soundManager._fxList;
}

SoundManager::SoundManager(const SoundManager && soundManager) {

	_pTrackManager = soundManager._pTrackManager;
	_pSoundFXManager = soundManager._pSoundFXManager;
	_musicList = soundManager._musicList;
	_fxList = soundManager._fxList;
}


SoundManager& SoundManager::operator = (const SoundManager &soundManager){

	_pTrackManager = soundManager._pTrackManager;
	_pSoundFXManager = soundManager._pSoundFXManager;
	_musicList = soundManager._musicList;
	_fxList = soundManager._fxList;

	return *this;

}

void SoundManager::start (std::shared_ptr<Configuration> configuration)
{
	// Init SDL
	_initSDL();
	_loadMusic(configuration);
	_loadFx(configuration);
}

void SoundManager::_loadMusic(std::shared_ptr<Configuration> configuration) {
	 for (std::map<std::string,std::string>::iterator it = configuration->getMusicList()->begin(); it != configuration->getMusicList()->end(); ++it){
		 TrackPtr myMusic = _pTrackManager->load(it->second,"Music");
		 (*_musicList)[it->first] = myMusic;
	 }
}

void SoundManager::_loadFx(std::shared_ptr<Configuration> configuration) {
	for (std::map<std::string,std::string>::iterator it = configuration->getFxList()->begin() ; it != configuration->getFxList()->end(); ++it){
		 SoundFXPtr myFx = _pSoundFXManager->load(it->second,"Fx");
		 (*_fxList)[it->first] = myFx;
	 }
}

void SoundManager::selectResource(TYPE type, std::string key, ACTION action, int volume,int loop) {

	switch (type) {
		case TYPE::MUSIC:

			switch (action) {
				case ACTION::PLAY:
						(*_musicList)[key]->play(volume,loop);
					break;
				case ACTION::STOP:
						(*_musicList)[key]->stop();
					break;
			}

			break;
		case TYPE::FX:
				(*_fxList)[key]->play(volume,loop);
			break;
	}
}

bool SoundManager::_initSDL() {
  if (SDL_Init(SDL_INIT_AUDIO) < 0) {
    return false;
  }
  // Llamar a  SDL_Quit al terminar.
  atexit(SDL_Quit);

  // Inicializando SDL mixer...
  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS, 4096) < 0) {
    return false;
  }

  // Llamar a Mix_CloseAudio al terminar.
  atexit(Mix_CloseAudio);

  return true;
}
