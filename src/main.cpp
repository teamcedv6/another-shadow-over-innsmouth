/*********************************************************************
 * Basic Game Structure - Ogre3d - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/


#include "GameManager.h"
#include "GameState.h"

#include "MainMenuState.h"
#include "IngameMenuState.h"
#include "AuthorState.h"
#include "ScoreState.h"
#include "OptionsState.h"
#include "LoadState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "ConversationState.h"
#include "AIState.h"
#include "InventoryState.h"
#include "InterludeState.h"

#include <iostream>

using namespace std;

 int main () {

	// Game Manager - only instance
	auto gameManagerPtr = make_shared<GameManager>();

	// Game States - only instance
	auto mainMenuStatePtr = std::make_shared<MainMenuState>(gameManagerPtr);
	auto ingameMenuStatePtr = std::make_shared<IngameMenuState>(gameManagerPtr);
	auto authorStatePtr = std::make_shared<AuthorState>(gameManagerPtr);
	auto scoreStatePtr = std::make_shared<ScoreState>(gameManagerPtr);
	auto optionsStatePtr = std::make_shared<OptionsState>(gameManagerPtr);
	auto loadStatePtr = std::make_shared<LoadState>(gameManagerPtr);
	auto playStatePtr = std::make_shared<PlayState>(gameManagerPtr);
	auto pauseStatePtr = std::make_shared<PauseState>(gameManagerPtr);
	auto conversationStatePtr = std::make_shared<ConversationState>(gameManagerPtr);
	auto aiStatePtr = std::make_shared<AIState>(gameManagerPtr);
	auto inventoryStatePtr = std::make_shared<InventoryState>(gameManagerPtr);
	auto interludeStatePtr = std::make_shared<InterludeState>(gameManagerPtr);

	// Map Game States
	gameManagerPtr->addNewState(GameState::States::MAIN_MENU,mainMenuStatePtr);
	gameManagerPtr->addNewState(GameState::States::INGAME_MENU,ingameMenuStatePtr);
	gameManagerPtr->addNewState(GameState::States::AUTHOR,authorStatePtr);
	gameManagerPtr->addNewState(GameState::States::SCORE,scoreStatePtr);
	gameManagerPtr->addNewState(GameState::States::OPTIONS,optionsStatePtr);
	gameManagerPtr->addNewState(GameState::States::LOAD,loadStatePtr);
	gameManagerPtr->addNewState(GameState::States::PLAY,playStatePtr);
	gameManagerPtr->addNewState(GameState::States::PAUSE,pauseStatePtr);
	gameManagerPtr->addNewState(GameState::States::CONVERSATION,conversationStatePtr);
	gameManagerPtr->addNewState(GameState::States::AI,aiStatePtr);
	gameManagerPtr->addNewState(GameState::States::INVENTORY, inventoryStatePtr);
	gameManagerPtr->addNewState(GameState::States::INTERLUDE, interludeStatePtr);

	// Main menu
	function<void(InputBindings::PushState)> fMainExit = std::bind(&MainMenuState::exitGame, mainMenuStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fMenuStart = std::bind(&MainMenuState::startGame, mainMenuStatePtr.get(), std::placeholders::_1);
	//function<void(InputBindings::PushState)> fAIState = std::bind(&MainMenuState::goAIState, mainMenuStatePtr.get(), std::placeholders::_1);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fMenuGamepadStart = std::bind(&MainMenuState::startGame, mainMenuStatePtr.get(), std::placeholders::_3);
	mainMenuStatePtr->getInputBindings()->setHook(OIS::KC_RETURN, fMenuStart);
	mainMenuStatePtr->getInputBindings()->setHook(OIS::KC_ESCAPE, fMainExit);
	mainMenuStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 1, fMenuGamepadStart);

	// Play
	function<void(InputBindings::PushState)> fPlayExit = std::bind(&PlayState::exitGame, playStatePtr.get(), std::placeholders::_1);
	//function<void(InputBindings::PushState)> fChangeDebuga = std::bind(&PlayState::aiDebug, playStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fPlayUp = std::bind(&PlayState::moveMainCharacterKeyboard, playStatePtr.get(), std::placeholders::_1, 0, -1);
	function<void(InputBindings::PushState)> fPlayDown = std::bind(&PlayState::moveMainCharacterKeyboard, playStatePtr.get(), std::placeholders::_1, 0, 1);
	function<void(InputBindings::PushState)> fPlayRight = std::bind(&PlayState::moveMainCharacterKeyboard, playStatePtr.get(), std::placeholders::_1, 1, 0);
	function<void(InputBindings::PushState)> fPlayLeft = std::bind(&PlayState::moveMainCharacterKeyboard, playStatePtr.get(), std::placeholders::_1, -1, 0);
	function<void(InputBindings::PushState)> fPlayAction0 = std::bind(&PlayState::actionPush, playStatePtr.get(), std::placeholders::_1, 0);
	function<void(InputBindings::PushState)> fPlayAction1 = std::bind(&PlayState::actionPush, playStatePtr.get(), std::placeholders::_1, 1);
	function<void(InputBindings::PushState)> fPlayAction2 = std::bind(&PlayState::actionPush, playStatePtr.get(), std::placeholders::_1, 2);
	function<void(InputBindings::PushState)> fPlayAction3 = std::bind(&PlayState::actionPush, playStatePtr.get(), std::placeholders::_1, 3);
	function<void(InputBindings::PushState)> fPlayOpenInventory = std::bind(&PlayState::openInventory, playStatePtr.get(), std::placeholders::_1);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fPlayGamepadAxis = std::bind(&PlayState::moveMainCharacterGamepad, playStatePtr.get(), std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fPlayGamepadAction0 = std::bind(&PlayState::actionPush, playStatePtr.get(), std::placeholders::_3, 0);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fPlayGamepadAction1 = std::bind(&PlayState::actionPush, playStatePtr.get(), std::placeholders::_3, 1);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fPlayGamepadAction2 = std::bind(&PlayState::actionPush, playStatePtr.get(), std::placeholders::_3, 2);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fPlayGamepadAction3 = std::bind(&PlayState::actionPush, playStatePtr.get(), std::placeholders::_3, 3);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fPlayGamepadOpenInventory = std::bind(&PlayState::openInventory, playStatePtr.get(), std::placeholders::_3);




	playStatePtr->getInputBindings()->setHook(OIS::KC_ESCAPE, fPlayExit);
	playStatePtr->getInputBindings()->setHook(OIS::KC_UP, fPlayUp);
	playStatePtr->getInputBindings()->setHook(OIS::KC_DOWN, fPlayDown);
	playStatePtr->getInputBindings()->setHook(OIS::KC_LEFT, fPlayLeft);
	playStatePtr->getInputBindings()->setHook(OIS::KC_RIGHT, fPlayRight);
	playStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::AXIS, 0, fPlayGamepadAxis);
	playStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::AXIS, 1, fPlayGamepadAxis);
	playStatePtr->getInputBindings()->setHook(OIS::KC_W, fPlayAction0);
	playStatePtr->getInputBindings()->setHook(OIS::KC_D, fPlayAction1);
	playStatePtr->getInputBindings()->setHook(OIS::KC_S, fPlayAction2);
	playStatePtr->getInputBindings()->setHook(OIS::KC_A, fPlayAction3);
	playStatePtr->getInputBindings()->setHook(OIS::KC_D, fPlayAction3);
	playStatePtr->getInputBindings()->setHook(OIS::KC_I, fPlayOpenInventory);
	playStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 0, fPlayGamepadAction0);
	playStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 1, fPlayGamepadAction1);
	playStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 2, fPlayGamepadAction2);
	playStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 3, fPlayGamepadAction3);
	playStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 8, fPlayGamepadOpenInventory);
	//playStatePtr->getInputBindings()->setHook(OIS::KC_1, fChangeDebuga);


	// Conversation
	function<void(InputBindings::PushState)> fConversationPush = std::bind(&ConversationState::push, conversationStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fConversationBack = std::bind(&ConversationState::back, conversationStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fConversationUp = std::bind(&ConversationState::up, conversationStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fConversationDown = std::bind(&ConversationState::down, conversationStatePtr.get(), std::placeholders::_1);

	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fConversationPushGamePad = std::bind(&ConversationState::push, conversationStatePtr.get(), std::placeholders::_3);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fConversationBackGamePad = std::bind(&ConversationState::back, conversationStatePtr.get(), std::placeholders::_3);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fConversationUpDownGamePad = std::bind(&ConversationState::gamePadUpDown, conversationStatePtr.get(), std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);


	conversationStatePtr->getInputBindings()->setHook(OIS::KC_SPACE, fConversationPush);
	conversationStatePtr->getInputBindings()->setHook(OIS::KC_ESCAPE, fConversationBack);
	conversationStatePtr->getInputBindings()->setHook(OIS::KC_UP, fConversationUp);
	conversationStatePtr->getInputBindings()->setHook(OIS::KC_DOWN, fConversationDown);
	conversationStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 2, fConversationPushGamePad);
	conversationStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 0, fConversationBackGamePad);
	conversationStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::AXIS, 1, fConversationUpDownGamePad);

	// AI State
	/*function<void(InputBindings::PushState)> fAiExit = std::bind(&AIState::exitGame, aiStatePtr.get(), std::placeholders::_1);
	aiStatePtr->getInputBindings()->setHook(OIS::KC_ESCAPE, fAiExit);

	function<void(InputBindings::PushState)> fO1 =std::bind(&AIState::obstacle1, aiStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fO2 =std::bind(&AIState::obstacle2, aiStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fO3 =std::bind(&AIState::obstacle3, aiStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fO4 =std::bind(&AIState::obstacle4, aiStatePtr.get(), std::placeholders::_1);
	aiStatePtr->getInputBindings()->setHook(OIS::KC_Z, fO1);
	aiStatePtr->getInputBindings()->setHook(OIS::KC_X, fO2);
	aiStatePtr->getInputBindings()->setHook(OIS::KC_C, fO3);
	aiStatePtr->getInputBindings()->setHook(OIS::KC_V, fO4);

	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fAIGamepadAxis = std::bind(&AIState::moveMainCharacterGamepad, aiStatePtr.get(), std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
	aiStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::AXIS, 0, fAIGamepadAxis);
	aiStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::AXIS, 1, fAIGamepadAxis);


	function<void(InputBindings::PushState)> fPlayUpAI = std::bind(&AIState::moveMainCharacterKeyboard, aiStatePtr.get(), std::placeholders::_1, 0, -1);
	function<void(InputBindings::PushState)> fPlayDownAI = std::bind(&AIState::moveMainCharacterKeyboard, aiStatePtr.get(), std::placeholders::_1, 0, 1);
	function<void(InputBindings::PushState)> fPlayRightAI = std::bind(&AIState::moveMainCharacterKeyboard, aiStatePtr.get(), std::placeholders::_1, 1, 0);
	function<void(InputBindings::PushState)> fPlayLeftAI = std::bind(&AIState::moveMainCharacterKeyboard, aiStatePtr.get(), std::placeholders::_1, -1, 0);


	aiStatePtr->getInputBindings()->setHook(OIS::KC_UP, fPlayUpAI);
	aiStatePtr->getInputBindings()->setHook(OIS::KC_DOWN, fPlayDownAI);
	aiStatePtr->getInputBindings()->setHook(OIS::KC_LEFT, fPlayRightAI);
	aiStatePtr->getInputBindings()->setHook(OIS::KC_RIGHT, fPlayLeftAI);*/

	// Inventory State
	function<void(InputBindings::PushState)> fInventorySelect = std::bind(&InventoryState::select, inventoryStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fInventoryLook = std::bind(&InventoryState::look, inventoryStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fInventoryBack = std::bind(&InventoryState::back, inventoryStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fInventoryUp = std::bind(&InventoryState::up, inventoryStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fInventoryDown = std::bind(&InventoryState::down, inventoryStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fInventoryRight = std::bind(&InventoryState::right, inventoryStatePtr.get(), std::placeholders::_1);
	function<void(InputBindings::PushState)> fInventoryLeft = std::bind(&InventoryState::left, inventoryStatePtr.get(), std::placeholders::_1);

	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fInventorySelectGamePad = std::bind(&InventoryState::select, inventoryStatePtr.get(), std::placeholders::_3);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fInventoryLookGamePad = std::bind(&InventoryState::look, inventoryStatePtr.get(), std::placeholders::_3);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fInventoryBackGamePad = std::bind(&InventoryState::back, inventoryStatePtr.get(), std::placeholders::_3);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fInventoryUpDownGamePad = std::bind(&InventoryState::gamePadMove, inventoryStatePtr.get(), std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
	function<void(int, const OIS::JoyStickEvent &, InputBindings::PushState)> fInventoryLeftRightGamePad = std::bind(&InventoryState::gamePadMove, inventoryStatePtr.get(), std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);

	inventoryStatePtr->getInputBindings()->setHook(OIS::KC_S, fInventorySelect);
	inventoryStatePtr->getInputBindings()->setHook(OIS::KC_ESCAPE, fInventoryBack);
	inventoryStatePtr->getInputBindings()->setHook(OIS::KC_I, fInventoryBack);
	inventoryStatePtr->getInputBindings()->setHook(OIS::KC_W, fInventoryLook);
	inventoryStatePtr->getInputBindings()->setHook(OIS::KC_UP, fInventoryUp);
	inventoryStatePtr->getInputBindings()->setHook(OIS::KC_DOWN, fInventoryDown);
	inventoryStatePtr->getInputBindings()->setHook(OIS::KC_LEFT, fInventoryLeft);
	inventoryStatePtr->getInputBindings()->setHook(OIS::KC_RIGHT, fInventoryRight);
	inventoryStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 2, fInventorySelectGamePad);
	inventoryStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 0, fInventoryLookGamePad);
	inventoryStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 1, fInventoryBackGamePad);
	inventoryStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::BUTTON, 8, fInventoryBackGamePad);
	inventoryStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::AXIS, 1, fInventoryUpDownGamePad);
	inventoryStatePtr->getInputBindings()->setHook(InputBindings::JoystickEventType::AXIS, 0, fInventoryLeftRightGamePad);

	try {
		// Init Game

		gameManagerPtr->start(mainMenuStatePtr);
		//gameManagerPtr->start(playStatePtr);
		//gameManagerPtr->start(aiStatePtr);
	} catch (Ogre::Exception& e) {
		std::cerr << "Exception detected: " << e.getFullDescription();
	}

	// TODO: Ver liberación de memoria en GameManager e InputManager (No pasa por los destructores, hacer release?)

	return 0;
}
