#include "Configuration.h"

Configuration::Configuration(){
	_physicConfig = std::make_shared<Physic_t>();
	_aiConfig = std::make_shared<Ai_t>();
	_musicList = std::make_shared<std::map<std::string, std::string>>();
	_fxList = std::make_shared<std::map<std::string, std::string>>();
	_customList = std::make_shared<std::map<int, std::shared_ptr<Item_t>>>();

	_keyRandomName = 0;
	_mouseGrab = "false";
}
Configuration::~Configuration (){}

Configuration::Configuration(const Configuration & configuration) {
	_physicConfig = configuration._physicConfig;
	_aiConfig = configuration._aiConfig;
	_musicList = configuration._musicList;
	_fxList = configuration._fxList;
	_customList = configuration._customList;
	_keyRandomName = configuration._keyRandomName;

	_mouseGrab = configuration._mouseGrab;
}
Configuration::Configuration(const Configuration && configuration) {
	_physicConfig = configuration._physicConfig;
	_aiConfig = configuration._aiConfig;
	_musicList = configuration._musicList;
	_fxList = configuration._fxList;
	_customList = configuration._customList;
	_keyRandomName = configuration._keyRandomName;
	_mouseGrab = configuration._mouseGrab;
}

Configuration& Configuration::operator = (const Configuration &configuration){
	_physicConfig = configuration._physicConfig;
	_aiConfig = configuration._aiConfig;
	_musicList = configuration._musicList;
	_fxList = configuration._fxList;
	_customList = configuration._customList;
	_keyRandomName = configuration._keyRandomName;

	return *this;
}

// Setters Option
void Configuration::setPhysicConfig(bool debug, float gravity, float worldBounds, float bodyRestitution, float bodyFriction, float bodyMass) {
	_physicConfig->debug = debug;
	_physicConfig->gravity = gravity;
	_physicConfig->worldBounds = worldBounds;
	_physicConfig->bodyRestitution = bodyRestitution;
	_physicConfig->bodyFriction = bodyFriction;
	_physicConfig->bodyMass = bodyMass;
}

void Configuration::setAiConfig(bool debug, bool singleNavMesh, std::string player) {
	_aiConfig->debug = debug;
	_aiConfig->singleNavMesh = singleNavMesh;
	_aiConfig->player = player;
}

void Configuration::setMusic(std::string key, std::string value) {
	(*_musicList)[key] = value;
}

void Configuration::setFx(std::string key, std::string value) {
	(*_fxList)[key] = value;
}

void Configuration::setCustom(int key, std::shared_ptr<Item_t> value) {
	(*_customList)[key] = value;
}

void Configuration::setMouseGrab(bool active) {
	_mouseGrab = active;
}

// Getters
std::shared_ptr<Configuration::Physic_t> Configuration::getPhysicConfig() {
	return _physicConfig;
}

std::shared_ptr<Configuration::Ai_t> Configuration::getAiConfig() {
	return _aiConfig;
}

std::shared_ptr<std::map<std::string, std::string>> Configuration::getMusicList() {
	return _musicList;
}

std::shared_ptr<std::map<std::string, std::string>> Configuration::getFxList() {
	return _fxList;
}

std::shared_ptr<std::map<int, std::shared_ptr<Configuration::Item_t>>> Configuration::getCustomList() {
	return _customList;
}

std::string Configuration::getMouseGrab() {
	return _mouseGrab;
}

void Configuration::loadConfig() {

	auto xmlEditor = std::make_shared<XmlEditor>();
	xmlEditor->loadDocument("config.xml","Config","config");

	assert(xmlEditor->isLoad());

	_proccessConfig(xmlEditor->getXmlDoc()->RootElement());

}

void Configuration::writeConfig() {
	auto xmlEditor = std::make_shared<XmlEditor>();
	xmlEditor->loadDocument("config.xml","Config","config");

	assert(xmlEditor->isLoad());

	auto configXML = xmlEditor->getXmlDoc();
	xmlEditor->writeDocument(configXML,"config/","config.xml",true);

	TiXmlText* test = new TiXmlText("false");
	configXML->RootElement()->FirstChildElement("physic")->FirstChildElement("debug")->Clear();
	configXML->RootElement()->FirstChildElement("physic")->FirstChildElement("debug")->LinkEndChild(test);

	xmlEditor->writeDocument(configXML,"config/","config.xml",false);
}

bool Configuration::_proccessConfig(TiXmlElement* rootXml) {

	_mouseGrab = rootXml->Attribute("mouseGrab");

	// ProccessPhysic
	TiXmlElement * physic = rootXml->FirstChildElement("physic");
		if(physic)
			_proccessPhysic(physic);

	// ProccessCustom
	TiXmlElement * ai = rootXml->FirstChildElement("ai");
		if(ai)
			_proccessAi(ai);

	// ProccessSound
	TiXmlElement * sound = rootXml->FirstChildElement("sound");
		if(sound)
			_proccessSound(sound);

	// ProccessCustom
	TiXmlElement * custom = rootXml->FirstChildElement("custom");
		if(custom)
			_proccessCustom(custom);

	// ProccessCustom
	TiXmlElement * gamelogic = rootXml->FirstChildElement("gamelogic");
		if(gamelogic)
			_proccessGameLogic(gamelogic);

	return true;
}

void Configuration::_proccessPhysic(TiXmlElement* physic) {

	bool debug;
	std::istringstream(physic->FirstChildElement("debug")->GetText()) >> std::boolalpha >> debug;
	float worldBounds = atof(physic->FirstChildElement("worldBounds")->GetText());
	float gravity = atof(physic->FirstChildElement("gravity")->GetText());
	float bodyRestitution = atof(physic->FirstChildElement("bodyRestitution")->GetText());
	float bodyFriction = atof(physic->FirstChildElement("bodyFriction")->GetText());
	float bodyMass = atof(physic->FirstChildElement("bodyMass")->GetText());

	_physicConfig->debug = debug;
	_physicConfig->worldBounds = worldBounds;
	_physicConfig->gravity = gravity;
	_physicConfig->bodyRestitution = bodyRestitution;
	_physicConfig->bodyFriction = bodyFriction;
	_physicConfig->bodyMass = bodyMass;

}

void Configuration::_proccessAi(TiXmlElement* ai) {

	bool debug;
	bool singleNavMesh;
	std::istringstream(ai->FirstChildElement("debug")->GetText()) >> std::boolalpha >> debug;
	std::istringstream(ai->FirstChildElement("singleNavMesh")->GetText()) >> std::boolalpha >> singleNavMesh;
	std::string player = ai->FirstChildElement("player")->GetText();

	_aiConfig->debug = debug;
	_aiConfig->singleNavMesh = singleNavMesh;
	_aiConfig->player = player;

}

void Configuration::_proccessSound(TiXmlElement* sound) {

	TiXmlElement* music = sound->FirstChildElement("music");
	for(TiXmlElement* e = music->FirstChildElement("track"); e != NULL; e = e->NextSiblingElement("track")) {
		(*_musicList)[e->Attribute("key")] = e->GetText();
	}

	TiXmlElement* fx = sound->FirstChildElement("fx");
	for(TiXmlElement* e = fx->FirstChildElement("track"); e != NULL; e = e->NextSiblingElement("track")) {
		(*_fxList)[e->Attribute("key")] = e->GetText();
	}
}

void Configuration::_proccessCustom(TiXmlElement* custom) {

	for(TiXmlElement* e = custom->FirstChildElement("item"); e != NULL; e = e->NextSiblingElement("item")) {

		std::shared_ptr<Item_t> item = std::make_shared<Item_t>();
		_getAttributes(item, e);
		item->name = e->GetText();

		(*_customList)[atoi(e->Attribute("key"))] = item;
	}
}

void Configuration::_proccessGameLogic(TiXmlElement* gameLogic){



}

void Configuration::_getAttributes(std::shared_ptr<Item_t> item, TiXmlElement* element) {

	if(element->Attribute("bodyRestitution")) {
		item->bodyRestitution =atof(element->Attribute("bodyRestitution"));
	} else {
		item->bodyRestitution = _physicConfig->bodyRestitution;
	}

	if(element->Attribute("bodyFriction")) {
		item->bodyFriction = atof(element->Attribute("bodyFriction"));
	} else {
		item->bodyFriction = _physicConfig->bodyFriction;
	}

	if(element->Attribute("bodyMass")) {
		item->bodyMass = atof(element->Attribute("bodyMass"));
	} else {
		item->bodyMass = _physicConfig->bodyMass;
	}
}

int Configuration::getKeyRandomName() {

	int actualKey = _keyRandomName;
	_keyRandomName++;

	return actualKey;
}
