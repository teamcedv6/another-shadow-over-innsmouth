	#include "GameLogic.h"

GameLogic::GameLogic(){
}

GameLogic::GameLogic(const GameLogic & gameLogic) {

}

GameLogic::GameLogic(const GameLogic && gameLogic) {

}

GameLogic::~GameLogic (){}

GameLogic& GameLogic::operator = (const GameLogic &gameLogic){

	return *this;
}

void GameLogic::resetGame() {


}

void GameLogic::setValue(const std::string & name, int value, bool relative) {
	if(relative) {
		_storyVariables[name] += value;
	}
	else {
		_storyVariables[name] = value;
	}

}

int GameLogic::getValue(const std::string & name) {
	return _storyVariables[name];
}

