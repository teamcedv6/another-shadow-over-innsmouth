#include "Inventory.h"
#include <iostream>

Inventory::Inventory() : _maxCapacity(10), _usedCapacity(0) {
	_objects = std::make_shared<std::vector<std::shared_ptr<mInventoryObject>>>();

}

Inventory::Inventory(int capacity) : _maxCapacity(capacity), _usedCapacity(0) {
	_objects = std::make_shared<std::vector<std::shared_ptr<mInventoryObject>>>();
}

Inventory::Inventory(const Inventory & inventory) : _maxCapacity(inventory._maxCapacity), _usedCapacity(inventory._usedCapacity){

}

Inventory::Inventory(const Inventory && inventory) :  _maxCapacity(inventory._maxCapacity), _usedCapacity(inventory._usedCapacity){

}

Inventory::~Inventory() {

}

Inventory & Inventory::operator = (const Inventory &inventory) {
	return *this;
}

void Inventory::createObject(const std::string & name, const std::string & picture) {
	std::shared_ptr<mInventoryObject> object = std::make_shared<mInventoryObject>();
	//mInventoryObject object;
	object->name = name;
	object->picture = picture;
	//std::shared_ptr<mInventoryObject> objectPtr(object);
	_possibleObjects[name] = object;

}

bool Inventory::pickObject(const std::string & name) {
	if(_possibleObjects.count(name) > 0 && _usedCapacity < _maxCapacity) {
		_objects->push_back(_possibleObjects[name]);
		_usedCapacity++;
		std::cout << "object " << name << " picked (inventory)\n";

		return true;
	}
	else return _usedCapacity < _maxCapacity;
}

void Inventory::dropObject(const std::string & name) {

	/*auto obj = std::find_if(_objects.begin(), _objects.end(), [](const std::shared_ptr<mInventoryObject> & obj)->bool{ return obj->name == "";});

	if(obj != _objects.end()) {
		_objects.erase(obj);
	}*/
	std::shared_ptr<mInventoryObject> found = nullptr;
	auto it = _objects->begin();

	while(found != nullptr && it != _objects->end()) {
		if(_compareObjects(*it, name)) { found = *it; }
		it++;
	}

	//if(found != _objects.end()) return *found;
	//else return nullptr;
	if(it != _objects->end()) _objects->erase(it);
}

bool Inventory::hasObject(const std::string & name) {
	auto found = getObject(name);

	return found != nullptr;
}

std::shared_ptr<Inventory::mInventoryObject> Inventory::getObject(const std::string & name) {
	/*auto found = std::find_if(_objects.begin(), _objects.end(), [](const std::shared_ptr<mInventoryObject> & obj)->bool{ return obj->name == "";});*/
	 //std::function<bool(const std::shared_ptr<mInventoryObject>&)> f = std::bind(&Inventory::_compareObjects, this, std::placeholders::_1, name);
	 //auto found = std::find_if(_objects.begin(), _objects.end(), f);
	std::shared_ptr<mInventoryObject> found = nullptr;
	auto it = _objects->begin();

	while(found != nullptr && it != _objects->end()) {
		if(_compareObjects(*it, name)) { found = *it; }
		it++;
	}

	//if(found != _objects.end()) return *found;
	//else return nullptr;
	return found;
}

std::shared_ptr<std::vector<std::shared_ptr<Inventory::mInventoryObject>>> Inventory::getAllObjects() {
	return _objects;
}

bool Inventory::_compareObjects(const std::shared_ptr<mInventoryObject> & object, const std::string & name) {
	return object->name == name;
}

