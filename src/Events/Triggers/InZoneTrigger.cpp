#include "InZoneTrigger.h"

InZoneTrigger::InZoneTrigger() : InZoneTrigger(nullptr, Ogre::Vector2(0,0), Ogre::Vector2(1,1)) {};

InZoneTrigger::InZoneTrigger(Ogre::SceneNode * node, const Ogre::Vector2 & point1, const Ogre::Vector2 & point2) : Trigger(),  _node(node), _p1(point1), _p2(point2) {};

InZoneTrigger::InZoneTrigger(const InZoneTrigger & inZoneTrigger) : Trigger(inZoneTrigger),  _node(inZoneTrigger._node), _p1(inZoneTrigger._p1), _p2(inZoneTrigger._p2){};

InZoneTrigger::InZoneTrigger(const InZoneTrigger && inZoneTrigger) : Trigger(inZoneTrigger), _node(inZoneTrigger._node), _p1(inZoneTrigger._p1), _p2(inZoneTrigger._p2){};

InZoneTrigger::~InZoneTrigger() {};

InZoneTrigger & InZoneTrigger::operator= (const InZoneTrigger & inZoneTrigger) {
	_node = inZoneTrigger._node;
	_p1 = inZoneTrigger._p1;
	_p2 = inZoneTrigger._p2;
	return *this;
}

bool InZoneTrigger::check() {
	return (_node->getPosition().x > _p1.x &&
			_node->getPosition().x < _p2.x &&
			_node->getPosition().z > _p1.y &&
			_node->getPosition().z < _p2.y);
}

std::shared_ptr<Trigger> InZoneTrigger::clone() {
		return std::make_shared<InZoneTrigger>(*this);
}
