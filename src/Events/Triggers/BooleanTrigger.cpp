#include "BooleanTrigger.h"

BooleanTrigger::BooleanTrigger() {};

BooleanTrigger::BooleanTrigger(const BooleanTrigger & booleanTrigger) {}

BooleanTrigger::BooleanTrigger(const BooleanTrigger && booleanTrigger) {}

BooleanTrigger::~BooleanTrigger() {};

BooleanTrigger & BooleanTrigger::operator= (const BooleanTrigger & hasObject) {
	return *this;
}

bool BooleanTrigger::check() {
	return true;
}

std::shared_ptr<Trigger> BooleanTrigger::clone() {
		return std::make_shared<BooleanTrigger>(*this);
}
