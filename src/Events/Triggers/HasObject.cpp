#include "HasObject.h"

HasObject::HasObject() : HasObject("", nullptr){};

HasObject::HasObject(const std::string & objectName, std::shared_ptr<Inventory> inventory) : _objectName(objectName), _inventory(inventory) {}

HasObject::HasObject(const HasObject & hasObject) : _objectName(hasObject._objectName), _inventory(hasObject._inventory) {}

HasObject::HasObject(const HasObject && hasObject) : _objectName(hasObject._objectName), _inventory(hasObject._inventory) {}

HasObject::~HasObject() {};

HasObject & HasObject::operator= (const HasObject & hasObject) {
	return *this;
}

bool HasObject::check() {
	return _inventory->hasObject(_objectName);
}

std::shared_ptr<Trigger> HasObject::clone() {
		return std::make_shared<HasObject>(*this);
}
