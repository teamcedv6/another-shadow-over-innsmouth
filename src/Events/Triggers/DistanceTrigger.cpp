#include "DistanceTrigger.h"

DistanceTrigger::DistanceTrigger() : DistanceTrigger(nullptr, Ogre::Vector3(0,0,0), 10) {};

DistanceTrigger::DistanceTrigger(Ogre::SceneNode * node, const Ogre::Vector3 & point, float distance) : Trigger(),  _node(node), _point(point), _distance(distance) {};

DistanceTrigger::DistanceTrigger(const DistanceTrigger & distanceTrigger) : Trigger(distanceTrigger),  _node(distanceTrigger._node), _point(distanceTrigger._point), _distance(distanceTrigger._distance) {};

DistanceTrigger::DistanceTrigger(const DistanceTrigger && distanceTrigger) : Trigger(distanceTrigger), _node(distanceTrigger._node), _point(distanceTrigger._point), _distance(distanceTrigger._distance) {};

DistanceTrigger::~DistanceTrigger() {};

DistanceTrigger & DistanceTrigger::operator= (const DistanceTrigger & distanceTrigger) {
	DistanceTrigger dt(distanceTrigger._node, distanceTrigger._point, distanceTrigger._distance);
	return dt;
}

bool DistanceTrigger::check() {
	//std::cout << _node->getPosition() << " - " << _point << " = "  << (_node->getPosition() - _point).length() << " < " << _distance << "?\n";
	if((_node->getPosition() - _point).length() <= _distance) {
		std::cout << "Distance check \n";
	}
	return (_node->getPosition() - _point).length() <= _distance;
}

std::shared_ptr<Trigger> DistanceTrigger::clone() {
		return std::make_shared<DistanceTrigger>(*this);
}
