#include "CheckStoryValue.h"

CheckStoryValue::CheckStoryValue() : _valueName(""), _value(0){

}

CheckStoryValue::CheckStoryValue(const std::string & name, const int value, std::shared_ptr<GameLogic> gameLogic) : _valueName(name), _value(value), _gameLogic(gameLogic){

}

CheckStoryValue::CheckStoryValue(const CheckStoryValue & checkStoryValue) : _valueName(checkStoryValue._valueName), _value(checkStoryValue._value), _gameLogic(checkStoryValue._gameLogic) {

}

CheckStoryValue::CheckStoryValue(const CheckStoryValue && checkStoryValue) : _valueName(checkStoryValue._valueName), _value(checkStoryValue._value), _gameLogic(checkStoryValue._gameLogic) {

}

CheckStoryValue::~CheckStoryValue() {}

CheckStoryValue & CheckStoryValue::operator= (const CheckStoryValue & checkStoryValue) {
	return *this;
}


bool CheckStoryValue::check() {
	return _gameLogic->getValue(_valueName) == _value;
}

std::shared_ptr<Trigger> CheckStoryValue::clone() {
		return std::make_shared<CheckStoryValue>(*this);
}
