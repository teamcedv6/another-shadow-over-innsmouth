#include "Obj.h"

#include "AddNode.h"
#include "AttachNode.h"
#include "AddGraphicComponent.h"
#include "AddPhysicComponent.h"
#include "AddImpulse.h"

#include "Action.h"



Obj::Obj(){}

Obj::Obj(std::shared_ptr<Configuration> config, std::shared_ptr<NodeManager> nodes,
		std::shared_ptr<GraphicSystem> graphic, PhysicSystem* physic,
		std::shared_ptr<ActionsManager> actions, std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> entities,
		Obj::Options_t options) :
		_config(config),_nodes(nodes),_graphic(graphic),_physic(physic),_actions(actions),
		_entities(entities),_options(options) {

	switch (options.typeAct) {
		case Obj::NEW:
				_new();
			break;
		case Obj::DELETE:
			_delete();
			break;
		default:
			break;
	}
}

Obj::Obj(const Obj & obj) {
	_config = obj._config;
	_nodes = obj._nodes;
	_graphic = obj._graphic;
	_physic = obj._physic;
	_actions = obj._actions;
	_entities = obj._entities;
	_options = obj._options;
}

Obj::Obj(const Obj && obj) {
	_config = obj._config;
	_nodes = obj._nodes;
	_graphic = obj._graphic;
	_physic = obj._physic;
	_actions = obj._actions;
	_entities = obj._entities;
	_options = obj._options;
}

Obj::~Obj() {}

Obj & Obj::operator= (const Obj & obj) {
	_config = obj._config;
	_nodes = obj._nodes;
	_graphic = obj._graphic;
	_physic = obj._physic;
	_actions = obj._actions;
	_entities = obj._entities;
	_options = obj._options;

	return *this;
}

void Obj::_new() {

	int mapSize = 0;
	Ogre::Vector3 nodePosition;
	Ogre::Vector3 vector;
	std::shared_ptr<std::map<int, std::shared_ptr<Configuration::Item_t>>> objList;

	auto fov = _nodes->getNode("pov");
	Ogre::Quaternion direction = fov->getOrientation();
	Ogre::Vector3 poleposition =  direction * Ogre::Vector3(0.09, -2.33, 0.29);

	switch (_options.typeObj) {
		case Obj::CUSTOM:
				objList = _config->getCustomList();
				mapSize = (*objList.get()).size()-1;
				nodePosition = fov->getPosition() + poleposition;
				vector = direction * Ogre::Vector3(0, -2, 0);
			break;
		default:
			break;
	}


	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> randomKeyObj(0, mapSize);

	int nObj = randomKeyObj(gen);
	if(_options.typeObj == Obj::CUSTOM)
		nObj = 0;
	int generatedKey = _config->getKeyRandomName();

	int size = (*objList.get())[nObj]->name.size();
	std::ostringstream oss;
	oss << (*objList.get())[nObj]->name << generatedKey ;
	std::string name = oss.str();

	std::shared_ptr<Entity> entObj = std::make_shared<Entity>(name);
	(*_entities)[entObj->getName()] = entObj;

	std::shared_ptr<Action> createObj = std::make_shared<Action>();

	if(_options.addNode){
		std::shared_ptr<AddNode> addNode;
		addNode = std::make_shared<AddNode>(entObj,_nodes,nodePosition);
		createObj->addSubaction(addNode);
	}

	if(_options.attachNode){
		std::shared_ptr<AttachNode> attachNode;
		attachNode = std::make_shared<AttachNode>(entObj,_nodes,true);
		createObj->addSubaction(attachNode);
	}

	if(_options.addGraphic) {
		std::shared_ptr<AddGraphicComponent> addGraphicComponent;
		addGraphicComponent = std::make_shared<AddGraphicComponent>(entObj,_graphic,_nodes,size);
		createObj->addSubaction(addGraphicComponent);
	}

	if(_options.addPhysic) {
		std::shared_ptr<AddPhysicComponent> addPhysicComponent;
		PhysicComponent::PhysicProperties objProperties;
			objProperties.bodyFriction = (*objList.get())[nObj]->bodyFriction;
			objProperties.bodyRestitution = (*objList.get())[nObj]->bodyRestitution;
			objProperties.bodyMass = (*objList.get())[nObj]->bodyMass;
			objProperties.shapeId = (*objList.get())[nObj]->name;
		addPhysicComponent = std::make_shared<AddPhysicComponent>(entObj,_physic,PhysicSystem::MESH,
					PhysicSystem::DYNAMIC,objProperties);
		createObj->addSubaction(addPhysicComponent);
	}

	if(_options.addImpulse){
		std::shared_ptr<AddImpulse> addImpulse;
		addImpulse = std::make_shared<AddImpulse>(entObj,_physic,vector);
		createObj->addSubaction(addImpulse);
	}

	_actions->addAction(createObj);
}

void Obj::_delete(){

	auto id = (*_entities)[_options.entityName]->getId();

	if(_options.addPhysic) {
		_physic->removeComponent(id);
	}
	if(_options.addGraphic) {
		_graphic->removeComponent(id);
	}
	if(_options.addNode){
		_nodes->deleteNode(_nodes->getNode(_options.entityName),true);
	}
}
