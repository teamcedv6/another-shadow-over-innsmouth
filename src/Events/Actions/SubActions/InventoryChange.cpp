#include "InventoryChange.h"

InventoryChange::InventoryChange() {}

InventoryChange::InventoryChange(const std::string & name, std::shared_ptr<Inventory> inventory, int amount) : Subaction(), _name(name), _inventory(inventory), _amount(amount) {}

InventoryChange::InventoryChange(const InventoryChange & inventoryChange) : Subaction(inventoryChange), _name(inventoryChange._name), _inventory(inventoryChange._inventory), _amount(inventoryChange._amount) {

}

InventoryChange::InventoryChange(const InventoryChange && inventoryChange) : Subaction(inventoryChange), _name(inventoryChange._name), _inventory(inventoryChange._inventory), _amount(inventoryChange._amount) {

}

InventoryChange::~InventoryChange() {}

InventoryChange & InventoryChange::operator= (const InventoryChange & inventoryChange) {

	return *this;
}


Action::ActionResult InventoryChange::update(Ogre::Real deltaTime) {
	if(_amount > 0) {
		_inventory->pickObject(_name);
	}
	else {
		_inventory->dropObject(_name);
	}

	_hasFinished = true;
	return Action::ActionResult::ACTION_FINISHED;
}

std::shared_ptr<Subaction> InventoryChange::clone() {
	return std::make_shared<InventoryChange>(*this);
}

