#include "MovePoV.h"

MovePoV::MovePoV() : _yaw(0), _pitch(0), _roll(0) {}

MovePoV::MovePoV(std::shared_ptr<Entity> entity, std::shared_ptr<NodeManager> nodeManager, float yaw, float pitch, float roll) :
		_yaw(yaw), _pitch(pitch), _roll(roll),  _entity(entity), _nodes(nodeManager) {}

MovePoV::MovePoV(const MovePoV & movePoV) {
	_entity = movePoV._entity;
	_yaw = movePoV._yaw;
	_pitch = movePoV._pitch;
	_roll = movePoV._roll;
	_nodes = movePoV._nodes;
}

MovePoV::MovePoV(const MovePoV && movePoV) {
	_entity = movePoV._entity;
	_yaw = movePoV._yaw;
	_pitch = movePoV._pitch;
	_roll = movePoV._roll;
	_nodes = movePoV._nodes;
}

MovePoV::~MovePoV() {}

MovePoV & MovePoV::operator= (const MovePoV & movePoV) {
	_entity = movePoV._entity;
	_yaw = movePoV._yaw;
	_pitch = movePoV._pitch;
	_roll = movePoV._roll;
	_nodes = movePoV._nodes;

	return *this;
}


Action::ActionResult MovePoV::update(Ogre::Real deltaTime) {
	/***********************************************************
	 *  LEGACY CODE
	 ************************************************************/
	/*Ogre::SceneNode * node = _nodes->getNode(_entity->getName());
	Ogre::SceneNode * rifleNode = _nodes->getNode("PrecisionGun");

	// Como tiene el eje de yaw fijo, se hace yaw con referencia el mundo pero pitch con referencia local, de forma que se imita el comportamiento por defecto de la cámara (nunca se hace roll
	// por lo que se mantiene el horizonte recto.
	if(_yaw != 0){
		node->yaw(Ogre::Radian(_yaw * deltaTime), Ogre::Node::TS_WORLD);
	}
	if(_pitch != 0){
		node->pitch(Ogre::Radian(_pitch  * deltaTime), Ogre::Node::TS_LOCAL);
	}
	if(_roll != 0){
		node->roll(Ogre::Radian(_roll  * deltaTime), Ogre::Node::TS_WORLD);
	}

	// Poner la orientación del rifle igual que la cámara y de la mirilla igual que el rifle (los tres tienen el mismo centro), de forma que estos siempre sigan a la cámara
	rifleNode->setOrientation(node->getOrientation());*/

	_hasFinished = true;

	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> MovePoV::clone() {
	return std::make_shared<MovePoV>(*this);
}

