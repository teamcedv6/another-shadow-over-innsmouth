#include <Events/Actions/SubActions/StartConversation.h>

StartConversation::StartConversation() {}

StartConversation::StartConversation(const std::string & name, std::shared_ptr<ConversationSystem> conversationSystem) : Subaction(), _name(name), _conversationSystem(conversationSystem) {}

StartConversation::StartConversation(const StartConversation & startConversation) : Subaction(startConversation), _name(startConversation._name), _conversationSystem(startConversation._conversationSystem){

}

StartConversation::StartConversation(const StartConversation && startConversation) : Subaction(startConversation), _name(startConversation._name), _conversationSystem(startConversation._conversationSystem) {

}

StartConversation::~StartConversation() {}

StartConversation & StartConversation::operator= (const StartConversation & startConversation) {

	return *this;
}


Action::ActionResult StartConversation::update(Ogre::Real deltaTime) {
	_conversationSystem->loadConversation(_name);
	_conversationSystem->startConversation();

	_hasFinished = true;
	return Action::ActionResult::ACTION_FINISHED;
}

std::shared_ptr<Subaction> StartConversation::clone() {
	return std::make_shared<StartConversation>(*this);
}

