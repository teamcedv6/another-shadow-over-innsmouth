#include "AddGraphicComponent.h"

AddGraphicComponent::AddGraphicComponent(){}

AddGraphicComponent::AddGraphicComponent(std::shared_ptr<Entity> entity, std::shared_ptr<GraphicSystem> grSystem, std::shared_ptr<NodeManager> ndManager, int size) :
		_grSystem(grSystem), _entity(entity),_ndManager(ndManager), _size(size) {}

AddGraphicComponent::AddGraphicComponent(const AddGraphicComponent & addGraphicComponent) {
	_grSystem = addGraphicComponent._grSystem;
	_entity = addGraphicComponent._entity;
	_ndManager = addGraphicComponent._ndManager;
	_size = addGraphicComponent._size;
}

AddGraphicComponent::AddGraphicComponent(const AddGraphicComponent && addGraphicComponent) {
	_grSystem = addGraphicComponent._grSystem;
	_entity = addGraphicComponent._entity;
	_ndManager = addGraphicComponent._ndManager;
	_size = addGraphicComponent._size;

}

AddGraphicComponent::~AddGraphicComponent() {}

AddGraphicComponent & AddGraphicComponent::operator= (const AddGraphicComponent & addGraphicComponent) {
	_grSystem = addGraphicComponent._grSystem;
	_entity = addGraphicComponent._entity;
	_ndManager = addGraphicComponent._ndManager;
	_size = addGraphicComponent._size;

	return *this;
}

void AddGraphicComponent::_add() {

	// Gráficos
	std::shared_ptr<GraphicComponent> grComp = std::make_shared<GraphicComponent>();
	std::string name = _entity->getName();
	std::string auxName = _entity->getName().substr(0,_size);

	std::ostringstream oss;
	oss << auxName << ".mesh" ;
	const std::string mesh = oss.str();

	grComp->addGraphic(name,mesh,_ndManager->getNode(_entity->getName()),true);
	_grSystem->addComponent(_entity->getId(),grComp);
}

Action::ActionResult AddGraphicComponent::update(Ogre::Real deltaTime) {

	_add();

	_hasFinished = true;

	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> AddGraphicComponent::clone() {
	return std::make_shared<AddGraphicComponent>(*this);
}
