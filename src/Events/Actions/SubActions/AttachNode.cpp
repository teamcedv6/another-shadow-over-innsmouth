#include <AttachNode.h>

AttachNode::AttachNode(){}

AttachNode::AttachNode(std::shared_ptr<Entity> entity, std::shared_ptr<NodeManager> ndManager, bool visible) :
		_entity(entity), _ndManager(ndManager), _visible(visible) {}

AttachNode::AttachNode(const AttachNode & attachNode) {
	_ndManager = attachNode._ndManager;
	_entity = attachNode._entity;
	_visible = attachNode._visible;
}

AttachNode::AttachNode(const AttachNode && attachNode) {
	_ndManager = attachNode._ndManager;
	_entity = attachNode._entity;
	_visible = attachNode._visible;
}

AttachNode::~AttachNode() {}

AttachNode & AttachNode::operator= (const AttachNode & attachNode) {
	_ndManager = attachNode._ndManager;
	_entity = attachNode._entity;
	_visible = attachNode._visible;

	return *this;
}

void AttachNode::_attach() {

	auto node = _ndManager->getNode(_entity->getName());

	if(_visible) {
		_ndManager->attachNode(node, "root");
	} else {
		_ndManager->dettachNode(node);
	}
}

Action::ActionResult AttachNode::update(Ogre::Real deltaTime) {

	_attach();

	_hasFinished = true;

	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> AttachNode::clone() {
	return std::make_shared<AttachNode>(*this);
}
