#include "ChangeStoryValue.h"

ChangeStoryValue::ChangeStoryValue() {}

ChangeStoryValue::ChangeStoryValue(const std::string & name, int value, std::shared_ptr<GameLogic> gameLogic, bool relative) : Subaction(), _name(name), _value(value), _gameLogic(gameLogic), _relative(relative) {}

ChangeStoryValue::ChangeStoryValue(const ChangeStoryValue & changeStoryValue) : Subaction(changeStoryValue), _name(changeStoryValue._name), _value(changeStoryValue._value), _gameLogic(changeStoryValue._gameLogic), _relative(changeStoryValue._relative) {}

ChangeStoryValue::ChangeStoryValue(const ChangeStoryValue && changeStoryValue) : Subaction(changeStoryValue), _name(changeStoryValue._name), _value(changeStoryValue._value), _gameLogic(changeStoryValue._gameLogic), _relative(changeStoryValue._relative) {}

ChangeStoryValue::~ChangeStoryValue() {}

ChangeStoryValue & ChangeStoryValue::operator= (const ChangeStoryValue & changeStoryValue) {
	_name = changeStoryValue._name;
	_value = changeStoryValue._value;
	_relative = changeStoryValue._relative;
	return *this;
}


Action::ActionResult ChangeStoryValue::update(Ogre::Real deltaTime) {
	_gameLogic->setValue(_name, _value, _relative);

	_hasFinished = true;
	return Action::ActionResult::ACTION_FINISHED;
}

std::shared_ptr<Subaction> ChangeStoryValue::clone() {
	return std::make_shared<ChangeStoryValue>(*this);
}
