#include "CreateEvent.h"

CreateEvent::CreateEvent() {}

CreateEvent::CreateEvent(std::shared_ptr<Trigger> trigger, std::shared_ptr<Action> action, std::shared_ptr<ActionsManager> actionsManager) : Subaction(),
		_actionsManager(actionsManager), _action(action), _trigger(trigger) {}

CreateEvent::CreateEvent(const CreateEvent & createEvent) : _actionsManager(createEvent._actionsManager),
		_action(std::make_shared<Action>(*createEvent._action)), _trigger(createEvent._trigger->clone())  {

}

CreateEvent::CreateEvent(const CreateEvent && createEvent) : _actionsManager(createEvent._actionsManager),
		_action(createEvent._action), _trigger(createEvent._trigger)  {

}

CreateEvent::~CreateEvent() {}

CreateEvent & CreateEvent::operator= (const CreateEvent & createEvent) {
	_actionsManager = createEvent._actionsManager;
	_action = std::make_shared<Action>(*createEvent._action);
	_trigger = createEvent._trigger->clone();
	return *this;
}


Action::ActionResult CreateEvent::update(Ogre::Real deltaTime) {
	_actionsManager->addEvent(_trigger, _action);
	_hasFinished = true;
	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> CreateEvent::clone() {
	return std::make_shared<CreateEvent>(*this);
}

