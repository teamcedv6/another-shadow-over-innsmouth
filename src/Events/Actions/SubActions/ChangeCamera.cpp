#include "ChangeCamera.h"

ChangeCamera::ChangeCamera() {}

ChangeCamera::ChangeCamera(const std::string & name, std::shared_ptr<CameraSystem> cameraSystem, std::shared_ptr<IdNameMapper> mapper) : Subaction(), _name(name), _cameras(cameraSystem), _mapper(mapper) {}

ChangeCamera::ChangeCamera(const ChangeCamera & changeCamera) : Subaction(changeCamera), _name(changeCamera._name), _cameras(changeCamera._cameras), _mapper(changeCamera._mapper) {}

ChangeCamera::ChangeCamera(const ChangeCamera && changeCamera) : Subaction(changeCamera), _name(changeCamera._name), _cameras(changeCamera._cameras), _mapper(changeCamera._mapper) {}

ChangeCamera::~ChangeCamera() {}

ChangeCamera & ChangeCamera::operator= (const ChangeCamera & changeCamera) {

	return *this;
}

Action::ActionResult ChangeCamera::update(Ogre::Real deltaTime) {
	_cameras->setActiveCamera(_mapper->nameToId(_name));

	_hasFinished = true;
	return Action::ActionResult::ACTION_FINISHED;
}

std::shared_ptr<Subaction> ChangeCamera::clone() {
	return std::make_shared<ChangeCamera>(*this);
}
