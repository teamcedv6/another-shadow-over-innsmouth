#include "PlayFx.h"

PlayFx::PlayFx(){}

PlayFx::PlayFx(std::shared_ptr<SoundManager> soundManager, std::string fxKey) :
		_soundManager(soundManager), _fxKey(fxKey) {}

PlayFx::PlayFx(const PlayFx & playFx) {
	_soundManager = playFx._soundManager;
	_fxKey = playFx._fxKey;
}

PlayFx::PlayFx(const PlayFx && playFx) {
	_soundManager = playFx._soundManager;
	_fxKey = playFx._fxKey;
}

PlayFx::~PlayFx() {}

PlayFx & PlayFx::operator= (const PlayFx & playFx) {
	_soundManager = playFx._soundManager;
	_fxKey = playFx._fxKey;

	return *this;
}


Action::ActionResult PlayFx::update(Ogre::Real deltaTime) {

	_soundManager->selectResource(SoundManager::FX,_fxKey,SoundManager::PLAY,80,false);

	_hasFinished = true;

	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> PlayFx::clone() {
	return std::make_shared<PlayFx>(*this);
}
