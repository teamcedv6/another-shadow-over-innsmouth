#include "AddNode.h"

AddNode::AddNode(){}

AddNode::AddNode(std::shared_ptr<Entity> entity, std::shared_ptr<NodeManager> ndManager, Ogre::Vector3 position) :
		_entity(entity), _ndManager(ndManager), _position(position) {}

AddNode::AddNode(const AddNode & addNode) {
	_ndManager = addNode._ndManager;
	_entity = addNode._entity;
	_position = addNode._position;
}

AddNode::AddNode(const AddNode && addNode) {
	_ndManager = addNode._ndManager;
	_entity = addNode._entity;
	_position = addNode._position;
}

AddNode::~AddNode() {}

AddNode & AddNode::operator= (const AddNode & addNode) {
	_ndManager = addNode._ndManager;
	_entity = addNode._entity;
	_position = addNode._position;

	return *this;
}

void AddNode::_add() {
	auto name = _entity->getName();
	auto node = _ndManager->createNode(name);
	node->setPosition(_position);
}

Action::ActionResult AddNode::update(Ogre::Real deltaTime) {

	_add();

	_hasFinished = true;

	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> AddNode::clone() {
	return std::make_shared<AddNode>(*this);
}
