#include "FunctorRunner.h"

FunctorRunner::FunctorRunner() {}

FunctorRunner::FunctorRunner(std::function<void(void)> f) : Subaction(), _f(f) {}

FunctorRunner::FunctorRunner(const FunctorRunner & movePoV) {

}

FunctorRunner::FunctorRunner(const FunctorRunner && movePoV) {

}

FunctorRunner::~FunctorRunner() {}

FunctorRunner & FunctorRunner::operator= (const FunctorRunner & movePoV) {

	return *this;
}


Action::ActionResult FunctorRunner::update(Ogre::Real deltaTime) {
	_f();
	_hasFinished = true;
	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> FunctorRunner::clone() {
	return std::make_shared<FunctorRunner>(*this);
}

