#include "CameraExchangeLoop.h"
#include "EventFactory.h"

CameraExchangeLoop::CameraExchangeLoop() {}

CameraExchangeLoop::CameraExchangeLoop(Ogre::SceneNode * node, const Ogre::Vector2 & zone1_1, const Ogre::Vector2 & zone1_2,
		const Ogre::Vector2 & zone2_1, const Ogre::Vector2 & zone2_2,
		const std::string & camera1, const std::string & camera2,
		std::shared_ptr<EventFactory> factory,std::shared_ptr<ActionsManager> actionManager) : Subaction(),
				_actions(actionManager), _factory(factory) {

	EntityExtraLoader::EventPart triggerConf;
	triggerConf.name = "inZone";
	EntityExtraLoader::Param p1;
	p1.type = "float";
	p1.paramValue.floatValue = zone1_1.x;
	EntityExtraLoader::Param p2;
	p2.type = "float";
	p2.paramValue.floatValue = zone1_1.y;
	EntityExtraLoader::Param p3;
	p3.type = "float";
	p3.paramValue.floatValue = zone1_2.x;
	EntityExtraLoader::Param p4;
	p4.type = "float";
	p4.paramValue.floatValue = zone1_2.y;
	EntityExtraLoader::Param p5;
	p5.type = "float";
	p5.paramValue.floatValue = zone2_1.x;
	EntityExtraLoader::Param p6;
	p6.type = "float";
	p6.paramValue.floatValue = zone2_1.y;
	EntityExtraLoader::Param p7;
	p7.type = "float";
	p7.paramValue.floatValue = zone2_2.x;
	EntityExtraLoader::Param p8;
	p8.type = "float";
	p8.paramValue.floatValue = zone2_2.y;

	triggerConf.params.push_back(p1);
	triggerConf.params.push_back(p2);
	triggerConf.params.push_back(p3);
	triggerConf.params.push_back(p4);

	_trigger = factory->createTrigger(triggerConf);

	_action = std::make_shared<Action>();

	EntityExtraLoader::EventPart subAcCam;
	subAcCam.name = "changeCamera";
	EntityExtraLoader::Param c1;
	c1.type = "str";
	c1.paramValue.strValue = camera1;

	subAcCam.params.push_back(c1);

	EntityExtraLoader::Param c2;
	c2.type = "str";
	c2.paramValue.strValue = camera2;

	_cameraExchangeConf.name = "cameraExchangeLoop";
	_cameraExchangeConf.params.push_back(p5);
	_cameraExchangeConf.params.push_back(p6);
	_cameraExchangeConf.params.push_back(p7);
	_cameraExchangeConf.params.push_back(p8);
	_cameraExchangeConf.params.push_back(c2);
	_cameraExchangeConf.params.push_back(p1);
	_cameraExchangeConf.params.push_back(p2);
	_cameraExchangeConf.params.push_back(p3);
	_cameraExchangeConf.params.push_back(p4);
	_cameraExchangeConf.params.push_back(c1);

	_action->addSubaction(factory->createSubaction(subAcCam));

}

CameraExchangeLoop::CameraExchangeLoop(const CameraExchangeLoop & cameraExchangeLoop) : Subaction(cameraExchangeLoop), _actions(cameraExchangeLoop._actions),
		_action(std::make_shared<Action>(*cameraExchangeLoop._action)), _trigger(cameraExchangeLoop._trigger->clone()), _factory(cameraExchangeLoop._factory),
		_cameraExchangeConf(cameraExchangeLoop._cameraExchangeConf){}

CameraExchangeLoop::CameraExchangeLoop(const CameraExchangeLoop && cameraExchangeLoop) : Subaction(cameraExchangeLoop), _actions(cameraExchangeLoop._actions),
		_action(cameraExchangeLoop._action), _trigger(cameraExchangeLoop._trigger), _factory(cameraExchangeLoop._factory),
		_cameraExchangeConf(cameraExchangeLoop._cameraExchangeConf){}

CameraExchangeLoop::~CameraExchangeLoop() {}

CameraExchangeLoop & CameraExchangeLoop::operator= (const CameraExchangeLoop & changeCamera) {

	return *this;
}

Action::ActionResult CameraExchangeLoop::update(Ogre::Real deltaTime) {
	_action->addSubaction(_factory->createSubaction(_cameraExchangeConf));
	_actions->addEvent(_trigger, _action);

	_hasFinished = true;
	return Action::ActionResult::ACTION_FINISHED;
}

std::shared_ptr<Subaction> CameraExchangeLoop::clone() {
	return std::make_shared<CameraExchangeLoop>(*this);
}
