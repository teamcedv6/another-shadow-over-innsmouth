#include "ChangeDaytime.h"

ChangeDaytime::ChangeDaytime() {}

ChangeDaytime::ChangeDaytime(int n, PlayState * playState) : Subaction(), _n(n), _playState(playState){}


ChangeDaytime::ChangeDaytime(const ChangeDaytime & changeDaytime) : Subaction(changeDaytime), _n(changeDaytime._n), _playState(changeDaytime._playState) {

}

ChangeDaytime::ChangeDaytime(const ChangeDaytime && changeDaytime) : Subaction(changeDaytime), _n(changeDaytime._n), _playState(changeDaytime._playState) {

}

ChangeDaytime::~ChangeDaytime() {}

ChangeDaytime & ChangeDaytime::operator= (const ChangeDaytime & changeDaytime) {

	return *this;
}


Action::ActionResult ChangeDaytime::update(Ogre::Real deltaTime) {
	_playState->setDaytime(_n);
	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> ChangeDaytime::clone() {
	return std::make_shared<ChangeDaytime>(*this);
}

