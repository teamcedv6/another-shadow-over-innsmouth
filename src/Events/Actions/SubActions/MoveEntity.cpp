#include "MoveEntity.h"

MoveEntity::MoveEntity() : _newPosition(0,0,0) {}

MoveEntity::MoveEntity(std::shared_ptr<Entity> entity,std::shared_ptr<PhysicSystem> phSystem, Ogre::Vector3 newPosition) :
		_phSystem(phSystem), _newPosition(newPosition),  _entity(entity) {}

MoveEntity::MoveEntity(const MoveEntity & moveEntity) {
	_entity = moveEntity._entity;
	_newPosition = moveEntity._newPosition;
	_phSystem = moveEntity._phSystem;
}

MoveEntity::MoveEntity(const MoveEntity && moveEntity) {
	_entity = moveEntity._entity;
		_newPosition = moveEntity._newPosition;
		_phSystem = moveEntity._phSystem;
}

MoveEntity::~MoveEntity() {}

MoveEntity & MoveEntity::operator= (const MoveEntity & moveEntity) {
	_entity = moveEntity._entity;
	_newPosition = moveEntity._newPosition;
	_phSystem = moveEntity._phSystem;

	return *this;
}


Action::ActionResult MoveEntity::update(Ogre::Real deltaTime) {
	_phSystem->updateObject(_entity->getId(),_newPosition);

	_hasFinished = true;

	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> MoveEntity::clone() {
	return std::make_shared<MoveEntity>(*this);
}
