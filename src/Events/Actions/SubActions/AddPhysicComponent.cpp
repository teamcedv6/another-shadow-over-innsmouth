#include "AddPhysicComponent.h"

AddPhysicComponent::AddPhysicComponent(){}

AddPhysicComponent::AddPhysicComponent(std::shared_ptr<Entity> entity, PhysicSystem* phSystem, PhysicSystem::ShapeForm shapeForm, PhysicSystem::ShapeType shapeType, PhysicComponent::PhysicProperties objProperties) :
		_phSystem(phSystem), _entity(entity),_shapeForm(shapeForm),_shapeType(shapeType), _objProperties(objProperties) {}

AddPhysicComponent::AddPhysicComponent(const AddPhysicComponent & addPhysicComponent) {
	_phSystem = addPhysicComponent._phSystem;
	_entity = addPhysicComponent._entity;
	_shapeForm = addPhysicComponent._shapeForm;
	_shapeType = addPhysicComponent._shapeType;
	_objProperties = addPhysicComponent._objProperties;
}

AddPhysicComponent::AddPhysicComponent(const AddPhysicComponent && addPhysicComponent) {
	_phSystem = addPhysicComponent._phSystem;
	_entity = addPhysicComponent._entity;
	_shapeForm = addPhysicComponent._shapeForm;
	_shapeType = addPhysicComponent._shapeType;
	_objProperties = addPhysicComponent._objProperties;
}

AddPhysicComponent::~AddPhysicComponent() {}

AddPhysicComponent & AddPhysicComponent::operator= (const AddPhysicComponent & addPhysicComponent) {
	_phSystem = addPhysicComponent._phSystem;
	_entity = addPhysicComponent._entity;
	_shapeForm = addPhysicComponent._shapeForm;
	_shapeType = addPhysicComponent._shapeType;
	_objProperties = addPhysicComponent._objProperties;

	return *this;
}

void AddPhysicComponent::_add() {
	std::shared_ptr<PhysicComponent> phComp = std::make_shared<PhysicComponent>();
	_phSystem->addComponent(_entity->getId(),phComp);
	_phSystem->addPhysicObject(_entity->getId(),_shapeForm,_shapeType,_objProperties);
}

Action::ActionResult AddPhysicComponent::update(Ogre::Real deltaTime) {

	_add();

	_hasFinished = true;

	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> AddPhysicComponent::clone() {
	return std::make_shared<AddPhysicComponent>(*this);
}
