#include "DeleteEntity.h"

DeleteEntity::DeleteEntity() : _id(-1) {}

DeleteEntity::DeleteEntity(std::string name, std::shared_ptr<GameManager> gameManager) : Subaction(), _id(gameManager->getMapper()->nameToId(name)), _gameManager(gameManager) {}

DeleteEntity::DeleteEntity(Entity::ID id, std::shared_ptr<GameManager> gameManager): Subaction(), _id(id), _gameManager(gameManager){

}

DeleteEntity::DeleteEntity(const DeleteEntity & deleteEntity) : Subaction(deleteEntity), _id(deleteEntity._id), _gameManager(deleteEntity._gameManager){

}

DeleteEntity::DeleteEntity(const DeleteEntity && deleteEntity) : Subaction(deleteEntity), _id(deleteEntity._id), _gameManager(deleteEntity._gameManager) {

}

DeleteEntity::~DeleteEntity() {}

DeleteEntity & DeleteEntity::operator= (const DeleteEntity & deleteEntity) {

	return *this;
}


Action::ActionResult DeleteEntity::update(Ogre::Real deltaTime) {
	std::string name = _gameManager->getMapper()->idToName(_id);
	_gameManager->getAISystem()->removeComponent(_id);
	_gameManager->getAnimationSystem()->removeComponent(_id);
	_gameManager->getCameraSystem()->removeComponent(_id);
	_gameManager->getConversationSystem()->removeComponent(_id);
	_gameManager->getGraphicsSystem()->removeComponent(_id);
	_gameManager->getInteractionSystem()->removeComponent(_id);
	_gameManager->getMovementSystem()->removeComponent(_id);
	_gameManager->getPhysicsSystem()->removeComponent(_id);
	_gameManager->getUserInterfaceSystem()->removeComponent(_id);
	_gameManager->getNodeManager()->deleteNode(_gameManager->getNodeManager()->getNode(name), true);
	_gameManager->deleteEntity(name);
	_hasFinished = true;
	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> DeleteEntity::clone() {
	return std::make_shared<DeleteEntity>(*this);
}

