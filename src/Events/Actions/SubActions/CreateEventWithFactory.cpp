#include "CreateEventWithFactory.h"

CreateEventWithFactory::CreateEventWithFactory() {}

CreateEventWithFactory::CreateEventWithFactory(EntityExtraLoader::EventPart & triggerConf, EntityExtraLoader::EventPart & subactionConf, std::shared_ptr<ActionsManager> actionsManager, EventFactory * factory) : Subaction(),
		_actionsManager(actionsManager), _triggerConf(triggerConf), _subactionConf(subactionConf), _factory(factory) {}

CreateEventWithFactory::CreateEventWithFactory(const CreateEventWithFactory & createEventWithFactory) : _actionsManager(createEventWithFactory._actionsManager),
		_triggerConf(createEventWithFactory._triggerConf), _subactionConf(createEventWithFactory._subactionConf),  _factory(createEventWithFactory._factory) {

}

CreateEventWithFactory::CreateEventWithFactory(const CreateEventWithFactory && createEventWithFactory) :  _actionsManager(createEventWithFactory._actionsManager),
		_triggerConf(createEventWithFactory._triggerConf), _subactionConf(createEventWithFactory._subactionConf),  _factory(createEventWithFactory._factory)  {

}

CreateEventWithFactory::~CreateEventWithFactory() {}

CreateEventWithFactory & CreateEventWithFactory::operator= (const CreateEventWithFactory & createEventWithFactory) {
	_actionsManager = createEventWithFactory._actionsManager;
	_triggerConf = createEventWithFactory._triggerConf;
	_subactionConf = createEventWithFactory._subactionConf;
	_factory = createEventWithFactory._factory;

	return *this;
}


Action::ActionResult CreateEventWithFactory::update(Ogre::Real deltaTime) {
	std::shared_ptr<Trigger> trigger = _factory->createTrigger(_triggerConf);
	std::shared_ptr<Action> action = std::make_shared<Action>();
	std::shared_ptr<Subaction> subaction = _factory->createSubaction(_subactionConf);
	action->addSubaction(subaction);

	_actionsManager->addEvent(trigger, action);

	_hasFinished = true;
	return Action::ACTION_FINISHED;
}

std::shared_ptr<Subaction> CreateEventWithFactory::clone() {
	return std::make_shared<CreateEventWithFactory>(*this);
}

