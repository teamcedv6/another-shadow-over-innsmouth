#include "Action.h"
#include "Subaction.h"

Action::Action(){}

Action::Action(const Action & action) {
	for(auto it = action._subactions.begin(); it != action._subactions.end(); it++) {
		subactionVector_ptr sVector = std::make_shared<std::vector<std::shared_ptr<Subaction>>>();

		for(auto it2 = (*it)->begin(); it2 != (*it)->end(); it2++) {
			std::shared_ptr<Subaction> subaction = (*it2)->clone();
			sVector->push_back(subaction);
		}

		_subactions.push_back(sVector);
		_running.push_back(sVector->begin());
	}
}

Action::Action(const Action && action){}

Action::~Action(){}

Action & Action::operator= (const Action & action){ return *this;}


void Action::addSubaction(std::shared_ptr<Subaction> subaction) {
	subactionVector_ptr newSequence = std::make_shared<std::vector<std::shared_ptr<Subaction>>>();
	newSequence->push_back(subaction);
	_running.push_back(newSequence->begin());
	_subactions.push_back(newSequence);
}

void Action::addSubactionSequence(subactionVector_ptr sequence) {
	_running.push_back(sequence->begin());
	_subactions.push_back(sequence);
}


Action::ActionResult Action::update(Ogre::Real deltaTime){
	std::vector<int> finished;
	Action::ActionResult result;

	finished.reserve(_subactions.size());
	int i = 0;

	for(auto it = _running.begin(); it != _running.end(); it++, i++) {

		result = (*(*it))->update(deltaTime);

		if(result == Action::ACTION_FINISHED) {
			(*it)++;

			if((*it) == _subactions.at(i)->end()) {
				finished.push_back(i);
			}
			else {
				result = Action::CONTINUE;
			}
		}
		else if(result == Action::PLAYER_WIN || result == Action::PLAYER_LOST) {
			return result;
		}
	}

	for(auto it = finished.rbegin(); it != finished.rend();  it++) {
		_subactions.erase(_subactions.begin() + *(it));
		_running.erase(_running.begin() + *(it));
	}

	if(_subactions.size() == 0) result = Action::ACTION_FINISHED;

	return result;

}

