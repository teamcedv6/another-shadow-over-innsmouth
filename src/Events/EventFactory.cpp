#include "EventFactory.h"
#include "GameManager.h"
#include "CheckStoryValue.h"

EventFactory::EventFactory() {}

EventFactory::EventFactory(std::shared_ptr<GameManager> gameManager) : _gameManager(gameManager) {
	_initializeBuilders();
}

EventFactory::EventFactory(const EventFactory & eventFactory) {}

EventFactory::EventFactory(const EventFactory && eventFactory) {}

EventFactory::~EventFactory() {}

EventFactory & EventFactory::operator= (const EventFactory & eventFactory) {
	return *this;
}

// Subactions
std::shared_ptr<Subaction> EventFactory::createSubaction(EntityExtraLoader::EventPart & subactionConf) {
	std::cout << "Create subaction.." << subactionConf.name << "\n";
	return _subactionBuilders[subactionConf.name](subactionConf);
}

std::shared_ptr<Trigger> EventFactory::createTrigger(EntityExtraLoader::EventPart & triggerConf) {
	return _triggerBuilders[triggerConf.name](triggerConf);
}

void EventFactory::_initializeBuilders() {
	// Subactions
	_subactionBuilders["pickObject"] = [this](EntityExtraLoader::EventPart & subactionConf)-> std::shared_ptr<Subaction> { return std::make_shared<InventoryChange>(subactionConf.params[0].paramValue.strValue, _gameManager->getInventory(), 1); };
	_subactionBuilders["dropObject"] = [this](EntityExtraLoader::EventPart & subactionConf)-> std::shared_ptr<Subaction> { return std::make_shared<InventoryChange>(subactionConf.params[0].paramValue.strValue, _gameManager->getInventory(), -1); };
	_subactionBuilders["changeCamera"] = [this](EntityExtraLoader::EventPart & subactionConf)-> std::shared_ptr<Subaction> { return std::make_shared<ChangeCamera>(subactionConf.params[0].paramValue.strValue, _gameManager->getCameraSystem(), _gameManager->getMapper() ); };
	_subactionBuilders["changeStoryValue"] = [this](EntityExtraLoader::EventPart & subactionConf) -> std::shared_ptr<Subaction>  { return std::make_shared<ChangeStoryValue>(subactionConf.params[0].paramValue.strValue, subactionConf.params[1].paramValue.intValue, _gameManager->getGameLogic(), false); };
	_subactionBuilders["startConversation"] = [this](EntityExtraLoader::EventPart & subactionConf) -> std::shared_ptr<Subaction>  { return std::make_shared<StartConversation>(subactionConf.params[0].paramValue.strValue, _gameManager->getConversationSystem()); };
	_subactionBuilders["playFx"] = [this](EntityExtraLoader::EventPart & subactionConf) -> std::shared_ptr<Subaction> { return std::make_shared<PlayFx>(_gameManager->getSoundManager(), subactionConf.params[0].paramValue.strValue); };
	_subactionBuilders["createEvent"] = [this](EntityExtraLoader::EventPart & subactionConf) -> std::shared_ptr<Subaction> {
		std::string triggerName = subactionConf.params[0].paramValue.strValue;
		int paramNumber = subactionConf.params[1].paramValue.intValue;
		EntityExtraLoader::EventPart triggerConf;
		triggerConf.name = triggerName;

		int i;
		int iterations = 2 + paramNumber;

		for(i = 2; i < iterations; i++) {
			triggerConf.params.push_back(subactionConf.params[i]);
		}

		std::shared_ptr<Trigger> trigger = createTrigger(triggerConf);

		i++;
		int subactionNumber = subactionConf.params[i].paramValue.intValue;
		std::shared_ptr<Action> action = std::make_shared<Action>();

		iterations = i + subactionNumber;

		for(; i < iterations; i++) {
			EntityExtraLoader::EventPart newSubactionConf;
			newSubactionConf.name = subactionConf.params[i].paramValue.strValue;
			i++;
			int subactionIterations = subactionConf.params[i].paramValue.intValue + i;
			iterations += subactionConf.params[i].paramValue.intValue;

			for(; i < subactionIterations; i++) {
				newSubactionConf.params.push_back(subactionConf.params[i]);
			}

			action->addSubaction(createSubaction(newSubactionConf));
		}

		return std::make_shared<CreateEvent>(trigger, action, _gameManager->getActionsManager());
	};
	//_subactionBuilders["createEventWithFactory"] = [this](EntityExtraLoader::EventPart & subactionConf) -> std::shared_ptr<Subaction> { return std::make_shared<CreateEventWithFactory>(_gameManager->getSoundManager(), subactionConf.params[0].paramValue.strValue); };
	_subactionBuilders["cameraExchangeLoop"] = [this](EntityExtraLoader::EventPart & subactionConf) -> std::shared_ptr<Subaction> {

		auto player = _gameManager->getConfig()->getAiConfig()->player;
		Ogre::SceneNode * node = _gameManager->getNodeManager()->getNode(player);
		const Ogre::Vector2 & p1 = Ogre::Vector2(subactionConf.params[0].paramValue.floatValue, subactionConf.params[1].paramValue.floatValue);
		const Ogre::Vector2 & p2 = Ogre::Vector2(subactionConf.params[2].paramValue.floatValue, subactionConf.params[3].paramValue.floatValue);
		std::string camera1 = subactionConf.params[4].paramValue.strValue;
		const Ogre::Vector2 & p3 = Ogre::Vector2(subactionConf.params[5].paramValue.floatValue, subactionConf.params[6].paramValue.floatValue);
		const Ogre::Vector2 & p4 = Ogre::Vector2(subactionConf.params[7].paramValue.floatValue, subactionConf.params[8].paramValue.floatValue);
		std::string camera2 = subactionConf.params[9].paramValue.strValue;

		return std::make_shared<CameraExchangeLoop>(node, p1, p2, p3, p4, camera1, camera2,_gameManager->getEventFactory(), _gameManager->getActionsManager());
	};
	_subactionBuilders["changeDaytime"] = [this](EntityExtraLoader::EventPart & subactionConf) -> std::shared_ptr<Subaction> { return std::make_shared<ChangeDaytime>(subactionConf.params[0].paramValue.intValue, (PlayState*)_gameManager->getState(GameState::PLAY).get()); };

	// Triggers
	_triggerBuilders["mainCharacterdistanceFrom"] = [this](EntityExtraLoader::EventPart & triggerConf) -> std::shared_ptr<Trigger> {
		auto player = _gameManager->getConfig()->getAiConfig()->player;
		Ogre::SceneNode * node = _gameManager->getNodeManager()->getNode(player);
		const Ogre::Vector3 & p = _gameManager->getNodeManager()->getNode(triggerConf.params[0].	paramValue.strValue)->getPosition();
		return std::make_shared<DistanceTrigger>(node, p, triggerConf.params[1].paramValue.floatValue);
	};
	_triggerBuilders["true"] = [this](EntityExtraLoader::EventPart & triggerConf) -> std::shared_ptr<Trigger> { return std::make_shared<BooleanTrigger>();};
	_triggerBuilders["hasObject"] = [this](EntityExtraLoader::EventPart & triggerConf) -> std::shared_ptr<Trigger> {return std::make_shared<HasObject>(triggerConf.params[0].paramValue.strValue, _gameManager->getInventory());};
	_triggerBuilders["checkStoryValue"] = [this](EntityExtraLoader::EventPart & triggerConf) -> std::shared_ptr<Trigger> {return std::make_shared<CheckStoryValue>(triggerConf.params[0].paramValue.strValue, triggerConf.params[1].paramValue.intValue, _gameManager->getGameLogic());};
	_triggerBuilders["inZone"] = [this](EntityExtraLoader::EventPart & triggerConf) -> std::shared_ptr<Trigger> {
		auto player = _gameManager->getConfig()->getAiConfig()->player;
		Ogre::SceneNode * node = _gameManager->getNodeManager()->getNode(player);
		const Ogre::Vector2 & p1 = Ogre::Vector2(triggerConf.params[0].paramValue.floatValue, triggerConf.params[1].paramValue.floatValue);
		const Ogre::Vector2 & p2 = Ogre::Vector2(triggerConf.params[2].paramValue.floatValue, triggerConf.params[3].paramValue.floatValue);
		return std::make_shared<InZoneTrigger>(node, p1, p2);
	};

}


