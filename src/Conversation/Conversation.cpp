#include "Conversation.h"

Conversation::Conversation() : _hijackInput(false), _nodes(std::make_shared<std::vector<std::shared_ptr<ConversationNode>>>()) { std::cout << "E CONST\n"; _current = _nodes->begin(); }

Conversation::Conversation(bool isHijacking) : _hijackInput(isHijacking), _nodes(std::make_shared<std::vector<std::shared_ptr<ConversationNode>>>()) {std::cout << "CONST\n"; _current = _nodes->begin(); }

Conversation::~Conversation (){ std::cout << "DEST\n"; }

Conversation::Conversation(const Conversation & conversation) :  _current(conversation._current), _hijackInput(conversation._hijackInput) {
	std::cout << "COPY\n";
	// Can't clone nodes here because a shared pointer is needed in order to do it
	_nodes = std::make_shared<std::vector<std::shared_ptr<ConversationNode>>>();

	_current = _nodes->begin();// + (conversation._current - conversation._nodes->begin());
}

Conversation::Conversation(const Conversation && conversation) : _nodes(conversation._nodes), _current(conversation._current), _hijackInput(conversation._hijackInput) {
	std::cout << "MOVE\n";
}

Conversation& Conversation::operator = (const Conversation &conversation){
	std::cout << "= OP\n";
	return *this;
}

Conversation & Conversation::operator= (const Conversation && conversation) {
	std::cout << "= MOVE OP\n";
	_nodes = conversation._nodes;
	_current = conversation._current;
	_hijackInput = conversation._hijackInput;
}

void Conversation::addNode(std::shared_ptr<ConversationNode> node) {
	int index = 0;
	if(_current != _nodes->end()) index = _current - _nodes->begin();
	_nodes->push_back(node);
	if(_current != _nodes->end()) _current = _nodes->begin() + index;
	std::cout << "conv size "<<_nodes->size() << "\n";
}

void Conversation::initConversation() {
	_current = _nodes->begin();
}

bool Conversation::nextNode() {
	_current++;

	std::cout << "conv size "<<_nodes->size() << "\n";

	if (_current == _nodes->end()) {
		return false;
	}
	else {
		return true;
	}
}

std::shared_ptr<ConversationNode> Conversation::getNode() {
	return *_current;
}

bool Conversation::hasInputHijacked() {
	return _hijackInput;
}

std::shared_ptr<Conversation> Conversation::clone() {
	auto conversation = std::make_shared<Conversation>(*this);

	for(auto it = _nodes->begin(); it != _nodes->end(); it++) {
		conversation->_nodes->push_back((*it)->clone(conversation));
	}

	return conversation;
}

void Conversation::clear() {
	for(auto it = _nodes->begin(); it != _nodes->end(); it++) {
		(*it)->clear();
	}
}
