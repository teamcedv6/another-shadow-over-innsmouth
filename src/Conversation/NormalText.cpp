#include "NormalText.h"

NormalText::NormalText() : ConversationNode(NodeType::NODE_TEXT), _text(""), _name(""), _picture("") {}

NormalText::NormalText(std::string name, std::string text, std::string picture) : ConversationNode(NodeType::NODE_TEXT), _text(text),
		_name(name), _picture(picture){/*std::cout << "TTT const\n";*/}

NormalText::~NormalText (){/*std::cout << "TTT dest\n";*/}

NormalText::NormalText(const NormalText & normalText) : ConversationNode(normalText), _text(normalText._text), _name(normalText._name), _picture(normalText._picture) {
	//std::cout << "TTT cop\n";
}

NormalText::NormalText(const NormalText && normalText) : ConversationNode(normalText),  _text(normalText._text), _name(normalText._name), _picture(normalText._picture) {
	//std::cout << "TTT mov\n";
}

NormalText& NormalText::operator= (const NormalText &normalText){
	//std::cout << "TTT operator=\n";
	NormalText tmp(normalText._name, normalText._text, normalText._picture);
	return tmp;
}

bool NormalText::execute() {

	Noesis::TextBlock * textZone;
	textZone = (Noesis::TextBlock *) _textRoot->FindName("text");

	Noesis::TextBlock * nameZone;
	nameZone = (Noesis::TextBlock *) _textRoot->FindName("name");

	textZone->SetText(_text.c_str());
	nameZone->SetText(_name.c_str());

	Noesis::Image * profile;
	profile = (Noesis::Image *) _textRoot->FindName("profile");

	std::stringstream ss;
	ss << "Assets/" << _picture;
	Noesis::ImageSource * source = new Noesis::TextureSource(ss.str().c_str());
	profile->SetSource(source);

	return true;
}

bool NormalText::up() {
	return true;
}

bool NormalText::down() {
	return true;
}

bool NormalText::push() {
	return false;
}

bool NormalText::back() {
	return true;
}

std::shared_ptr<ConversationNode> NormalText::clone(std::shared_ptr<Conversation> c) {
	return std::make_shared<NormalText>(*this);
}

void NormalText::clear() {}
