#include "ThoughtNode.h"

ThoughtNode::ThoughtNode() : ConversationNode(NodeType::NODE_THOUGHT), _text("") {}

ThoughtNode::ThoughtNode(std::string text) : ConversationNode(NodeType::NODE_THOUGHT), _text(text){}

ThoughtNode::~ThoughtNode (){}

ThoughtNode::ThoughtNode(const ThoughtNode & ThoughtNode) : ConversationNode(ThoughtNode), _text(ThoughtNode._text) {

}

ThoughtNode::ThoughtNode(const ThoughtNode && ThoughtNode) : ConversationNode(ThoughtNode),  _text(ThoughtNode._text) {

}

ThoughtNode& ThoughtNode::operator= (const ThoughtNode &ThoughtNode){
	return *this;
}

bool ThoughtNode::execute() {
	std::cout << _text << "\n";
	Noesis::TextBlock * textZone;
	textZone = (Noesis::TextBlock *) _textRoot->FindName("text");

	textZone->SetText(_text.c_str());

	return true;
}

bool ThoughtNode::up() {
	return true;
}

bool ThoughtNode::down() {
	return true;
}

bool ThoughtNode::push() {
	return false;
}

bool ThoughtNode::back() {
	return true;
}

std::shared_ptr<ConversationNode> ThoughtNode::clone(std::shared_ptr<Conversation> c) {
	return std::make_shared<ThoughtNode>(*this);
}

void ThoughtNode::clear() {}
