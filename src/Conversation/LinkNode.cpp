#include "LinkNode.h"

LinkNode::LinkNode() : ConversationNode(NodeType::NODE_LINK), _conversation(nullptr), _linkName(""), _linkNodes(nullptr) {}

LinkNode::LinkNode(std::string linkName, std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> linkNodes, std::shared_ptr<Conversation> conversation) :
		ConversationNode(NodeType::NODE_LINK), _linkNodes(linkNodes), _linkName(linkName), _conversation(conversation) {}

LinkNode::~LinkNode (){}

LinkNode::LinkNode(const LinkNode & linkNode) : ConversationNode(linkNode) {
	_linkName = linkNode._linkName;
	_conversation = linkNode._conversation;

	if(linkNode._linkNodes == nullptr) {
		_linkNodes = linkNode._linkNodes;
	}
	else {
		_linkNodes = std::make_shared<std::vector<std::shared_ptr<ConversationNode>>>();

		for(auto it = linkNode._linkNodes->begin(); it != linkNode._linkNodes->end(); it++) {
			_linkNodes->push_back((*it)->clone(_conversation));
		}
	}
}

LinkNode::LinkNode(const LinkNode && linkNode) : ConversationNode(linkNode){
	_linkName = linkNode._linkName;
	_conversation = linkNode._conversation;
	_linkNodes = linkNode._linkNodes;
}

LinkNode& LinkNode::operator = (const LinkNode &linkNode){
	_linkName = linkNode._linkName;
	_conversation = linkNode._conversation;
	_linkNodes = linkNode._linkNodes;
	return *this;
}

bool LinkNode::execute() {

	if(_linkNodes == nullptr) {
		// Load has been delayed, load nodes now
	}

	for(auto it = _linkNodes->begin(); it < _linkNodes->end(); it++) {
		_conversation->addNode(*it);
	}

	return false;
}

bool LinkNode::up() {
	// Nothing to do
	return true;
}

bool LinkNode::down() {
	// Nothing to do
	return true;
}

bool LinkNode::push() {
	// Nothing to do
	return true;
}

bool LinkNode::back() {
	// Nothing to do
	return true;
}

std::shared_ptr<ConversationNode> LinkNode::clone(std::shared_ptr<Conversation> c) {
	std::shared_ptr<Conversation> oldConver = _conversation;
	_conversation = c;
	return std::make_shared<LinkNode>(*this);
	_conversation = oldConver;
}

void LinkNode::clear() {
	if(_conversation != nullptr) {
		_conversation = nullptr;

		if(_linkNodes != nullptr) {
			for(auto it = _linkNodes->begin(); it != _linkNodes->end(); it++) {
				(*it)->clear();
			}
		}
	}

}
