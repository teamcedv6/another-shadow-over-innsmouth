#include "QuestionText.h"

QuestionText::QuestionText() : ConversationNode(NodeType::NODE_QUESTION), _highlighted(0), _conversation(nullptr), _optionsNumber(0){}

QuestionText::QuestionText(std::shared_ptr<Conversation> conversation) : ConversationNode(NodeType::NODE_QUESTION), _highlighted(0), _conversation(conversation), _optionsNumber(0) {}

QuestionText::~QuestionText (){}

QuestionText::QuestionText(const QuestionText & questionText) : ConversationNode(questionText) {
	_highlighted = questionText._highlighted;
	_answers = questionText._answers;
	_conversation = questionText._conversation;
	_optionsNumber = questionText._optionsNumber;

	for(auto it = questionText._paths.begin(); it != questionText._paths.end(); it++) {
		std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> branch = std::make_shared<std::vector<std::shared_ptr<ConversationNode>>>();

		for(auto it2 = (*it)->begin(); it2 != (*it)->end(); it2++) {
			branch->push_back((*it2)->clone(_conversation));
		}

		_paths.push_back(branch);

	}
}
QuestionText::QuestionText(const QuestionText && questionText) : ConversationNode(questionText){
	_highlighted = questionText._highlighted;
	_answers = questionText._answers;
	_conversation = questionText._conversation;
	_optionsNumber = questionText._optionsNumber;
}

QuestionText& QuestionText::operator = (const QuestionText &questionText){
	_highlighted = questionText._highlighted;
	_answers = questionText._answers;
	_conversation = questionText._conversation;
	_optionsNumber = questionText._optionsNumber;
	return *this;
}

void QuestionText::addAnswer(const std::string & answer, std::string pathName, mVectorOfNodesPtr path) {

	_optionsNumber++;
	_answers.push_back(answer);

	_pathsNames.push_back(pathName);

	if(path == nullptr) {
		_paths.push_back(std::make_shared<std::vector<std::shared_ptr<ConversationNode>>>());
	}
	else {
		_paths.push_back(path);
	}
}

bool QuestionText::execute() {

	Noesis::Grid * questionZone = (Noesis::Grid *) _textRoot->FindName("grid");
	_highlighted = 0;

	int i = 0;

	// Generate options
	for(; i < _answers.size(); i++) {
		std::stringstream ss;
		ss << "option" << i;
		Noesis::TextBlock * questionText = (Noesis::TextBlock *)questionZone->FindName(ss.str().c_str());
		questionText->SetText(_answers[i].c_str());
		std::cout << "\t answer " << i << ": " << _answers[i] << "\n";

	}

	for (; i < 4; i++) {
		std::stringstream ss;
		ss << "option" << i;
		Noesis::TextBlock * questionText = (Noesis::TextBlock *)questionZone->FindName(ss.str().c_str());
		questionText->SetText(_answers[i].c_str());
		questionText->SetVisibility(Noesis::Visibility::Visibility_Hidden);
	}

	return true;
}

bool QuestionText::up() {
	// Play unhighlight animation
	std::stringstream ss;
	ss << "unfocus_" << _highlighted;
	auto ani = _textRoot->FindName(ss.str().c_str());
	((Noesis::Storyboard *) ani)->Begin();

	_highlighted--;
	if(_highlighted < 0 ) _highlighted = _optionsNumber - 1;

	// Play highlight animation
	std::stringstream ss2;
	ss2 << "focus_" << _highlighted;
	auto ani2 = _textRoot->FindName(ss2.str().c_str());
	((Noesis::Storyboard *) ani2)->Begin();

	return true;
}

bool QuestionText::down() {
	// Play unhighlight animation
	std::stringstream ss;
	ss << "unfocus_" << _highlighted;
	auto ani = _textRoot->FindName(ss.str().c_str());
	((Noesis::Storyboard *) ani)->Begin();

	_highlighted = (_highlighted + 1) % _optionsNumber;

	// Play highlight animation
	std::stringstream ss2;
	ss2 << "focus_" << _highlighted;
	auto ani2 = _textRoot->FindName(ss2.str().c_str());
	((Noesis::Storyboard *) ani2)->Begin();

	return true;
}

bool QuestionText::push() {
	// Response selected, continue with the branch desired
	// If path is not loaded, load it
	if(_paths[_highlighted]->size() == 0) {

	}
	// Otherwise, add this branch to the convesation
	else {
		for(auto it = _paths[_highlighted]->begin(); it < _paths[_highlighted]->end(); it++) {
			_conversation->addNode(*it);
		}
	}

	return false;
}

bool QuestionText::back() {
	return true;
}

std::shared_ptr<ConversationNode> QuestionText::clone(std::shared_ptr<Conversation> c) {
	std::shared_ptr<Conversation> oldConver = _conversation;
	_conversation = c;
	std::shared_ptr<QuestionText> q = std::make_shared<QuestionText>(*this);
	_conversation = oldConver;

	return q;
}

void QuestionText::clear() {
	if(_conversation != nullptr) {
		_conversation = nullptr;

		for(auto it = _paths.begin(); it != _paths.end(); it++) {
			for (auto it2 = (*it)->begin(); it2 != (*it)->end(); it2++) {
				(*it2)->clear();
			}
		}
	}

}
