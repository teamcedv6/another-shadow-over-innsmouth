#include "TriggerNode.h"

TriggerNode::TriggerNode() : ConversationNode(NodeType::NODE_TRIGGER), _trigger(nullptr) {}

TriggerNode::TriggerNode(std::shared_ptr<Trigger> trigger, std::shared_ptr<Conversation> conversation) :
				ConversationNode(NodeType::NODE_TRIGGER), _trigger(trigger), _truePath(nullptr),
				_falsePath(nullptr), _conversation(conversation) {}

TriggerNode::~TriggerNode (){/*std::cout << "TTT dest\n";*/}

TriggerNode::TriggerNode(const TriggerNode & triggerNode) : ConversationNode(triggerNode), _trigger(triggerNode._trigger->clone()),
_conversation(triggerNode._conversation) {
	std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> truePath = std::make_shared<std::vector<std::shared_ptr<ConversationNode>>>();
	std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> falsePath = std::make_shared<std::vector<std::shared_ptr<ConversationNode>>>();

	if(triggerNode._truePath != nullptr) {
		for(auto it = triggerNode._truePath->begin(); it != triggerNode._truePath->end(); it++) {
			truePath->push_back((*it)->clone(triggerNode._conversation));
		}
	}

	if(triggerNode._falsePath != nullptr) {
		for(auto it = triggerNode._falsePath->begin(); it != triggerNode._falsePath->end(); it++) {
				falsePath->push_back((*it)->clone(triggerNode._conversation));
		}
	}

	_truePath = truePath;
	_falsePath = falsePath;

}

TriggerNode::TriggerNode(const TriggerNode && triggerNode) : ConversationNode(triggerNode),  _trigger(triggerNode._trigger) {}

TriggerNode& TriggerNode::operator= (const TriggerNode &triggerNode){
	 _trigger = triggerNode._trigger->clone();
	return *this;
}

void TriggerNode::addTruPath(std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> truePath, const std::string & trueName) {
	_truePath = truePath;
	_trueName = trueName;
}

void TriggerNode::addFalsePath(std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> falsePath, const std::string & falseName) {
	_falsePath = falsePath;
	_falseName = falseName;
}

bool TriggerNode::execute() {
	if(_trigger->check()) {
		if(_truePath != nullptr) {
			std::cout << "TruePath "<<  _truePath->size() << "\n";
			for(auto it = _truePath->begin(); it != _truePath->end(); it++) {
							_conversation->addNode(*it);
			}
		}
		else {
			//Load _truePath
		}
	}
	else {
		if(_falsePath != nullptr) {
			for(auto it = _falsePath->begin(); it != _falsePath->end(); it++) {
				_conversation->addNode(*it);
			}
		}
		else {
			//Load _falsePath
		}
	}

	return false;
}

bool TriggerNode::up() {
	return true;
}

bool TriggerNode::down() {
	return true;
}

bool TriggerNode::push() {
	return true;
}

bool TriggerNode::back() {
	return true;
}

std::shared_ptr<ConversationNode> TriggerNode::clone(std::shared_ptr<Conversation> c) {
	std::shared_ptr<Conversation> oldConver = _conversation;
	_conversation = c;
	std::shared_ptr<TriggerNode> t = std::make_shared<TriggerNode>(*this);
	_conversation = oldConver;
	return t;
}

void TriggerNode::clear() {
	if(_conversation != nullptr) {
			_conversation = nullptr;

		if(_truePath != nullptr) {
			for(auto it = _truePath->begin(); it != _truePath->end(); it++) {
				(*it)->clear();
			}
		}
		if(_falsePath != nullptr) {
			for(auto it = _falsePath->begin(); it != _falsePath->end(); it++) {
				(*it)->clear();
			}
		}
	}
}
