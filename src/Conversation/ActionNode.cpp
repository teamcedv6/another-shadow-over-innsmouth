#include "ActionNode.h"

std::shared_ptr<ActionsManager> ActionNode::_actionManager = nullptr;

ActionNode::ActionNode() : ConversationNode(NodeType::NODE_ACTION), _action(nullptr) {}

ActionNode::ActionNode(std::shared_ptr<Action> action) : ConversationNode(NodeType::NODE_ACTION), _action(action) {}

ActionNode::~ActionNode (){}

ActionNode::ActionNode(const ActionNode & actionNode) : ConversationNode(actionNode) {
	_action = std::make_shared<Action>(*actionNode._action);
}

ActionNode::ActionNode(const ActionNode && actionNode) : ConversationNode(actionNode), _action(actionNode._action){}

ActionNode& ActionNode::operator = (const ActionNode &actionNode){
	_action = actionNode._action;
	return *this;
}

bool ActionNode::execute() {

	_actionManager->addAction(_action);

	return false;
}

bool ActionNode::up() {
	// Nothing to do
	return true;
}

bool ActionNode::down() {
	// Nothing to do
	return true;
}

bool ActionNode::push() {
	// Nothing to do
	return true;
}

bool ActionNode::back() {
	// Nothing to do
	return true;
}

std::shared_ptr<ConversationNode> ActionNode::clone(std::shared_ptr<Conversation> c) {
	return std::make_shared<ActionNode>(*this);
}

void ActionNode::clear() {}


void ActionNode::setActionManager(std::shared_ptr<ActionsManager> actionManager) { _actionManager = actionManager; }
