#!/bin/bash

for filename in ./*.skeleton.xml; do
	OgreXMLConverter "$filename"
done

for filename in ./*.mesh.xml; do
	OgreXMLConverter "$filename"
done
