/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef DISTANCE_TRIGGER_H_
#define DISTANCE_TRIGGER_H_

#include "Ogre.h"
#include "Trigger.h"

class DistanceTrigger : public Trigger{

public:

	DistanceTrigger();
	DistanceTrigger(Ogre::SceneNode * node, const Ogre::Vector3 & point, float distance);
	DistanceTrigger(const DistanceTrigger & distanceTrigger);
	DistanceTrigger(const DistanceTrigger && distanceTrigger);
	virtual ~DistanceTrigger();

	DistanceTrigger & operator= (const DistanceTrigger & distanceTrigger);

	bool check() override;
	std::shared_ptr<Trigger> clone() override;


private:
	Ogre::SceneNode * _node;
	Ogre::Vector3 _point;
	const float _distance;
};

#endif /* DISTANCE_TRIGGER_H_ */
