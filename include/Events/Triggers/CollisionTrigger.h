/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/
/*
#ifndef COLLISION_TRIGGER_H_
#define COLLISION_TRIGGER_H_

#include "Trigger.h"

class CollisionTrigger : public Trigger{

public:

	CollisionTrigger();
	CollisionTrigger(const std::string & name1, const std::string & name2);
	CollisionTrigger(const CollisionTrigger & collisionTrigger);
	CollisionTrigger(const CollisionTrigger && collisionTrigger);
	virtual ~CollisionTrigger();

	CollisionTrigger & operator= (const CollisionTrigger & collisionTrigger);

	bool check() override;

private:
	std::string _ame1;
	std::string _name2;
};

#endif*/ /* COLLISION_TRIGGER_H_ */
