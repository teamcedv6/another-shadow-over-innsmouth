/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef IN_ZONE_TRIGGER_H_
#define IN_ZONE_TRIGGER_H_

#include "Ogre.h"
#include "Trigger.h"

class InZoneTrigger : public Trigger{

public:

	InZoneTrigger();
	InZoneTrigger(Ogre::SceneNode * node, const Ogre::Vector2 & point1, const Ogre::Vector2 & point2);
	InZoneTrigger(const InZoneTrigger & inZoneTrigger);
	InZoneTrigger(const InZoneTrigger && inZoneTrigger);
	virtual ~InZoneTrigger();

	InZoneTrigger & operator= (const InZoneTrigger & inZoneTrigger);

	bool check() override;
	std::shared_ptr<Trigger> clone() override;

private:
	Ogre::SceneNode * _node;
	Ogre::Vector2 _p1, _p2;
};

#endif /* IN_ZONE_TRIGGER_H_ */
