/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef TRIGGER_H_
#define TRIGGER_H_

#include <memory>

class Trigger {

public:

	Trigger() {};
	Trigger(const Trigger & trigger) {};
	Trigger(const Trigger && trigger) {};
	virtual ~Trigger() {};

	Trigger & operator= (const Trigger & trigger) { return *this; };

	virtual bool check() = 0;
	virtual std::shared_ptr<Trigger> clone() = 0;
};


#endif /* TRIGGER_H_ */
