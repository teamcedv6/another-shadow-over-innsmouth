/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CHECK_STORY_VALUE_H_
#define CHECK_STORY_VALUE_H_

#include <string>
#include "GameLogic.h"

class CheckStoryValue : public Trigger {

public:

	CheckStoryValue();
	CheckStoryValue(const std::string & name, const int value, std::shared_ptr<GameLogic> gameLogic);
	CheckStoryValue(const CheckStoryValue & checkStoryValue);
	CheckStoryValue(const CheckStoryValue && checkStoryValue);
	virtual ~CheckStoryValue();

	CheckStoryValue & operator= (const CheckStoryValue & checkStoryValue);

	bool check() override;
	std::shared_ptr<Trigger> clone() override;

private:
	std::shared_ptr<GameLogic> _gameLogic;
	const std::string _valueName;
	const int _value;
};

#endif /* CHECK_STORY_VALUE_H_ */
