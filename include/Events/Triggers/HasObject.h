/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef HAS_OBJECT_H_
#define HAS_OBJECT_H_

#include <string>
#include <memory>

#include "Trigger.h"
#include "Inventory.h"

class HasObject : public Trigger {

public:

	HasObject();
	HasObject(const std::string & objectName, std::shared_ptr<Inventory> inventory);
	HasObject(const HasObject & hasObject);
	HasObject(const HasObject && hasObject);
	virtual ~HasObject();

	HasObject & operator= (const HasObject & hasObject);

	bool check() override;
	std::shared_ptr<Trigger> clone() override;

private:
	std::shared_ptr<Inventory> _inventory;
	std::string _objectName;
};

#endif /* HAS_OBJECT_H_ */
