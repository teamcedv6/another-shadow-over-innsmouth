/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef EVENT_FACTORY_H_
#define EVENT_FACTORY_H_

#include "Action.h"
#include "Trigger.h"
#include "EntityExtraLoader.h"

// Subactions
#include "InventoryChange.h"
#include "ChangeCamera.h"
#include "ChangeStoryValue.h"
#include "StartConversation.h"
#include "PlayFx.h"
#include "CreateEvent.h"
#include "CameraExchangeLoop.h"
#include "ChangeDaytime.h"


// Triggers
#include "DistanceTrigger.h"
#include "HasObject.h"
#include "CheckStoryValue.h"
#include "BooleanTrigger.h"
#include "InZoneTrigger.h"



class GameManager;

class EventFactory {

public:

	EventFactory();
	EventFactory(std::shared_ptr<GameManager> gameManager);
	EventFactory(const EventFactory & eventFactory);
	EventFactory(const EventFactory && eventFactory);
	virtual ~EventFactory();

	EventFactory & operator= (const EventFactory & eventFactory);

	// Subactions
	std::shared_ptr<Subaction> createSubaction(EntityExtraLoader::EventPart & subactionConf);
	std::shared_ptr<Trigger> createTrigger(EntityExtraLoader::EventPart & triggerConf);


private:
	std::map<std::string, std::function<std::shared_ptr<Subaction>(EntityExtraLoader::EventPart & subactionConf)>> _subactionBuilders;
	std::map<std::string, std::function<std::shared_ptr<Trigger>(EntityExtraLoader::EventPart & subactionConf)>> _triggerBuilders;
	std::shared_ptr<GameManager> _gameManager;

	void _initializeBuilders();
};


#endif /* EVENT_FACTORY_H_ */
