/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef ACTION_H_
#define ACTION_H_

#include <vector>
#include "Ogre.h"

// Forward declaration
class Subaction;

class Action {

	typedef std::shared_ptr<std::vector<std::shared_ptr<Subaction>>> subactionVector_ptr;
	typedef std::vector<std::shared_ptr<Subaction>>::iterator subactionVector_it;

public:

	enum ActionResult : unsigned char {
				CONTINUE,
				PLAYER_WIN,
				PLAYER_LOST,
				ACTION_FINISHED
	};

	Action();
	Action(const Action & action);
	Action(const Action && action);
	virtual ~Action();

	Action & operator= (const Action & action);

	void addSubaction(std::shared_ptr<Subaction> subaction);
	void addSubactionSequence(subactionVector_ptr sequence);

	ActionResult update(Ogre::Real deltaTime);

private:
	std::vector<subactionVector_ptr> _subactions;
	std::vector<subactionVector_it> _running;

};


#endif /* ACTION_H_ */
