/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef ADD_IMPULSE_H
#define ADD_IMPULSE_H

#include "Ogre.h"
#include "Entity.h"
#include "PhysicSystem.h"

#include "Subaction.h"

class AddImpulse : public Subaction {
public:

	AddImpulse();
	AddImpulse(std::shared_ptr<Entity> object, PhysicSystem* phSystem,Ogre::Vector3 vector);
	AddImpulse(const AddImpulse & addImpulse);
	AddImpulse(const AddImpulse && addImpulse);
	virtual ~AddImpulse();

	AddImpulse & operator= (const AddImpulse & addImpulse);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	PhysicSystem* _phSystem;
	std::shared_ptr<Entity> _object;
	Ogre::Vector3 _vector;
};

#endif /* ADD_IMPULSE_H */
