/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CAMERA_EXCHANGE_H_
#define CAMERA_EXCHANGE_H_

#include "ConversationSystem.h"
#include "Subaction.h"

#include "ChangeCamera.h"
#include "InZoneTrigger.h"
#include "Action.h"
#include "Trigger.h"
#include "EntityExtraLoader.h"

#include <Ogre.h>

class EventFactory;

class CameraExchangeLoop : public Subaction {
public:

	CameraExchangeLoop();
	CameraExchangeLoop(Ogre::SceneNode * node, const Ogre::Vector2 & zone1_1, const Ogre::Vector2 & zone1_2,
			const Ogre::Vector2 & zone2_1, const Ogre::Vector2 & zone2_2,
			const std::string & camera1, const std::string & camera2,
			std::shared_ptr<EventFactory> factory,std::shared_ptr<ActionsManager> actionManager);
	CameraExchangeLoop(const CameraExchangeLoop & cameraExchangeLoop);
	CameraExchangeLoop(const CameraExchangeLoop && cameraExchangeLoop);
	virtual ~CameraExchangeLoop();

	CameraExchangeLoop & operator= (const CameraExchangeLoop & cameraExchangeLoop);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	std::shared_ptr<ActionsManager> _actions;
	std::shared_ptr<Action> _action;
	EntityExtraLoader::EventPart _cameraExchangeConf;
	std::shared_ptr<EventFactory> _factory;
	std::shared_ptr<Trigger> _trigger;
};


#endif /* CAMERA_EXCHANGE_H_ */
