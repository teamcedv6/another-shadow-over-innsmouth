/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef ADD_PHYSIC_COMPONENT_H
#define ADD_PHYSIC_COMPONENT_H

#include "Entity.h"
#include "PhysicSystem.h"

#include "Subaction.h"

class AddPhysicComponent : public Subaction {
public:

	AddPhysicComponent();
	AddPhysicComponent(std::shared_ptr<Entity> entity, PhysicSystem* phSystem, PhysicSystem::ShapeForm shapeForm, PhysicSystem::ShapeType shapeType, PhysicComponent::PhysicProperties objProperties);
	AddPhysicComponent(const AddPhysicComponent & addPhysicComponent);
	AddPhysicComponent(const AddPhysicComponent && addPhysicComponent);
	virtual ~AddPhysicComponent();

	AddPhysicComponent & operator= (const AddPhysicComponent & addPhysicComponent);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	PhysicSystem* _phSystem;
	std::shared_ptr<Entity> _entity;
	PhysicSystem::ShapeForm _shapeForm;
	PhysicSystem::ShapeType _shapeType;
	PhysicComponent::PhysicProperties _objProperties;

	void _add();
};

#endif /* ADD_PHYSIC_COMPONENT_H */
