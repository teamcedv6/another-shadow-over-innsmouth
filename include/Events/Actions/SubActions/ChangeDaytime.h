/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CHANGE_DAYTIME_H_
#define CHANGE_DAYTIME_H_

#include "PlayState.h"
#include "Subaction.h"

class ChangeDaytime : public Subaction {
public:

	ChangeDaytime();
	ChangeDaytime(int n, PlayState * playState);
	ChangeDaytime(const ChangeDaytime & ChangeDaytime);
	ChangeDaytime(const ChangeDaytime && ChangeDaytime);
	virtual ~ChangeDaytime();

	ChangeDaytime & operator= (const ChangeDaytime & ChangeDaytime);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	int _n;
	PlayState *  _playState;

};


#endif /* CHANGE_DAYTIME_H_ */

