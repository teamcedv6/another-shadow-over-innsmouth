/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef INVENTORY_CHANGE_H_
#define INVENTORY_CHANGE_H_

#include "Inventory.h"
#include "Subaction.h"

class InventoryChange : public Subaction {
public:

	InventoryChange();
	InventoryChange(const std::string & name, std::shared_ptr<Inventory> inventory, int amount);
	InventoryChange(const InventoryChange & inventoryChange);
	InventoryChange(const InventoryChange && inventoryChange);
	virtual ~InventoryChange();

	InventoryChange & operator= (const InventoryChange & inventoryChange);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	std::shared_ptr<Inventory> _inventory;
	std::string _name;
	int _amount;
};


#endif /* INVENTORY_CHANGE_H_ */
