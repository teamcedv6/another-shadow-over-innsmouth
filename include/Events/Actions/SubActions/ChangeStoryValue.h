/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CHANGE_STORY_VALUE_H_
#define CHANGE_STORY_VALUE_H_

#include "ConversationSystem.h"
#include "Subaction.h"

class ChangeStoryValue : public Subaction {
public:

	ChangeStoryValue();
	ChangeStoryValue(const std::string & name, int value, std::shared_ptr<GameLogic> gameLogic, bool relative);
	ChangeStoryValue(const ChangeStoryValue & changeCamera);
	ChangeStoryValue(const ChangeStoryValue && changeCamera);
	virtual ~ChangeStoryValue();

	ChangeStoryValue & operator= (const ChangeStoryValue & changeCamera);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	std::string _name;
	int _value;
	std::shared_ptr<GameLogic> _gameLogic;
	bool _relative;
};


#endif /* CHANGE_STORY_VALUE_H_ */
