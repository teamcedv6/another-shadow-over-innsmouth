/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef ADD_NODE_H
#define ADD_NODE_H

#include "Ogre.h"
#include "Entity.h"
#include "NodeManager.h"

#include "Subaction.h"

class AddNode : public Subaction {
public:

	AddNode();
	AddNode(std::shared_ptr<Entity> entity, std::shared_ptr<NodeManager> ndManager, Ogre::Vector3 position);
	AddNode(const AddNode & addNode);
	AddNode(const AddNode && addNode);
	virtual ~AddNode();

	AddNode & operator= (const AddNode & addNode);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	std::shared_ptr<NodeManager> _ndManager;
	std::shared_ptr<Entity> _entity;
	Ogre::Vector3 _position;

	void _add();
};

#endif /* ADD_NODE_H */
