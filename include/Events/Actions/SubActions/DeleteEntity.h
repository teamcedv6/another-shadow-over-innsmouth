/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef DELETE_ENTITY_H_
#define DELETE_ENTITY_H_

#include "Ogre.h"
#include "Entity.h"
#include "NodeManager.h"

#include "GameManager.h"
#include "AISystem.h"
#include "AnimationSystem.h"
#include "CameraSystem.h"
#include "ConversationSystem.h"
#include "GraphicSystem.h"
#include "InteractionSystem.h"
#include "MovementSystem.h"
#include "PhysicSystem.h"
#include "UserInterfaceSystem.h"

#include "Subaction.h"

class DeleteEntity : public Subaction {
public:

	DeleteEntity();
	DeleteEntity(std::string name, std::shared_ptr<GameManager> gameManager);
	DeleteEntity(Entity::ID id, std::shared_ptr<GameManager> gameManager);
	DeleteEntity(const DeleteEntity & deleteEntity);
	DeleteEntity(const DeleteEntity && deleteEntity);
	virtual ~DeleteEntity();

	DeleteEntity & operator= (const DeleteEntity & deleteEntity);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	Entity::ID _id;
	std::shared_ptr<GameManager> _gameManager;

};


#endif /* DELETE_ENTITY_H_ */

