/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CREATE_EVENT_H_
#define CREATE_EVENT_H_

#include "Ogre.h"
#include "Entity.h"
#include "ActionsManager.h"

#include "Subaction.h"

class CreateEvent : public Subaction {
public:

	CreateEvent();
	CreateEvent(std::shared_ptr<Trigger> trigger, std::shared_ptr<Action> action, std::shared_ptr<ActionsManager> actionsManager);
	CreateEvent(const CreateEvent & createEvent);
	CreateEvent(const CreateEvent && createEvent);
	virtual ~CreateEvent();

	CreateEvent & operator= (const CreateEvent & createEvent);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	std::shared_ptr<ActionsManager> _actionsManager;
	std::shared_ptr<Trigger> _trigger;
	std::shared_ptr<Action> _action;

};


#endif /* CREATE_EVENT_H_ */
