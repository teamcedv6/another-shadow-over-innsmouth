/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef FUNCTOR_RUNNER_H_
#define FUNCTOR_RUNNER_H_

#include "Ogre.h"
#include "Entity.h"
#include "NodeManager.h"

#include "Subaction.h"

class FunctorRunner : public Subaction {
public:

	FunctorRunner();
	FunctorRunner(std::function<void(void)> f);
	FunctorRunner(const FunctorRunner & functorRunner);
	FunctorRunner(const FunctorRunner && functorRunner);
	virtual ~FunctorRunner();

	FunctorRunner & operator= (const FunctorRunner & functorRunner);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	std::function<void(void)> _f;

};


#endif /* FUNCTOR_RUNNER_H_ */
