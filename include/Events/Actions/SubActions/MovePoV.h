/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef MOVEPOV_H_
#define MOVEPOV_H_

#include "Ogre.h"
#include "Entity.h"
#include "NodeManager.h"

#include "Subaction.h"

class MovePoV : public Subaction {
public:

	MovePoV();
	MovePoV(std::shared_ptr<Entity> entity,  std::shared_ptr<NodeManager> nodeManager, float yaw, float pitch, float roll);
	MovePoV(const MovePoV & movePoV);
	MovePoV(const MovePoV && movePoV);
	virtual ~MovePoV();

	MovePoV & operator= (const MovePoV & movePoV);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	float _yaw, _pitch, _roll;
	std::shared_ptr<Entity> _entity;
	std::shared_ptr<NodeManager> _nodes;

};


#endif /* MOVEPOV_H_ */
