/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef PLAY_FX_H
#define PLAY_FX_H

#include "Ogre.h"
#include "Entity.h"
#include "SoundManager.h"

#include "Subaction.h"

class PlayFx : public Subaction {
public:

	PlayFx();
	PlayFx(std::shared_ptr<SoundManager> soundManager, std::string fxKey);
	PlayFx(const PlayFx & playFx);
	PlayFx(const PlayFx && playFx);
	virtual ~PlayFx();

	PlayFx & operator= (const PlayFx & playFx);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	std::shared_ptr<SoundManager> _soundManager;
	std::string _fxKey;
};

#endif /* PLAY_FX_H */
