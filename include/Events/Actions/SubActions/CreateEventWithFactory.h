/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CREATE_EVENT_FACTORY_H_
#define CREATE_EVENT_FACTORY_H_

#include "Ogre.h"
#include "Entity.h"
#include "ActionsManager.h"
#include "EventFactory.h"
#include "EntityExtraLoader.h"

#include "Subaction.h"

class CreateEventWithFactory : public Subaction {
public:

	CreateEventWithFactory();
	CreateEventWithFactory(EntityExtraLoader::EventPart & triggerConf, EntityExtraLoader::EventPart & subactionConf, std::shared_ptr<ActionsManager> actionsManager, EventFactory * factory);
	CreateEventWithFactory(const CreateEventWithFactory & createEventWithFactory);
	CreateEventWithFactory(const CreateEventWithFactory && createEventWithFactory);
	virtual ~CreateEventWithFactory();

	CreateEventWithFactory & operator= (const CreateEventWithFactory & createEventWithFactory);

	Action::ActionResult update(Ogre::Real deltaTime) override;
	std::shared_ptr<Subaction> clone() override;

private:
	std::shared_ptr<ActionsManager> _actionsManager;
	EventFactory * _factory;
	EntityExtraLoader::EventPart _triggerConf;
	EntityExtraLoader::EventPart _subactionConf;
};


#endif /* CREATE_EVENT_FACTORY_H_ */
