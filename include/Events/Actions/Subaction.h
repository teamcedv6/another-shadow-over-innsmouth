/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef SUBACTION_H_
#define SUBACTION_H_

#include "Ogre.h"
#include "Action.h"

class Subaction {
public:

	Subaction();
	Subaction(const Subaction & subaction);
	Subaction(const Subaction && subaction);
	virtual ~Subaction();

	Subaction & operator= (const Subaction & subaction);

	virtual Action::ActionResult update(Ogre::Real deltaTime) = 0;
	virtual std::shared_ptr<Subaction> clone() = 0;

	bool hasFinished();

protected:
	bool _hasFinished;

};


#endif /* SUBACTION_H_ */
