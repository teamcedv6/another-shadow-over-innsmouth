/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef OBJ_H
#define OBJ_H

#include "Configuration.h"
#include "NodeManager.h"
#include "GraphicSystem.h"
#include "ActionsManager.h"
#include "Entity.h"

class PhysicSystem;

class Obj {

	public:

		enum TypeObj {
			CUSTOM
		};

		enum TypeAction {
			NEW,
			DELETE
		};

		typedef struct Options {
			bool addNode;
			bool attachNode;
			bool addGraphic;
			bool addPhysic;
			bool addImpulse;
			TypeObj typeObj;
			TypeAction typeAct;
			std::string entityName;
		} Options_t;

		Obj();
		Obj(std::shared_ptr<Configuration> config, std::shared_ptr<NodeManager> nodes,
				std::shared_ptr<GraphicSystem> graphic, PhysicSystem* physic,
				std::shared_ptr<ActionsManager> actions, std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> entities,
				Obj::Options_t options);
		Obj(const Obj & obj);
		Obj(const Obj && obj);
		virtual ~Obj();

		Obj & operator= (const Obj & obj);

	private:

		void _new();
		void _delete();

		std::shared_ptr<Configuration> _config;
		std::shared_ptr<NodeManager> _nodes;
		std::shared_ptr<GraphicSystem> _graphic;
		PhysicSystem* _physic;
		std::shared_ptr<ActionsManager> _actions;
		std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> _entities;
		Options_t _options;
};

#endif /* OBJ_H */
