/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef ACTION_NODE_H_
#define ACTION_NODE_H_

#include "ConversationNode.h"
#include "ActionsManager.h"

class ActionNode : public ConversationNode {

public:

	ActionNode();
	ActionNode(std::shared_ptr<Action> action);

	ActionNode(const ActionNode & actionNode);
	ActionNode(const ActionNode && actionNode);
	virtual ~ActionNode ();

	ActionNode & operator= (const ActionNode & actionNode);

	bool execute() override;
	bool up()      override;
	bool down()    override;
	bool push()    override;
	bool back()    override;

	static void setActionManager(std::shared_ptr<ActionsManager> actionManager);

	std::shared_ptr<ConversationNode> clone(std::shared_ptr<Conversation> c) override;
	void clear() override;

private:
	std::shared_ptr<Action> _action;
	static std::shared_ptr<ActionsManager> _actionManager;
};
#endif /* END ACTION_NODE_H_ */
