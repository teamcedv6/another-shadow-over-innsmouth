/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef QUESTION_TEXT_H_
#define QUESTION_TEXT_H_

#include "ConversationNode.h"
#include "Conversation.h"

class QuestionText : public ConversationNode {

public:

	typedef std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> mVectorOfNodesPtr;

	QuestionText();
	QuestionText(std::shared_ptr<Conversation> conversation);

	QuestionText(const QuestionText & questionText);
	QuestionText(const QuestionText && questionText);
	virtual ~QuestionText ();

	QuestionText & operator= (const QuestionText & questionText);

	void addAnswer(const std::string & answer, std::string pathName, mVectorOfNodesPtr path);

	bool execute() override;
	bool up()      override;
	bool down()    override;
	bool push()    override;
	bool back()    override;

	std::shared_ptr<ConversationNode> clone(std::shared_ptr<Conversation> c) override;
	void clear() override;

private:
	int _highlighted;
	int _optionsNumber;

	std::vector<std::string> _answers;
	std::vector<std::string> _pathsNames;
	std::vector<std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>>> _paths;
	std::shared_ptr<Conversation> _conversation;


};
#endif /* END QUESTION_TEXT_H_ */
