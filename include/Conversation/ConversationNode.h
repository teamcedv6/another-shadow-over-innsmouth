/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CONVERSATION_NODE_H_
#define CONVERSATION_NODE_H_

#include <vector>
#include "Ogre.h"

#include "pch.h"

#include "Action.h"

using namespace std;

class Conversation;

class ConversationNode {

public:

	enum NodeType {
		NODE_EMPTY,
		NODE_TEXT,
		NODE_QUESTION,
		NODE_LINK,
		NODE_ACTION,
		NODE_TRIGGER,
		NODE_THOUGHT
	};

	ConversationNode() : _nodeType(NodeType::NODE_EMPTY){};
	ConversationNode(NodeType nodeType) : _nodeType(nodeType) {};

	ConversationNode(const ConversationNode & conversationNode) : _nodeType(conversationNode._nodeType){};
	ConversationNode(const ConversationNode && conversationNode): _nodeType(conversationNode._nodeType){};
	virtual ~ConversationNode () {};

	const NodeType getNodeType() { return _nodeType; };

	static void setTextRoot(Noesis::UserControl * textRoot) { _textRoot = textRoot; };

	ConversationNode & operator= (const ConversationNode & conversationNode) { std::cout << "NODE ="; return *this;};
	ConversationNode & operator= (const ConversationNode && conversationNode) { std::cout << "NODE MOVE ="; return *this;};

	virtual bool execute() = 0;
	virtual bool up()      = 0;
	virtual bool down()    = 0;
	virtual bool push()    = 0;
	virtual bool back()    = 0;

	virtual std::shared_ptr<ConversationNode> clone(std::shared_ptr<Conversation> c) = 0;
	virtual void clear() = 0;

protected:
	// UI where the text is display
	static Noesis::UserControl * _textRoot;
	const NodeType _nodeType;

};

#endif /* END CONVERSATION_NODE_H_ */
