/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CONVERSATION_H_
#define CONVERSATION_H_

#include <vector>
#include "Ogre.h"

#include "Action.h"
#include "ConversationNode.h"

using namespace std;

class Conversation {
public:

	Conversation();
	Conversation(bool isHijacking);
	Conversation(const Conversation & conversation);
	Conversation(const Conversation && conversation);
	~Conversation ();

	Conversation & operator= (const Conversation & conversation);
	Conversation & operator= (const Conversation && conversation);

	void addNode(std::shared_ptr<ConversationNode> node);
	void initConversation();

	bool nextNode();
	std::shared_ptr<ConversationNode> getNode();
	bool hasInputHijacked();

	std::shared_ptr<Conversation> clone();
	void clear();

private:
	std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> _nodes;
	std::vector<std::shared_ptr<ConversationNode>>::iterator _current;
	bool _hijackInput;
};
#endif /* END CONVERSATION_H_ */
