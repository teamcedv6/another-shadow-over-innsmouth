/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef NORMAL_TEXT_H_
#define NORMAL_TEXT_H_

#include "ConversationNode.h"

class NormalText : public ConversationNode {

public:

	NormalText();
	NormalText(std::string name, std::string text, std::string picture);

	NormalText(const NormalText & normalText);
	NormalText(const NormalText && normalText);
	virtual ~NormalText ();

	NormalText & operator= (const NormalText & normalText);

	bool execute() override;
	bool up()      override;
	bool down()    override;
	bool push()    override;
	bool back()    override;

	std::shared_ptr<ConversationNode> clone(std::shared_ptr<Conversation> c) override;
	void clear() override;

private:
	std::string _name;
	std::string _text;
	std::string _picture;
};
#endif /* END NORMAL_TEXT_H_ */
