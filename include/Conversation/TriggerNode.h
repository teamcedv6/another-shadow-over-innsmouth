/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef TRIGGER_NODE_H_
#define TRIGGER_NODE_H_

#include "ConversationNode.h"
#include "Trigger.h"
#include "Conversation.h"

class TriggerNode : public ConversationNode {

public:

	TriggerNode();
	TriggerNode(std::shared_ptr<Trigger> trigger, std::shared_ptr<Conversation> conversation);

	TriggerNode(const TriggerNode & triggerNode);
	TriggerNode(const TriggerNode && triggerNode);
	virtual ~TriggerNode ();

	TriggerNode & operator= (const TriggerNode & triggerNode);

	void addTruPath(std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> truePath, const std::string & trueName);
	void addFalsePath(std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> falsePath, const std::string & falseName);

	bool execute() override;
	bool up()      override;
	bool down()    override;
	bool push()    override;
	bool back()    override;


	std::shared_ptr<ConversationNode> clone(std::shared_ptr<Conversation> c) override;
	void clear() override;

private:
	std::shared_ptr<Trigger> _trigger;
	std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> _truePath;
	std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> _falsePath;
	std::string _trueName;
	std::string _falseName;
	std::shared_ptr<Conversation> _conversation;
};
#endif /* END TRIGGER_NODE_H_ */
