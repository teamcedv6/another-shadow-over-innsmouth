/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef LINK_NODE_H_
#define LINK_NODE_H_

#include "Conversation.h"
#include "ConversationNode.h"

class LinkNode : public ConversationNode {

public:

	LinkNode();
	LinkNode(std::string linkName, std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> linkNodes, std::shared_ptr<Conversation> conversation);

	LinkNode(const LinkNode & linkNode);
	LinkNode(const LinkNode && linkNode);
	virtual ~LinkNode ();

	LinkNode & operator= (const LinkNode & linkNode);

	bool execute() override;
	bool up()      override;
	bool down()    override;
	bool push()    override;
	bool back()    override;

	std::shared_ptr<ConversationNode> clone(std::shared_ptr<Conversation> c) override;
	void clear() override;

private:
	std::string _linkName;
	std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> _linkNodes;
	std::shared_ptr<Conversation> _conversation;
};
#endif /* END LINK_NODE_H_ */
