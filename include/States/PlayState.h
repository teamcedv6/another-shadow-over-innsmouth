/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef PLAY_STATE_H
#define PLAY_STATE_H

#include <Ogre.h>
#include <OIS.h>

#include "GameState.h"
#include "GameManager.h"

#include "InteractionSystem.h"
#include "InventoryState.h"
#include "InterludeState.h"

#include "MovePoV.h"
#include "Obj.h"
#include "AddImpulse.h"
#include "GameLogic.h"
#include <memory>

#include "FunctorRunner.h"
#include "DistanceTrigger.h"

#include "AISystem.h"
#include "OgreRecast.h"
#include "OgreDetourCrowd.h"


class PlayState : public GameState {

	public:

		PlayState(shared_ptr<GameManager> gameManager): GameState(gameManager), _fov(nullptr),
		_nodes(nullptr),_actions(nullptr),_grSystem(nullptr),_phSystem(nullptr),_aSystem(nullptr),_cmSystem(nullptr),
		_move(nullptr),_lose(false), _exitGame(false), _converSystem(nullptr) {}

		// States Actions
		void enter ();
		void exit ();
		void pause ();
		void resume ();

		void movePoV(const OIS::MouseEvent & e);
		void moveMainCharacterKeyboard(InputBindings::PushState state, int x, int y);
		void moveMainCharacterGamepad(int id, const OIS::JoyStickEvent & e, InputBindings::PushState state);
		void openInventory(InputBindings::PushState state);

		void actionPush(InputBindings::PushState state, int n);

		void setLose(bool lose);

		void aiDebug();

		// Game Loop
		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		void exitGame(InputBindings::PushState pushState);

		void setDaytime(int n);

	protected:

		void _moveMainCharacter();
		Ogre::Quaternion * _cameraOrientation;

		std::shared_ptr<Entity> _fov;

		std::shared_ptr<NodeManager> _nodes;
		std::shared_ptr<ActionsManager> _actions;
		std::shared_ptr<GraphicSystem> _grSystem;
		std::shared_ptr<PhysicSystem> _phSystem;
		std::shared_ptr<AnimationSystem> _aSystem;
		std::shared_ptr<CameraSystem> _cmSystem;
		std::shared_ptr<MovementSystem> _move;
		std::shared_ptr<UserInterfaceSystem> _ui;
		std::shared_ptr<GameLogic> _gameLogic;
		std::shared_ptr<ConversationSystem> _converSystem;
		std::shared_ptr<InteractionSystem> _interactionSystem;
		std::shared_ptr<AISystem> _ai;

		Ogre::ParticleSystem* _sun;
		Ogre::ParticleSystem* _moon;

		Ogre::Real _time;
		Ogre::ParticleSystem* _rain;

		bool _lose;
		bool _exitGame;

		bool _movingX = false;
		bool _movingY = false;
		bool _previouslyMoving = false;
		float _vX = 0.0;
		float _vY = 0.0;

		std::shared_ptr<Entity> _character;
		std::shared_ptr<Entity> _enemy;

		std::string _player;

		bool _aiDebug;

		bool _finish;

};

#endif /* END PLAY_STATE_H*/
