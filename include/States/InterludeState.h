/*********************************************************************
 * Basic Game Structure - Ogre3d - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.  
 *********************************************************************/

#ifndef INTERLUDE_STATE_H_
#define INTERLUDE_STATE_H_

#include "GameState.h"
#include "GameManager.h"

#include "ConversationSystem.h"
#include <Ogre.h>
#include <OIS.h>

#include "pch.h"


class InterludeState : public GameState {

	public:

		InterludeState(shared_ptr<GameManager> gameManager): GameState(gameManager) {}

		// States Actions
		void enter ();
		void exit ();
		void pause ();
		void resume ();

		// Game Loop
		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		void up(InputBindings::PushState pushState);
		void down(InputBindings::PushState pushState);
		void push(InputBindings::PushState pushState);
		void back(InputBindings::PushState pushState);

		void gamePadUpDown(int id, const OIS::JoyStickEvent & e, InputBindings::PushState pushState);

		void setDayTime(int n);

	private:
		std::shared_ptr<UserInterfaceSystem> _ui;
		std::shared_ptr<ConversationSystem> _conversationSystem;

		int _daytime;
		float _timer;
		int _internalState;
		Noesis::UserControl * _root;
};


#endif /* END INTERLUDE_STATE_H_*/
