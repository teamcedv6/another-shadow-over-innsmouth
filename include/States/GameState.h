/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef GAME_STATE_H
#define GAME_STATE_H

#include <Ogre.h>
#include <OIS.h>
 
#include "GameManager.h"
#include "InputManager.h"
#include "InputBindings.h"

class GameState {

	public:

		GameState(shared_ptr<GameManager> gameManager): _gameManager(gameManager), _bindings(make_shared<InputBindings>()) {}
		virtual ~GameState(){}
		// Game States Id
		enum States {
			MAIN_MENU,
			INGAME_MENU,
			AUTHOR,
			SCORE,
			OPTIONS,
			LOAD,
			PLAY,
			PAUSE,
			CONVERSATION,
			AI,
			INVENTORY,
			INTERLUDE
		};

		// State Manager
		virtual void enter () = 0;
		virtual void exit () = 0;
		virtual void pause () = 0;
		virtual void resume () = 0;

		// OIS Listeners
		void keyPressed (const OIS::KeyEvent &e) {
			_bindings->executeHook(e.key, InputBindings::PushState::PUSH);
		};

		void keyReleased (const OIS::KeyEvent &e) {
			_bindings->executeHook(e.key, InputBindings::PushState::RELEASE);
		};

		void mouseMoved (const OIS::MouseEvent &e) {
			_bindings->executeHook(e);
		};

		void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
			_bindings->executeHook(e, id, InputBindings::PushState::PUSH);
		};

		void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
			_bindings->executeHook(e, id, InputBindings::PushState::RELEASE);
		};

		bool povMoved( const OIS::JoyStickEvent &e, int pov ) {
			_bindings->executeHook(e, InputBindings::POV, pov, InputBindings::PushState::PUSH);
			return true;
		}

		bool axisMoved( const OIS::JoyStickEvent &e, int axis ) {
			_bindings->executeHook(e, InputBindings::AXIS, axis, InputBindings::PushState::PUSH);
			return true;
		}

		bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID ) {
			_bindings->executeHook(e, InputBindings::SLIDER, sliderID, InputBindings::PushState::PUSH);
			return true;
		}

		bool buttonPressed( const OIS::JoyStickEvent &e, int button ) {
			_bindings->executeHook(e, InputBindings::BUTTON, button, InputBindings::PushState::PUSH);
			return true;
		}

		bool buttonReleased( const OIS::JoyStickEvent &e, int button ) {
			_bindings->executeHook(e, InputBindings::BUTTON, button, InputBindings::PushState::RELEASE);
			return true;
		}
	
		// Game Loop
		virtual bool frameStarted (const Ogre::FrameEvent& evt) = 0;
		virtual bool frameEnded (const Ogre::FrameEvent& evt) = 0;

		// Game states transitions
		void changeState (shared_ptr<GameManager> gameManager, shared_ptr<GameState> state) {
			gameManager->changeState(state);
		}
		void pushState (shared_ptr<GameManager> gameManager, shared_ptr<GameState> state) {
			gameManager->pushState(state);
		}
		void popState (shared_ptr<GameManager> gameManager) {
			gameManager->popState();
		}

		shared_ptr<InputBindings> getInputBindings() {
			return _bindings;
		}

		void setInputBindings(shared_ptr<InputBindings> bindings) {
			_bindings = bindings;
		}

	protected:
		shared_ptr<GameManager> _gameManager;
		shared_ptr<InputBindings> _bindings;
};

#endif /* END GAME_STATE_H */
