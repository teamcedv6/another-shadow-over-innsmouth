/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef SCORE_STATE_H
#define SCORE_STATE_H

#include <Ogre.h>
#include <OIS.h>

#include "GameState.h"
#include "XmlEditor.h"


class ScoreState : public GameState {
	public:

		enum ScoreType {
			  READ=0,
			  WRITE=1
		};

		void setType(ScoreType type);

		typedef struct ScoreSave {
			std::string name;
			int points;
		}ScoreSave_t;

		ScoreState(shared_ptr<GameManager> gameManager): GameState(gameManager),_exitGame(false), _playReturn(false) {}

		// State Actions
		void enter ();
		void exit ();
		void pause ();
		void resume ();

		// Game Loop
		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		void loadScore();
		void writeScore();

		void menuClick(Noesis::BaseComponent * sender, const Noesis::MouseButtonEventArgs & args);

	protected:

		bool _exitGame;
		std::shared_ptr<UserInterfaceSystem> _ui;
		std::shared_ptr<GameLogic> _gameLogic;
		bool _playReturn;

		ScoreType _type;

		void _proccessScore();
		void _writeXml(ScoreState::ScoreSave_t scoreSave);
};

#endif /* END SCORE_STATE_H */
