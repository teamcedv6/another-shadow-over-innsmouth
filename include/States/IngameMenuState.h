/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef INGAME_MENU_STATE_H
#define INGAME_MENU_STATE_H

#include <Ogre.h>
#include <OIS.h>

#include "GameState.h"

class IngameMenuState : public GameState {
	public:

		IngameMenuState(shared_ptr<GameManager> gameManager): GameState(gameManager),_exitGame(false) {}

		// State Actions
		void enter ();
		void exit ();
		void pause ();
		void resume ();

		// Game Loop
		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

	protected:

		bool _exitGame;
};

#endif /* END INGAME_MENU_STATE_H */
