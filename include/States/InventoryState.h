/*********************************************************************
 * Basic Game Structure - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef INVENTORY_STATE_H
#define INVENTORY_STATE_H

#include "GameState.h"
#include "GameManager.h"

#include "Inventory.h"
#include "UserInterfaceSystem.h"
#include "pch.h"
#include "ConversationSystem.h"

#include "ConversationSystem.h"

#include <OIS.h>

class InventoryState : public GameState {
	public:
		enum SelectionType {
			GIVE,
			USE
		};

		InventoryState(shared_ptr<GameManager> gameManager): GameState(gameManager) { _rowLenght = 5; _rowNumber = 3;};

		// State Actions
		void enter ();
		void exit ();
		void pause ();
		void resume ();

		// Game Loop
		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		void up(InputBindings::PushState pushState);
		void left(InputBindings::PushState pushState);
		void right(InputBindings::PushState pushState);
		void down(InputBindings::PushState pushState);
		void look(InputBindings::PushState pushState);
		void select(InputBindings::PushState pushState);
		void back(InputBindings::PushState pushState);
		void gamePadMove(int id, const OIS::JoyStickEvent & e, InputBindings::PushState pushState);

		void setItemSelectionMode(bool selection, SelectionType type);
		std::shared_ptr<Inventory::InventoryObject> getSelectedItem();

	protected:
		std::shared_ptr<UserInterfaceSystem> _ui;
		std::shared_ptr<Inventory> _inventory;
		std::shared_ptr<ConversationSystem> _conversations;
		int _selectedColumn;
		int _selectedRow;
		int _rowLenght;
		int _rowNumber;

		bool _itemSelection = false;
		SelectionType _selectionType;
		std::shared_ptr<Inventory::InventoryObject> _selected = nullptr;


		Noesis::UserControl * _rootUI;

		Ogre::Real _waitTime;
		bool _exitState;
		int _nObjects;

		void _focus();
		void _unfocus();
		void _reloadUI();
};

#endif /* END INVENTORY_STATE_H */
