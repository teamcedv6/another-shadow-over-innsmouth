/*********************************************************************
 * Basic Game Structure - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef AI_STATE_H
#define AI_STATE_H

#include <Ogre.h>
#include <OIS.h>

#include "GameState.h"
#include "GameManager.h"

#include "MovePoV.h"
#include "Obj.h"
#include "AddImpulse.h"
#include "GameLogic.h"
#include <memory>

#include <DotSceneLoader.h>


#include "OgreRecast.h"
#include "OgreDetourCrowd.h"
#include "Character.h"
#include "AnimateableCharacter.h"

class AIState : public GameState {

	public:

		AIState	(shared_ptr<GameManager> gameManager): GameState(gameManager), _fov(nullptr),
		_nodes(nullptr),_actions(nullptr),_grSystem(nullptr),_phSystem(nullptr),_aSystem(nullptr),_cmSystem(nullptr),_move(nullptr),_lose(false), _exitGame(false) {}

		// States Actions
		void enter ();
		void exit ();
		void pause ();
		void resume ();

		void movePoV(const OIS::MouseEvent & e);
		void setLose(bool lose);
		void _navMeshUpdater(string);
		void obstacle1(InputBindings::PushState pushState);
		void obstacle2(InputBindings::PushState pushState);
		void obstacle3(InputBindings::PushState pushState);
		void obstacle4(InputBindings::PushState pushState);

		void moveMainCharacterGamepad(int id, const OIS::JoyStickEvent & e, InputBindings::PushState state);
		void moveMainCharacterKeyboard(InputBindings::PushState state, int x, int y);


		// Game Loop
		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		void exitGame(InputBindings::PushState pushState);

	protected:
	
		std::shared_ptr<Entity> _fov;

		std::shared_ptr<NodeManager> _nodes;
		std::shared_ptr<ActionsManager> _actions;
		std::shared_ptr<GraphicSystem> _grSystem;
		std::shared_ptr<PhysicSystem> _phSystem;
		std::shared_ptr<AnimationSystem> _aSystem;
		std::shared_ptr<CameraSystem> _cmSystem;
		std::shared_ptr<MovementSystem> _move;
		std::shared_ptr<UserInterfaceSystem> _ui;
		std::shared_ptr<AISystem> _ai;
		std::shared_ptr<GameLogic> _gameLogic;
		bool _lose;
		bool _exitGame;


		std::shared_ptr<Entity> _character;
		std::shared_ptr<Entity> _enemy;
		Ogre::Real _time;
		bool _stop;

		std::string _player;

	    void _moveMainCharacter();
	    bool _movingX = false;
		bool _movingY = false;
		float _vX = 0.0;
		float _vY = 0.0;

};

#endif /* END AI_STATE_H*/
