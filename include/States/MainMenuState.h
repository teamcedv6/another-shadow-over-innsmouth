/*********************************************************************
 * Basic Game Structure - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef MAIN_MENU_STATE_H
#define MAIN_MENU_STATE_H

#include "GameState.h"
#include "GameManager.h"

#include <Ogre.h>
#include <DotSceneLoader.h>
#include "ConversationSystem.h"
#include <OIS.h>

#include "ScoreState.h"

#include "Conversation.h"
#include "NormalText.h"
#include "QuestionText.h"

#include <sstream>

class MainMenuState : public GameState {
	public:

		MainMenuState(shared_ptr<GameManager> gameManager): GameState(gameManager),_exitGame(false) {}

		// State Actions
		void enter ();
		void exit ();
		void pause ();
		void resume ();

		// Game Loop
		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		void goAIState(InputBindings::PushState pushState);
		void startGame(InputBindings::PushState pushState);
		void exitGame(InputBindings::PushState pushState);

	protected:

		bool _exitGame;
		std::shared_ptr<UserInterfaceSystem> _ui;
		Noesis::Storyboard * _anim;
		float _timer;

		void _loadGui();
};

#endif /* END MAIN_MENU_STATE_H */
