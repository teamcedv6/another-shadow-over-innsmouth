/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CONVERSATION_LOADER_H_
#define CONVERSATION_LOADER_H_

#include <memory>
#include <map>
#include <iostream>

#include "XmlEditor.h"
#include "Conversation.h"
#include "NormalText.h"
#include "ThoughtNode.h"
#include "QuestionText.h"
#include "LinkNode.h"
#include "ActionNode.h"
#include "TriggerNode.h"

// Forward declaration
class EntityExtraLoader;
class EventFactory;

// Options for Game
class ConversationLoader{

	public:

		ConversationLoader();
		ConversationLoader(bool fullLoad, std::shared_ptr<EventFactory> factory);
		ConversationLoader(const ConversationLoader & conversationLoader);
		ConversationLoader(const ConversationLoader && conversationLoader);
		~ConversationLoader();
		ConversationLoader& operator = (const ConversationLoader &conversationLoader);

		void setExtraLoader(std::shared_ptr<EntityExtraLoader> loader);

		std::shared_ptr<Conversation> loadConversation(const std::string & name);
		std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> loadConversationNodes(const std::string & name, std::shared_ptr<Conversation> conversation);

	private:
		void _proccessConversation(TiXmlElement* conversation);
		void _proccessNodesVector(TiXmlElement* firstNode);
		shared_ptr<ConversationNode> _processNode(TiXmlElement * node);
		shared_ptr<NormalText> _processNormalText(TiXmlElement * node);
		shared_ptr<ThoughtNode> _processThoughtText(TiXmlElement * node);
		shared_ptr<ActionNode> _processActionNode(TiXmlElement * node);
		shared_ptr<TriggerNode> _processTriggerNode(TiXmlElement * node);
		shared_ptr<QuestionText> _processQuestionNode(TiXmlElement * node);
		shared_ptr<LinkNode> _processLinkNode(TiXmlElement * node);


		bool _fullLoad;

		std::shared_ptr<Conversation> _conversation;
		std::shared_ptr<std::vector<std::shared_ptr<ConversationNode>>> _nodesVector;
		std::shared_ptr<EntityExtraLoader> _loader;
		std::shared_ptr<EventFactory> _factory;

};
#endif /* CONVERSATION_LOADER_H_ */
