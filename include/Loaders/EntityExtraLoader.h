/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef ENTITY_EXTRA_LOADER_H_
#define ENTITY_EXTRA_LOADER_H_

#include <memory>
#include <map>
#include <iostream>

#include "XmlEditor.h"
#include "DotSceneLoader.h"
#include "InteractionComponent.h"
#include "ConversationSystem.h"
#include "InteractionSystem.h"
#include "ActionsManager.h"
#include "Inventory.h"
#include "ConversationLoader.h"

//using namespace Ogre;

// ForwardDeclaration
class GameManager;
class EventFactory;

// Options for Game
class EntityExtraLoader{

	friend class ConversationLoader;

	public:

		struct ParamValue{
			int intValue;
			float floatValue;
			std::string strValue;
		};

		struct Param{
			std::string type;
			 ParamValue paramValue;
		};

		struct EventPart {
			std::string name;
			std::vector<Param> params;
		};

		EntityExtraLoader();
		EntityExtraLoader(std::shared_ptr<GameManager> gameManager);
		EntityExtraLoader(const EntityExtraLoader & entityExtraLoader);
		EntityExtraLoader(const EntityExtraLoader && entityExtraLoader);
		~EntityExtraLoader();
		EntityExtraLoader& operator = (const EntityExtraLoader &entityExtraLoader);

		void preloadConfiguration();
		void loadConversationComponent(const std::string & name);
		void loadInteractionComponent(std::shared_ptr<Ogre::DotSceneLoader::Extra> extraConf);
		void loadEvents(std::shared_ptr<Ogre::DotSceneLoader::Extra> extraConf);
		void loadPatrolPoints(const std::string & name);

	private:

		struct ConverConf {
			std::string name;
		};

		struct EventConf {
			std::string type;
			std::string source;
			std::string target;
			shared_ptr<EventPart> trigger;
			std::vector<shared_ptr<EventPart>> subactions;
		};

		struct InteractionConf {
			float activeDistance;
			float interactionDistance;
			InteractionComponent::InteractionType type;
		};

		struct AIConf {
			std::vector<Ogre::Vector3> agentPatrol;
		};

		struct EntityConfigurations {
			ConverConf converConf;
			std::vector<shared_ptr<EventConf>> eventConfs;
			std::shared_ptr<InteractionConf> interactionConf;
			std::shared_ptr<AIConf> aiConf;
		};

		void _preloadExtraConfig();
		void _loadInventory();

		void _processConversation(const TiXmlElement * pElement);
		void _processEvent(const TiXmlElement * pElement);
		std::shared_ptr<EventPart> _processTrigger(const TiXmlElement * pElement);
		std::shared_ptr<EventPart> _processSubaction(const TiXmlElement * pElement);
		void _processInteraction(const TiXmlElement * pElement);
		void _processPatrol(const TiXmlElement * pElement);
		void _processParam(const TiXmlElement * paramElement, Param & param);


		std::map<std::string, shared_ptr<EntityConfigurations>> _entitiesConfMap;
		shared_ptr<EntityConfigurations> mEntConf;

		shared_ptr<ConversationLoader> _conversationLoader;
		shared_ptr<ConversationSystem> _conversationSystem;
		shared_ptr<InteractionSystem> _interactionSystem;
		shared_ptr<ActionsManager> _actionsManager;
		shared_ptr<EventFactory> _eventFactory;
		shared_ptr<IdNameMapper> _mapper;
		shared_ptr<Inventory> _inventory;
		shared_ptr<AISystem> _aiSystem;

};
#endif /* ENTITY_EXTRA_LOADER_H_ */
