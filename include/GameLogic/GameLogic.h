/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef GAME_LOGIC_H
#define GAME_LOGIC_H

#include <map>
#include <vector>
#include <random>
#include <string>
#include <tuple>

#include "Ogre.h"

#include "SoundManager.h"

#include "Obj.h"

#include "pch.h"


class PhysicSystem;

// Game Logic
class GameLogic{

	public:

		GameLogic();
		GameLogic(const GameLogic & gameLogic);
		GameLogic(const GameLogic && gameLogic);
		~GameLogic();
		GameLogic& operator = (const GameLogic &gameLogic);

		void resetGame();

		/*void update(std::shared_ptr<Configuration> config, std::shared_ptr<NodeManager> nodes,
				std::shared_ptr<GraphicSystem> graphic, PhysicSystem* physic,
				std::shared_ptr<ActionsManager> actions, std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> entities,
				Ogre::Real deltaTime);*/

		void setValue(const std::string & name, int value, bool relative);
		int getValue(const std::string & name);

	private:
		std::map<std::string, int> _storyVariables;
		std::shared_ptr<Configuration> _configuration;

};

#endif /* GAME_LOGIC_H */
