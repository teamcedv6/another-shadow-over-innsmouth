/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <memory>
#include <iostream>

#include "XmlEditor.h"

// Options for Game
class Configuration{

	public:

		Configuration();
		Configuration(const Configuration & configuration);
		Configuration(const Configuration && configuration);
		~Configuration();
		Configuration& operator = (const Configuration &configuration);

		void loadConfig();
		void writeConfig();

		// Config Options
		typedef struct Physic {
			bool debug;
			float gravity;
			float worldBounds;
			float bodyRestitution;
			float bodyFriction;
			float bodyMass;
		} Physic_t;

		// Config Options
		typedef struct Ai {
			bool debug;
			bool singleNavMesh;
			std::string player;
		} Ai_t;

		// Items Options
		typedef struct Item {
			std::string name;
			float bodyRestitution;
			float bodyFriction;
			float bodyMass;
		} Item_t;


		// Setters Options
		void setPhysicConfig(bool debug, float gravity, float worldBounds, float bodyRestitution, float bodyFriction, float bodyMass);
		void setMusic(std::string key, std::string value);
		void setFx(std::string key, std::string value);
		void setCustom(int key, std::shared_ptr<Item_t> value);
		void setMouseGrab(bool active);
		void setAiConfig(bool debug, bool singleNavMesh, std::string player);


		// Getters
		std::shared_ptr<Physic_t> getPhysicConfig();
		std::shared_ptr<std::map<std::string, std::string>> getMusicList();
		std::shared_ptr<std::map<std::string, std::string>> getFxList();
		std::shared_ptr<std::map<int, std::shared_ptr<Item_t>>> getCustomList();
		std::string getMouseGrab();
		std::shared_ptr<Ai_t> getAiConfig();


		int getKeyRandomName();

	private:

		bool _proccessConfig(TiXmlElement* rootXml);
		void _proccessPhysic(TiXmlElement* physic);
		void _proccessAi(TiXmlElement* ai);
		void _proccessSound(TiXmlElement* sound);
		void _proccessCustom(TiXmlElement* custom);
		void _proccessGameLogic(TiXmlElement* gameLogic);
		void _getAttributes(std::shared_ptr<Item_t> item, TiXmlElement* element);

		std::shared_ptr<Physic_t> _physicConfig;
		std::shared_ptr<Ai_t> _aiConfig;
		std::shared_ptr<std::map<std::string, std::string>> _musicList;
		std::shared_ptr<std::map<std::string, std::string>> _fxList;
		std::shared_ptr<std::map<int, std::shared_ptr<Item_t>>> _customList;

		std::string _mouseGrab;
		int _keyRandomName;


};
#endif /* CONFIGURATION_H */
