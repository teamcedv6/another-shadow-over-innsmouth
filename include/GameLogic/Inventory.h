/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>
#include <string>
#include <memory>
#include <map>
#include <algorithm>


// Game Logic
class Inventory{

	public:

		typedef struct InventoryObject {
			std::string name;
			std::string picture;
		} mInventoryObject;

		Inventory();
		Inventory(int capacity);
		Inventory(const Inventory & inventory);
		Inventory(const Inventory && inventory);
		~Inventory();
		Inventory& operator = (const Inventory &inventory);

		void createObject(const std::string & name, const std::string & picture);
		bool pickObject(const std::string & name);
		void dropObject(const std::string & name);
		bool hasObject(const std::string & name);
		std::shared_ptr<mInventoryObject> getObject(const std::string & name);
		std::shared_ptr<std::vector<std::shared_ptr<mInventoryObject>>> getAllObjects();

	private:
		std::shared_ptr<std::vector<std::shared_ptr<mInventoryObject>>> _objects;
		std::map<std::string, std::shared_ptr<mInventoryObject>> _possibleObjects;
		int _maxCapacity;
		int _usedCapacity;

		bool _compareObjects(const std::shared_ptr<mInventoryObject> & object, const std::string & name);
};

#endif /* INVENTORY_H */
