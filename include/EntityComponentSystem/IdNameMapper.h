/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef IDNAMEMAPPER_H_
#define IDNAMEMAPPER_H_

#include <string>
#include <map>
#include <cassert>

/**
 * Clase para crear y mantener equivalencias entre ids y nombres.
 */
class IdNameMapper {

public:
	IdNameMapper();
	IdNameMapper(const IdNameMapper & mapper);
	IdNameMapper(IdNameMapper && mapper);
	~IdNameMapper();

	IdNameMapper & operator=(const IdNameMapper & mapper);

	const int createId(const std::string & name);
	// Devuelve -1 si no existe el nombre
	const int nameToId(const std::string & name);
	// Devuelve * si no existe el id (por lo que el nombre * no esta permitido
	const std::string & idToName(const int id);
	void removeId(const int id);

	private:
	int _maxIds;
	int _allocatedIds;
	int _nextId;
	std::map<const int, const std::string> _idNameMap;
	std::map<const std::string, const int> _nameIdMap;
};

#endif /* END IDNAMEMAPPER_H_ */
