/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef ENTITY_H
#define ENTITY_H

#include "IdNameMapper.h"
#include <string>
#include <cassert>
#include <memory>

class Entity {

public:
	typedef const unsigned int ID;

	Entity();
	Entity(const std::string & name);
	Entity(const Entity & entity);
	Entity(Entity && entity);
	~Entity();

	Entity & operator=(const Entity & entity);

	const Entity::ID getId();
	const std::string & getName();

	static void setMapper(std::shared_ptr<IdNameMapper> mapper);

	private:
	static std::shared_ptr<IdNameMapper> _mapper;

	const std::string _name;
	const Entity::ID _id;

};

#endif /* END ENTITY_H */
