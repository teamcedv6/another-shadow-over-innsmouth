/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CAMERACOMPONENT_H_
#define CAMERACOMPONENT_H_

#include "Ogre.h"

class CameraComponent {

	friend class CameraSystem;

public:
	CameraComponent();
	CameraComponent(const std::string & name, Ogre::SceneNode * node);
	CameraComponent(const CameraComponent & camera);
	CameraComponent(const CameraComponent && camera);
	~CameraComponent();

	static void setSceneManager(std::shared_ptr<Ogre::SceneManager> sceneManager);

	CameraComponent & operator= (const CameraComponent & camera);

private:
	std::shared_ptr<Ogre::Camera> _camera;
	static std::shared_ptr<Ogre::SceneManager> _sceneManager;
};


#endif /* CAMERACOMPONENT_H_ */
