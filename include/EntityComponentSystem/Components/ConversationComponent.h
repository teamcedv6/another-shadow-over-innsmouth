/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CONVERSATION_COMPONENT_H_
#define CONVERSATION_COMPONENT_H_

#include <memory>
#include "Conversation.h"

class ConversationComponent {

	friend class ConversationSystem;

public:
	ConversationComponent();
	ConversationComponent(std::shared_ptr<Conversation> conversation);
	ConversationComponent(const ConversationComponent & conversationComponent);
	ConversationComponent(const ConversationComponent && conversationComponent);
	~ConversationComponent();

	ConversationComponent & operator= (const ConversationComponent & conversationComponent);

private:
	std::shared_ptr<Conversation> _conversation;
};


#endif /* CONVERSATION_COMPONENT_H_ */
