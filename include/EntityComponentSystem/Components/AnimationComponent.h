/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef ANIMATION_COMPONENT_H
#define ANIMATION_COMPONENT_H

#include "Ogre.h"

class AnimationComponent  {

	friend class AnimationSystem;
	
	public:
		AnimationComponent(Ogre::Entity* entity);
		AnimationComponent(const AnimationComponent & animation);
		AnimationComponent(const AnimationComponent && animation);
		~AnimationComponent();

		AnimationComponent & operator= (const AnimationComponent & animation);

		bool _blend;
		bool _loop;
		Ogre::Real _duration;
		Ogre::Real _timeLeft;
		bool _complete;

	private:
		Ogre::Entity * _entity;
		Ogre::AnimationState *_source;
		Ogre::AnimationState * _target;
};


#endif /* ANIMATION_COMPONENT_H */
