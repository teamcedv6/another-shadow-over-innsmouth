/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef UICOMPONENT_H_
#define UICOMPONENT_H_

#include "Ogre.h"

class UserInterfaceComponent {

	friend class UserInterfaceSystem;

public:
	UserInterfaceComponent();
	UserInterfaceComponent(const std::string & xaml);
	UserInterfaceComponent(const UserInterfaceComponent & userInterface);
	UserInterfaceComponent(const UserInterfaceComponent && userInterface);
	~UserInterfaceComponent();

	UserInterfaceComponent & operator= (const UserInterfaceComponent & userInterface);

private:
	const std::string & _xaml;

};


#endif /* UserInterfaceCOMPONENT_H_ */
