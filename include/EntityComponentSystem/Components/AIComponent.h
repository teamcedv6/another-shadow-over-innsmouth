/*********************************************************************
 * Basic Game Structure - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef AI_COMPONENT_H
#define AI_COMPONENT_H

#include "OgreDetourCrowd.h"

class AIComponent {

	friend class AISystem;

public:

	enum State {
		IDLE,
		RANDOM_PATROL,
		PATROL,
		HUNT,
		GOTO
	};

	AIComponent(Ogre::Vector3 position);
	AIComponent(const AIComponent & aiComponent);
	AIComponent(const AIComponent && aiComponent);
	~AIComponent();

	AIComponent & operator= (const AIComponent & aiComponent);

	int getId();
	const dtCrowdAgent* getAgent();
	const std::string getName();
	Ogre::Vector3 getInitPosition();
	Ogre::Vector3 getInitRandomPosition();
	std::vector<Ogre::Vector3> getPatrolPositions();
	Ogre::Vector3 getNextPosition();
	AIComponent::State getActualState();
	std::shared_ptr<Ogre::Camera> getEyes();
	Ogre::Vector3 getLastView();


	void setId(int agentId);
	void setAgent(const dtCrowdAgent* agent);
	void setName(std::string name);
	void setInitPosition(Ogre::Vector3 position);
	void setInitRandomPosition(Ogre::Vector3 position);
	void setPatrolPositions(Ogre::Vector3 patrolPositions);
	void setNextPosition(Ogre::Vector3 nextPosition);
	void setActualState(AIComponent::State newState);
	void setEyes(std::shared_ptr<Ogre::Camera>);
	void setLastView(Ogre::Vector3 position);



private:

    int _agentID;
    const dtCrowdAgent* _agent;
    Ogre::Vector3 _initPosition;
    Ogre::Vector3 _initRandomPosition;
    std::vector<Ogre::Vector3> _patrolPoints;
    Ogre::Vector3 _nextPosition;
    AIComponent::State _actualState;
    std::shared_ptr<Ogre::Camera> _eyes;
    Ogre::Vector3 _lastView;

};


#endif /* AI_COMPONENT_H */
