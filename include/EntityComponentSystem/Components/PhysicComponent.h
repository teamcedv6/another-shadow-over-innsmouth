/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef PHYSICCOMPONENT_H_
#define PHYSICCOMPONENT_H_

#include <OgreBulletDynamicsRigidBody.h>


class PhysicComponent  {

	friend class PhysicSystem;
	
public:

	struct PhysicProperties {
		float bodyRestitution;
		float bodyFriction;
		float bodyMass;
		std::string shapeId;
	};

	PhysicComponent();
	PhysicComponent(const PhysicComponent & physics);
	PhysicComponent(const PhysicComponent && physics);
	~PhysicComponent();

	PhysicComponent & operator= (const PhysicComponent & physics);

private:
		std::shared_ptr<OgreBulletDynamics::RigidBody> _rigidBody;
};


#endif /* PHYSICCOMPONENT_H_ */
