/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef GRAPHICCOMPONENT_H_
#define GRAPHICCOMPONENT_H_

#include "Ogre.h"
#include <map>
#include <memory>

class GraphicComponent {

	friend class GraphicSystem;

public:
	GraphicComponent();
	GraphicComponent(const GraphicComponent & graphics);
	GraphicComponent(const GraphicComponent && graphics);
	~GraphicComponent();

	GraphicComponent & operator= (const GraphicComponent & graphics);

	void addGraphic(const std::string & name, const std::string & mesh, Ogre::SceneNode * node, bool castShadows);
	void addStaticGraphic(const std::string & name, const std::string & mesh, bool castShadows);
	void removeGraphic(const std::string & name);

	static void setSceneManager(std::shared_ptr<Ogre::SceneManager> sceneManager);

private:
	std::map<const std::string, Ogre::Entity *> _entities;
	static std::shared_ptr<Ogre::SceneManager> _sceneManager;
};


#endif /* GRAPHICCOMPONENT_H_ */
