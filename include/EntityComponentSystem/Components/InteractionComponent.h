/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef INTERACTION_COMPONENT_H_
#define INTERACTION_COMPONENT_H_

#include <memory>
#include <Ogre.h>

class InteractionComponent {

	friend class InteractionSystem;

public:

	enum InteractionType {
		PERSON_TALKABLE,
		PERSON_NON_TALKABLE,
		OBJECT_PICKABLE,
		OBJECT_NON_PICKABLE
	};

	InteractionComponent();
	InteractionComponent(InteractionType type, Ogre::SceneNode * node, float activateDistance, float interactionDistance);
	InteractionComponent(const InteractionComponent & interactionComponent);
	InteractionComponent(const InteractionComponent && interactionComponent);
	~InteractionComponent();

	InteractionComponent & operator= (const InteractionComponent & interactionComponent);

private:
	InteractionType _type;
	// SceneNode (allows changes) or vector3 (not necesarily a node)
	Ogre::SceneNode * _node;
	float _activateDistance;
	float _interactionDistance;
};


#endif /* INTERACTION_COMPONENT_H_ */
