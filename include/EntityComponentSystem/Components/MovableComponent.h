/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef MOVABLECOMPONENT_H_
#define MOVABLECOMPONENT_H_

#include "Ogre.h"

class MovableComponent {

	friend class MovementSystem;

public:
	MovableComponent();
	MovableComponent(const MovableComponent & movable);
	MovableComponent(const MovableComponent && movable);
	~MovableComponent();

	MovableComponent & operator= (const MovableComponent & graphics);

	void addMovablePart(const std::string & name, Ogre::SceneNode * node, bool isPhysicalManaged);
	void deleteMovablePart(const std::string & part);

private:
	struct MovementParameters {
		Ogre::SceneNode * _node;
		Ogre::Vector3 _position;
		Ogre::Quaternion _rotation;
		bool _physicalMovement;
	};

	std::map<const std::string, MovementParameters> _movableParts;

};


#endif /* MOVABLECOMPONENT_H_ */
