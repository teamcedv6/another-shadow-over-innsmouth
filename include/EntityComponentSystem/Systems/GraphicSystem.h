/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef GRAPHICSYSTEM_H_
#define GRAPHICSYSTEM_H_

#include "System.h"
#include "Entity.h"
#include "GraphicComponent.h"

#include "Ogre.h"

class GraphicSystem : public System<GraphicComponent>{
public:
	GraphicSystem();
	GraphicSystem(std::shared_ptr<Ogre::SceneManager> sceneManager);
	GraphicSystem(const GraphicSystem & graphicSystem);
	GraphicSystem(const GraphicSystem && graphicSystem);
	~GraphicSystem();

	GraphicSystem & operator= (const GraphicSystem & graphicSystem);
	GraphicSystem & operator= (const GraphicSystem && graphicSystem);

private:
	std::shared_ptr<Ogre::SceneManager> _sceneManager;
};


#endif /* GRAHPICSYSTEM_H_ */
