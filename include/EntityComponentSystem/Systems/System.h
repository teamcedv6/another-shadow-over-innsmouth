/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include "Entity.h"
#include <memory>
#include <vector>
#include <map>

template<typename T> class System {
public:
	System() {};
	System(const System<T> & system) {};
	System(const System<T> && system) {};
	virtual ~System() {};

	System<T> & operator=(const System<T> & system) { return *this;};

	void addComponent(Entity::ID id, std::shared_ptr<T> & component) { _componentMap[id] = component; };
	std::shared_ptr<T> getComponent(Entity::ID id)  { return _componentMap[id]; /* ¡Cuidado!, si no existe devolverá un componente nuevo*/ };
	void removeComponent(Entity::ID id) { auto it = _componentMap.find(id); if(it != _componentMap.end()) _componentMap.erase(it);};

protected:
	std::map<Entity::ID, std::shared_ptr<T>> _componentMap;
};

#endif /* SYSTEM_H_ */
