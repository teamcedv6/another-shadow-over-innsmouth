/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef INTERACTION_SYSTEM_H_
#define INTERACTION_SYSTEM_H_

#include <vector>
#include "InteractionComponent.h"
#include "UserInterfaceSystem.h"
#include "ConversationSystem.h"
#include "CameraSystem.h"
#include <Ogre.h>
#include "InventoryState.h"
#include "IdNameMapper.h"
#include "DeleteEntity.h"
#include "InventoryChange.h"

#include "pch.h"

using namespace std;


class InteractionSystem : public System<InteractionComponent>{
public:

	InteractionSystem();
	InteractionSystem(const std::shared_ptr<GameManager> gameManager, const std::shared_ptr<UserInterfaceSystem> userInterface, const std::shared_ptr<CameraSystem> cameras, bool showInteractionOptions);

	InteractionSystem(const InteractionSystem & interactionSystem);
	InteractionSystem(const InteractionSystem && interactionSystem);
	~InteractionSystem();

	InteractionSystem & operator= (const InteractionSystem & interactionSystem);

	void setMainCharacter(Ogre::SceneNode * character);
	void setInteractionOptionsVisibility(bool show);
	void update(Ogre::Real deltaTime);

	void action(int n);

	void talk(int id);
	void look(int id);
	void pick(int id);
	void use(int id);
	void give(int id);

	void resume();
	//void use(int id, std::shared_ptr<Inventory::InventoryObject> object);
	//void give(int id, std::shared_ptr<Inventory::InventoryObject> object);

	void startInterface();
	void clearInterface();

	std::shared_ptr<ConversationSystem> conversations;

private:
	Ogre::SceneNode * _mainCharacter;
	std::shared_ptr<InteractionComponent> _active;
	int _id;

	const std::shared_ptr<UserInterfaceSystem> _ui;
	const std::shared_ptr<CameraSystem> _cameras;
	const std::shared_ptr<ActionsManager> _actions;

	Noesis::UserControl * _rootUI;
	Noesis::Storyboard * _enterAnim;
	Noesis::Storyboard * _exitAnim;

	bool _showInteractionOptions;
	Ogre::Real _elapsedTime;

	std::string _uiName = "GameUI";
	Ogre::Real _waiting = 0;

	std::function<void(int)> _interactionsF[4];
	bool _interactionsActives[4];

	std::function<void(int)> _talkF;
	std::function<void(int)> _lookF;
	std::function<void(int)> _pickF;
	std::function<void(int)> _useF;
	std::function<void(int)> _giveF;

	const std::shared_ptr<GameManager> _gameManager;
	bool _givingObject;
	bool _usingObject;
	int _interactingId;
};

#endif /* END INTERACTION_SYSTEM_H_ */
