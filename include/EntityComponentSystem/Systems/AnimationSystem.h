/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef ANIMATION_SYSTEM_H
#define ANIMATION_SYSTEM_H

#include "System.h"
#include "Entity.h"
#include "AnimationComponent.h"

#include "Ogre.h"

class  AnimationSystem : public System<AnimationComponent>{

	public:

		enum AnimationType : unsigned char {
			IDLE,
			WALK,
			RUN,
			CRAWL,
			JUMP,
			USE
		};

		enum AnimationTransition {
			SWITCH,
			BLEND
		};

		AnimationSystem();
		AnimationSystem(std::shared_ptr<Ogre::SceneManager> sceneManager, std::shared_ptr<IdNameMapper> mapper);
		AnimationSystem(const AnimationSystem & animationSystem);
		AnimationSystem(const AnimationSystem && animationSystem);
		~AnimationSystem();

		AnimationSystem & operator= (const AnimationSystem & animationSystem);

		void runAnimation(const Entity::ID id, AnimationType animationType,Ogre::Real duration, bool blend, bool loop);
		void update(Ogre::Real deltaTime);
		bool isRunning(const Entity::ID id);
		std::string getNameAnimation(AnimationType animationType);

	private:

		std::shared_ptr<Ogre::SceneManager> _sceneManager;
		std::shared_ptr<IdNameMapper> _mapper;
};

#endif /* ANIMATION_SYSTEM_H */

