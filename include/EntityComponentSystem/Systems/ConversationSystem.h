/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CONVERSATION_SYSTEM_H_
#define CONVERSATION_SYSTEM_H_

#include <vector>
#include "Conversation.h"
#include "ConversationComponent.h"
#include <OIS.h>
#include "UserInterfaceSystem.h"
#include "InputBindings.h"
#include "GameManager.h"
#include "ConversationNode.h"
#include "GameState.h"
#include "ConversationNode.h"

#include "pch.h"

using namespace std;

//Forward declaration
//class GameState
class ConversationLoader;

class ConversationSystem : public System<ConversationComponent>{
public:

	ConversationSystem();
	ConversationSystem(const std::shared_ptr<UserInterfaceSystem> userInterface, const std::shared_ptr<GameManager> gameManager);

	ConversationSystem(const ConversationSystem & conversationSystem);
	ConversationSystem(const ConversationSystem && conversationSystem);
	~ConversationSystem();

	ConversationSystem & operator= (const ConversationSystem & conversationSystem);

	void up();
	void down();
	void push();
	void back();


	void startConversation();
	void finishConversation();
	void setConversation(std::shared_ptr<Conversation> conversation);
	void setConversation(Entity::ID id);
	void loadConversation(const std::string & conversationName);

private:

	std::shared_ptr<Conversation> _conversation;
	const std::shared_ptr<GameManager> _gameManager;
	const std::shared_ptr<UserInterfaceSystem> _ui;
	ConversationNode::NodeType _currentNodeType;
	std::string _nodeUIName;

	void _updateUI();
	void _updateNextNode();

};
#endif /* END CONVERSATION_SYSTEM_H_ */
