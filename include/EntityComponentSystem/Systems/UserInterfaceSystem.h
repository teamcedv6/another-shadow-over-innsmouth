/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef UISYSTEM_H_
#define UISYSTEM_H_

#include "System.h"
#include "Entity.h"
#include "UserInterfaceComponent.h"
#include "OgreNsGuiBindings.h"
#include "pch.h"

#include <OIS.h>


class  UserInterfaceSystem : public System<UserInterfaceComponent>, public OIS::KeyListener, public OIS::MouseListener {
public:
	struct uiPart {
	    	Noesis::Ptr<Noesis::IRenderer> renderer;
	    	Noesis::Ptr<Noesis::Element> root;
	};

	UserInterfaceSystem();
	UserInterfaceSystem(std::shared_ptr<Ogre::RenderWindow> window);
	UserInterfaceSystem(const UserInterfaceSystem & userInterfaceSystem);
	UserInterfaceSystem(const UserInterfaceSystem && userInterfaceSystem);
	~UserInterfaceSystem();

	UserInterfaceSystem & operator= (const UserInterfaceSystem & userInterfaceSystem);

	void initializeUI();
	void terminateUI();

	void loadUI(const std::string & name, const std::string & xaml);
	void loadUI(const Entity::ID id);

	void unloadUI(const std::string & name);
	void unloadUI(const Entity::ID id);

	void update(Ogre::Real deltaTime);
	void frameRenderingQueued();

	void activateCursor();
	void deactivateCursor();

	std::shared_ptr<uiPart> getUI(const std::string & name);
	//void test(Noesis::BaseComponent * sender, const Noesis::RoutedEventArgs & args);

	//void test(Noesis::BaseComponent * sender, const Noesis::MouseEventArgs & args);

private:
	bool keyPressed (const OIS::KeyEvent &e);
	bool keyReleased (const OIS::KeyEvent &e);
	bool mouseMoved (const OIS::MouseEvent &e);
	bool mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
	bool mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    std::map<std::string, std::shared_ptr<uiPart>> _uiMap;
    std::shared_ptr<Ogre::RenderWindow> _window;
    double _time;

    Noesis::UserControl * _mouse = nullptr;

};



#endif /* UISYSTEM_H_ */
