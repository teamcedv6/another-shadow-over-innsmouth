/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef PHYSICSYSTEM_H_
#define PHYSICSYSTEM_H_

#include "System.h"
#include "Entity.h"
#include "Configuration.h"
#include "ActionsManager.h"
#include "SoundManager.h"
#include "NodeManager.h"
#include "GraphicSystem.h"
#include "PhysicComponent.h"

#include "PlayFx.h"

class  PhysicSystem : public System<PhysicComponent>{

	public:

		enum ShapeForm : unsigned char {
			BOUNDING_BOX,
			MESH,
			PLANE
		};

		enum ShapeType : unsigned char {
			STATIC,
			DYNAMIC
		};

		PhysicSystem();
		PhysicSystem(std::shared_ptr<Ogre::SceneManager> sceneManager, std::shared_ptr<IdNameMapper> mapper, std::shared_ptr<Configuration> config, std::shared_ptr<ActionsManager> actions, std::shared_ptr<SoundManager> soundManager,
				std::shared_ptr<NodeManager> nodes, std::shared_ptr<GraphicSystem> graphic, std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> entities);
		PhysicSystem(const PhysicSystem & physicSystem);
		PhysicSystem(const PhysicSystem && physicSystem);
		~PhysicSystem();

		PhysicSystem & operator= (const PhysicSystem & physicSystem);

		void addPhysicObject(const Entity::ID id, ShapeForm shapeForm, ShapeType shapeType, PhysicComponent::PhysicProperties objProperties);
		void deletePhysicObject(const Entity::ID id);

		void addImpulse(const Entity::ID id,Ogre::Vector3 vector);

		void activeObject(const Entity::ID id);

		void update(Ogre::Real deltaTime);
		void updateObject(const Entity::ID id,Ogre::Vector3 position);
		void checkCollisions();
		bool checkCollisions(const Entity::ID me,const Entity::ID object);

	private:

		void _createPhysicWorld();
		void _destroyPhysicWorld();

		void _activeDebug();

		void _newBoundingBoxObject(const std::string name, const std::string shapeId);
		void _newMesh2ShapeObject(const std::string name, const std::string shapeId);
		void _newStaticPlaneObject(const std::string shapeId);

		bool _checkEnvironmentCollission(const std::string & name1, const std::string & name2);
		bool _checkBulletCollission(const std::string & name1, const std::string & name2);

		std::shared_ptr<Ogre::SceneManager> _sceneManager;
		std::shared_ptr<SoundManager> _soundManager;
		std::shared_ptr<IdNameMapper> _mapper;
		std::shared_ptr<Configuration> _config;
		std::shared_ptr<ActionsManager> _actions;

		std::shared_ptr<NodeManager> _nodes;
		std::shared_ptr<GraphicSystem> _graphic;
		std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> _entities;

		std::shared_ptr<OgreBulletDynamics::DynamicsWorld> _physicWorld;
		std::map<std::string,std::shared_ptr<OgreBulletCollisions::CollisionShape>>  _collisionShapes;

		std::shared_ptr<OgreBulletCollisions::DebugDrawer> _debugDrawer;
		std::shared_ptr<Ogre::SceneNode> _debugNode;
		std::shared_ptr<PhysicSystem> _smartThis;
};

#endif /* PHYSICSYSTEM_H_ */

