/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CAMERASYSTEM_H_
#define CAMERASYSTEM_H_

#include "System.h"
#include "Entity.h"
#include "CameraComponent.h"

#include "Ogre.h"

// Forward declaration
class GameManager;

class CameraSystem : public System<CameraComponent> {


public:
	CameraSystem();
	CameraSystem(std::shared_ptr<GameManager> gameManager);
	CameraSystem(const CameraSystem & cameraSystem);
	CameraSystem(const CameraSystem && cameraSystem);
	~CameraSystem();

	CameraSystem & operator= (const CameraSystem & cameraSystem);

	/**
	 * Inicializa el viewport estableciendo la camara inicial
	 */
	void initializeViewport(Entity::ID id);
	Ogre::Viewport * getActiveViewport();
	std::shared_ptr<Ogre::Camera> getActiveCamera();
	void setActiveCamera(Entity::ID id);
	void setFov(Entity::ID id, Ogre::Degree fov);
	void setProjectionType(Entity::ID id, Ogre::ProjectionType projection);
	void setNearDist(Entity::ID id, Ogre::Real nearDist);
	void setFarDist(Entity::ID id, Ogre::Real farDist);

private:
	std::shared_ptr<GameManager> _gameManager;
	std::shared_ptr<Ogre::Camera> _activeCamera;
	std::shared_ptr<Ogre::SceneManager> _sceneManager;
	std::shared_ptr<Ogre::Viewport> _viewport;
};



#endif /* CAMERASYSTEM_H_ */
