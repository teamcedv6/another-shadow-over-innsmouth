/*********************************************************************
 * Basic Game Structure - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef AI_SYSTEM_H
#define AI_SYSTEM_H

#include "System.h"
#include "Entity.h"
#include "AIComponent.h"

#include "OgreRecast.h"
#include "OgreDetourCrowd.h"
#include "OgreDetourTileCache.h"


#include "Ogre.h"
#include "Configuration.h"
#include "MovementSystem.h"
#include "AnimationSystem.h"

class AISystem : public System<AIComponent> {


public:

	AISystem();
	AISystem(std::shared_ptr<Ogre::SceneManager> sceneManager,std::shared_ptr<Configuration> config, std::shared_ptr<MovementSystem> _movementSystem,
			std::shared_ptr<IdNameMapper> _mapper, std::shared_ptr<NodeManager> nodes
			, std::shared_ptr<AnimationSystem> animSystem, std::shared_ptr<SoundManager> soundManager);
	AISystem(const AISystem & aiSystem);
	AISystem(const AISystem && aiSystem);
	~AISystem();

	std::shared_ptr<OgreRecast> getRecast();
	std::shared_ptr<OgreDetourCrowd> getCrowd();
	std::shared_ptr<OgreDetourTileCache> getTileCache();

	void changeDebug(bool debug);

	void setIdleAgent(const Entity::ID);
	void setRandomAgent(const Entity::ID);
	void setPatrolAgent(const Entity::ID);
	void setHuntAgent(const Entity::ID);
	void setGotoAgent(const Entity::ID, Ogre::Vector3 newPosition);

	void setAllState(AIComponent::State state, Ogre::Vector3 position);
	void huntMode(bool hunt);

	void resetEnd(bool andGame);

	void createOgreCrowd(std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> navMesh,std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> obstacles);
	void createAgents();
	void deleteAgents();
	void updateNavMesh(std::string obstacleName);
	void update(Ogre::Real deltaTime);

	AISystem & operator= (const AISystem & aiSystem);

private:

	bool _debug;
	bool _singleNavMesh;
	Ogre::Real _tickTime;
	std::string _player;

	std::shared_ptr<OgreRecast> _recast;
	std::shared_ptr<OgreDetourCrowd> _crowd;
	std::shared_ptr<OgreDetourTileCache> _tileCache;

    std::vector<Ogre::Entity*> _navMesh;
    std::map<std::string,std::tuple<std::shared_ptr<ConvexVolume>,bool>> _obstacles;

	std::shared_ptr<Configuration> _config;
	std::shared_ptr<Ogre::SceneManager> _sceneManager;
	std::shared_ptr<MovementSystem> _movementSystem;
	std::shared_ptr<IdNameMapper> _mapper;
	std::shared_ptr<NodeManager> _nodes;
	std::shared_ptr<AnimationSystem> _animSystem;
	std::shared_ptr<SoundManager> _soundManager;

	void _configOptions();
	int  _createAgent(const Entity::ID entityId);
	void _calculateAndDrawPath(Ogre::Vector3 beginPos, Ogre::Vector3 endPos, int pathNb, int targetId);
	void _aiTick();
	void _setDestinationForAllAgents(Ogre::Vector3 destination, bool adjustExistingPath);
	void _setDestinationForAgentId(Entity::ID entityId, Ogre::Vector3 destination, bool adjustExistingPath);

	void _doIdle(const Entity::ID id);
	void _doRandomPatrol(const Entity::ID id);
	void _doPatrol(const Entity::ID id);
	void _doHunt(const Entity::ID id);
	void _doGoTo(const Entity::ID id);

	void _checkHunt(const Entity::ID id);


	bool _activeHunt;
};



#endif /* AI_SYSTEM_H */
