/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef MOVEMENTSYSTEM_H_
#define MOVEMENTSYSTEM_H_

#include "System.h"
#include "Entity.h"
#include "MovableComponent.h"
#include "PhysicSystem.h"

class  MovementSystem : public System<MovableComponent>{
public:
	MovementSystem();
	MovementSystem(const MovementSystem & movementSystem);
	MovementSystem(const MovementSystem && movementSystem);
	~MovementSystem();

	MovementSystem & operator= (const MovementSystem & graphicSystem);

	void setPhysicsSystem(std::shared_ptr<PhysicSystem> physics);

	void setPosition(const Entity::ID id, const std::string & name, const Ogre::Vector3 & position);
	void move(const Entity::ID id, const std::string & name, const Ogre::Vector3 & move);
	void setOrientation(const Entity::ID id, const std::string & name, const Ogre::Quaternion & orientation);

	void update(Ogre::Real deltaTime);

private:
	std::shared_ptr<PhysicSystem> _physics;
};



#endif /* MOVEMENTSYSTEM_H_ */
