/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include <stack>
#include <map>
#include <string>

#include <Ogre.h>
#include <OgreSingleton.h>
#include <OIS.h>

#include "Configuration.h"
#include "GameLogic.h"

#include "InputManager.h"

#include "NodeManager.h"
#include "ActionsManager.h"
#include "GraphicSystem.h"
#include "CameraSystem.h"
#include "PhysicSystem.h"
#include "AnimationSystem.h"
#include "MovementSystem.h"
#include "UserInterfaceSystem.h"
#include "AISystem.h"

#include "Inventory.h"

#include "SoundManager.h"
#include "ActionNode.h"

using namespace std;

class GameState;
class ConversationSystem;
class InteractionSystem;
class ConversationLoader;
class EntityExtraLoader;
class EventFactory;

class GameManager : public Ogre::FrameListener, public OIS::KeyListener, public OIS::MouseListener, public OIS::JoyStickListener {

	public:

		GameManager():_root(0){}
		~GameManager ();

		// Init
		void start (shared_ptr<GameState> state);

		// States changes
		void changeState (shared_ptr<GameState> state);
		void pushState (shared_ptr<GameState> state);
		void popState ();

		// Get/Set Unique States
		void addNewState(int id, shared_ptr<GameState> state);
		shared_ptr<GameState> getState(int id);

		// Getters
		shared_ptr<Ogre::Root> getRoot();
		shared_ptr<Ogre::SceneManager> getSceneManager();
		shared_ptr<Ogre::RenderWindow> getRenderWindow();
		shared_ptr<Ogre::Viewport> getViewport();
		shared_ptr<Ogre::Camera> getCamera();

		shared_ptr<NodeManager> getNodeManager();
		shared_ptr<ActionsManager> getActionsManager();
		shared_ptr<SoundManager> getSoundManager();
		shared_ptr<ConversationSystem> getConversationSystem();

		shared_ptr<Configuration> getConfig();

		std::shared_ptr<ConversationLoader> getConversationLoader();
		std::shared_ptr<EntityExtraLoader> getEntityExtraLoader();

		shared_ptr<GraphicSystem> getGraphicsSystem();
		shared_ptr<PhysicSystem> getPhysicsSystem();
		shared_ptr<AnimationSystem> getAnimationSystem();
		shared_ptr<CameraSystem> getCameraSystem();
		shared_ptr<MovementSystem> getMovementSystem();
		std::shared_ptr<IdNameMapper> getMapper();
		shared_ptr<UserInterfaceSystem> getUserInterfaceSystem();
		shared_ptr<InteractionSystem> getInteractionSystem();
		shared_ptr<AISystem> getAISystem();
		shared_ptr<GameLogic> getGameLogic();
		shared_ptr<Inventory> getInventory();
		shared_ptr<EventFactory> getEventFactory();





		void setEntity(const std::string & name, std::shared_ptr<Entity> entity){(*_entities)[name] = entity;}
		void setNavMeshEntity(const std::string & name, std::shared_ptr<Entity> entity){(*_navMeshEntities)[name] = entity;}
		void setNavObstacleEntity(const std::string & name, std::shared_ptr<Entity> entity){(*_navObstacleEntities)[name] = entity;}
		std::shared_ptr<Entity> getEntity(const std::string & name) { return (*_entities)[name]; }
		void deleteEntity(const std::string & name) {}
		std::shared_ptr<Entity> getNavMeshEntity(const std::string & name) { return (*_navMeshEntities)[name]; }
		std::shared_ptr<Entity> getNavObstacleEntity(const std::string & name) { return (*_navObstacleEntities)[name]; }

		enum navType {
			NAV_MESH,
			OBSTACLES
		};
		
		shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> getNavEntities(navType type) {

			switch (type) {
				case NAV_MESH:
					return _navMeshEntities;
					break;
				case OBSTACLES:
					return _navObstacleEntities;
					break;
				default:
					return _navMeshEntities;
					break;
			}
		}		
		
		shared_ptr<InputManager> getInputManager(){return _inputManager;}

		// Setters
		void setCamera(shared_ptr<Ogre::Camera> camera);
		void setViewPort(shared_ptr<Ogre::Viewport> viewport);
		shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> _entities;
		shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> _navMeshEntities;
		shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> _navObstacleEntities;
		void setGameLogic(shared_ptr<GameLogic> gameLogic);


	protected:

		// Ogre Objects
		shared_ptr<Ogre::Root> _root;
		shared_ptr<Ogre::SceneManager> _sceneManager;
		shared_ptr<Ogre::RenderWindow> _renderWindow;
		shared_ptr<Ogre::Viewport> _viewport;
		shared_ptr<Ogre::Camera> _camera;

		// Ogre Config
		void loadResources ();
		bool configure ();

		// Game Loop
		bool frameStarted (const Ogre::FrameEvent& evt) override;
		bool frameEnded (const Ogre::FrameEvent& evt) override;
		bool frameRenderingQueued(const Ogre::FrameEvent & evt) override;

	private:

		// OIS listeners
		bool keyPressed (const OIS::KeyEvent &e);
		bool keyReleased (const OIS::KeyEvent &e);
		bool mouseMoved (const OIS::MouseEvent &e);
		bool mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
		bool mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
		bool povMoved( const OIS::JoyStickEvent &e, int pov );
		bool axisMoved( const OIS::JoyStickEvent &e, int axis );
		bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
		bool buttonPressed( const OIS::JoyStickEvent &e, int button );
		bool buttonReleased( const OIS::JoyStickEvent &e, int button );

		// Input Manager
		shared_ptr<InputManager> _inputManager;

		// Game States - Stack States and Map Unique Instace States
		std::stack<shared_ptr<GameState>> _states;
		std::map<int,shared_ptr<GameState>> _existingStates;

		// Config
		std::shared_ptr<Configuration> _config;
		
		// Loaders
		std::shared_ptr<ConversationLoader> _conversationLoader;
		std::shared_ptr<EntityExtraLoader> _entityExtraLoader;
		//std::shared_ptr<InventoryLoader> _inventoryLoader;

		// GameLogic
		std::shared_ptr<GameLogic> _gameLogic;
		std::shared_ptr<Inventory> _inventory;
		std::shared_ptr<EventFactory> _eventFactory;

		// Id mapper
		std::shared_ptr<IdNameMapper> _idMapper;

		// Systems that the game need
		std::shared_ptr<NodeManager> _nodes;
		std::shared_ptr<ActionsManager> _actions;
		std::shared_ptr<SoundManager> _sound;

		std::shared_ptr<GraphicSystem> _graphics;
		std::shared_ptr<PhysicSystem> _physics;
		std::shared_ptr<AnimationSystem> _animations;
		std::shared_ptr<CameraSystem> _cameras;
		std::shared_ptr<MovementSystem> _movement;
		std::shared_ptr<UserInterfaceSystem> _ui;
		std::shared_ptr<ConversationSystem> _conversations;
		std::shared_ptr<InteractionSystem> _interactions;
		std::shared_ptr<AISystem> _ai;

};

#endif /* END GAME_MANAGER_H */
