/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef SOUND_SYSTEM_H
#define SOUND_SYSTEM_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include <map>
#include <string>

#include "TrackManager.h"
#include "SoundFXManager.h"

#include "Configuration.h"

class SoundManager{

	public:

		enum TYPE {
			MUSIC,
			FX
		};

		enum ACTION {
			PLAY,
			STOP
		};

		SoundManager();
		SoundManager(const SoundManager & soundManager);
		SoundManager(const SoundManager && soundManager);
		~SoundManager();
		SoundManager& operator = (const SoundManager &soundManager);

		void start(std::shared_ptr<Configuration> configuration);
		void selectResource(TYPE type, std::string key, ACTION action, int volume,int loop);


	private:

		// Init SDL
		bool _initSDL();

		// Load resources
		void _loadMusic(std::shared_ptr<Configuration> configuration);
		void _loadFx(std::shared_ptr<Configuration> configuration);

		// Sounds Manager
		std::shared_ptr<TrackManager> _pTrackManager;
		std::shared_ptr<SoundFXManager> _pSoundFXManager;

		// Sounds & Fx
		std::shared_ptr< std::map<std::string,TrackPtr> > _musicList;
		std::shared_ptr< std::map<std::string, SoundFXPtr> >  _fxList;
};

#endif
