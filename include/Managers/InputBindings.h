/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef INPUT_BINDINGS_H
#define INPUT_BINDINGS_H

#include <Ogre.h>
#include <OIS.h>
#include <utility>

using namespace std;

class InputBindings {
	public:
		enum PushState : unsigned char {
			PUSH,
			RELEASE,
			PRESSED
		};

		enum JoystickEventType : unsigned char {
			POV,
			AXIS,
			SLIDER,
			BUTTON
		};

		InputBindings();
		InputBindings(const InputBindings & bindings);
		InputBindings(const InputBindings && bindings);
		virtual ~InputBindings ();

		InputBindings & operator= (const InputBindings & bindings);
		InputBindings & operator= (const InputBindings && bindings);

		void setHook(OIS::KeyCode code, function<void(PushState)> f);
		void setHook(OIS::MouseButtonID id, function<void(const OIS::MouseEvent &, PushState)> f);
		void setHook(JoystickEventType eventType, int id, function<void(int, const OIS::JoyStickEvent &, PushState)> f);
		void setHook(function<void(const OIS::MouseEvent &)> f);

		void executeHook(OIS::KeyCode code, PushState pushState);
		void executeHook(const OIS::MouseEvent & event, OIS::MouseButtonID id, PushState pushState);
		void executeHook(const OIS::JoyStickEvent & event, JoystickEventType eventType, int id, PushState pushState);
		void executeHook(const OIS::MouseEvent & event);
		//void executeHook(const OIS::Button button, 	)


		void removeHook(OIS::KeyCode code);
		void removeHook(OIS::MouseButtonID id);
		void removeHook(std::pair<JoystickEventType, int> eventType);
		void removeMouseHook();

		void clearHooks();

	private:
		std::map<OIS::KeyCode, function<void(PushState)>> _keyHooks;
		std::map<OIS::MouseButtonID, function<void(const OIS::MouseEvent &, PushState)>> _mouseButtonsHooks;
		std::map<std::pair<JoystickEventType, int>, function<void(int, const OIS::JoyStickEvent &, PushState)>> _joystickHooks;
		function<void(const OIS::MouseEvent &)> _mouseHook;

};
#endif /* END INPUT_BINDINGS_H */
