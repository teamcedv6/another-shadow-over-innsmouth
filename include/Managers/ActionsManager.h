/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef ACTIONS_MANAGER_H_
#define ACTIONS_MANAGER_H_

#include <vector>
#include "Ogre.h"

#include "Action.h"
#include "Trigger.h"
#include "DistanceTrigger.h"

using namespace std;

class GameState;

/**
 * Features added in order to act as Event Manager instead of ActionsManager (it supports action triggering and triggers evaluations
 */
class ActionsManager {
public:

	ActionsManager(std::shared_ptr<GameState> playState ): _playState(playState){}

	ActionsManager(const ActionsManager & actionsManager);
	ActionsManager(const ActionsManager && actionsManager);
	~ActionsManager ();

	ActionsManager & operator= (const ActionsManager & actionsManager);

	void addEvent(shared_ptr<Trigger> trigger, shared_ptr<Action> action);
	void addDistanceEvent(shared_ptr<DistanceTrigger> trigger, shared_ptr<Action> action);
	void addGiveEvent(std::string object, std::string person, shared_ptr<Trigger> trigger, shared_ptr<Action> action);
	void addUseEvent(std::string object, std::string target, shared_ptr<Trigger> trigger, shared_ptr<Action> action);
	void addAction(shared_ptr<Action> action);

	void clearActions();
	void clearEvents();
	void clearGiveEvents();
	void clearUseEvents();

	void update(Ogre::Real deltaTime);

	void positionUpdate();
	void giveObjectTo(std::string object, std::string person);
	void useObjectWith(std::string object, std::string target);


private:
	vector<shared_ptr<Action>> _actions;

	// Triggers
	vector<pair<shared_ptr<Trigger>, shared_ptr<Action>>> _events;
	vector<pair<shared_ptr<DistanceTrigger>, shared_ptr<Action>>> _distanceEvents;
	// Interaction events
	map<pair<string, string>, pair<shared_ptr<Trigger>, shared_ptr<Action>>> _useInteractions;
	map<pair<string, string>, pair<shared_ptr<Trigger>, shared_ptr<Action>>> _giveInteractions;
	// Collisions events
	//map<pair<string, string>, pair<shared_ptr<Trigger>, shared_ptr<Action>>> _collisionsInteractions;

	std::shared_ptr<GameState> _playState;

	void _updateActions(Ogre::Real deltaTime);
	void _updateEvents(Ogre::Real deltaTime);

	void _updateGeneralEvents();
	void _updateDistanceEvents();
};
#endif /* END ACTIONS_MANAGER_H_ */
