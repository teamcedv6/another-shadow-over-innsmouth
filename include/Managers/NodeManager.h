/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef NODE_MANAGER_H_
#define NODE_MANAGER_H_

#include <vector>
#include "Ogre.h"

class NodeManager {
public:

	NodeManager();
	NodeManager(std::shared_ptr<Ogre::SceneManager> sceneManager);
	NodeManager(const NodeManager & nodeManager);
	NodeManager(const NodeManager && nodeManager);
	~NodeManager ();

	NodeManager & operator= (const NodeManager & nodeManager);

	Ogre::SceneNode * createNode(const std::string & name);
	Ogre::SceneNode * getNode (const std::string & name);
	void attachNode(Ogre::SceneNode * node, const std::string & parentName);
	void dettachNode(Ogre::SceneNode * node);
	void deleteNode(Ogre::SceneNode * node, const bool recursive);

private:
	std::shared_ptr<Ogre::SceneManager> _sceneManager;

};
#endif /* END NODE_MANAGER_H_ */
