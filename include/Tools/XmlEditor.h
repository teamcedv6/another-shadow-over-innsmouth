/*********************************************************************
 * Another shadow over Innsmouth - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef XML_EDITOR_H
#define XML_EDITOR_H

#include <Ogre.h>
#include "tinyxml.h"

class XmlEditor {

	public:

		XmlEditor();
		XmlEditor(const XmlEditor & xmlEditor);
		XmlEditor(const XmlEditor && xmlEditor);
		~XmlEditor ();
		XmlEditor & operator= (const XmlEditor & xmlEditor);

		void loadDocument(const Ogre::String &documentName, const Ogre::String &groupName, const Ogre::String &rootDocument);
		void writeDocument(std::shared_ptr<TiXmlDocument> document,const Ogre::String &dir, const Ogre::String &documentName, bool back);

		bool isLoad();
		bool isWrite();

		std::shared_ptr<TiXmlDocument> getXmlDoc();

	private:
		std::shared_ptr<TiXmlDocument> _xmlDoc;
		bool _load;
		bool _write;

};

#endif /* End XML_EDITOR_H */
