#ifndef DOT_SCENELOADER_H
#define DOT_SCENELOADER_H

//http://www.ogre3d.org/tikiwiki/tiki-index.php?page=New+DotScene+Loader

// Includes
#include <OgreString.h>
#include <OgreVector3.h>
#include <OgreQuaternion.h>
#include <vector>

#include "Entity.h"
#include "GraphicComponent.h"
#include "PhysicComponent.h"
#include "MovableComponent.h"
#include "CameraComponent.h"

#include "GameManager.h"

#include "GraphicSystem.h"
#include "PhysicSystem.h"
#include "MovementSystem.h"
#include "CameraSystem.h"

// Forward declarations
class TiXmlElement;
class EntityExtraLoader;

namespace Ogre
{
	// Forward declarations
	class SceneManager;
	class SceneNode;

	class nodeProperty
	{
	public:
		String nodeName;
		String propertyNm;
		String valueName;
		String typeName;

		nodeProperty(const String &node, const String &propertyName, const String &value, const String &type)
			: nodeName(node), propertyNm(propertyName), valueName(value), typeName(type) {}
	};

	class DotSceneLoader
	{
	public:

		// This mean there's info to load in entity extra loader
		struct Extra {
			std::string name;
			Ogre::Vector3 p;
			Ogre::SceneNode * node;
		};

		DotSceneLoader() : mSceneMgr(0) {};
		DotSceneLoader(std::shared_ptr<GameManager> gameManager, std::shared_ptr<Configuration> config) : mSceneMgr(0), _gameManager(gameManager), _config(config) {
					mGraphicSystem = gameManager -> getGraphicsSystem();
					mPhysicsSystem = gameManager -> getPhysicsSystem();
					mMovementSystem = gameManager -> getMovementSystem();
					mCameraSystem = gameManager -> getCameraSystem();
					mAnimationSystem = gameManager -> getAnimationSystem();
					mAiSystem = gameManager->getAISystem();
					_extra = gameManager -> getEntityExtraLoader();
		};
		virtual ~DotSceneLoader() {};

		void parseDotScene(const String &SceneName, const String &groupName, std::shared_ptr<SceneManager> yourSceneMgr, SceneNode *pAttachNode = NULL, const String &sPrependNode = "" );

		String getProperty(const String &ndNm, const String &prop);

		std::vector<nodeProperty> nodeProperties;
		std::vector<String> staticObjects;
		std::vector<String> dynamicObjects;

	protected:
		void processScene(TiXmlElement *XMLRoot);

		void processNodes(TiXmlElement *XMLNode);
		void processExternals(TiXmlElement *XMLNode);
		void processEnvironment(TiXmlElement *XMLNode);
		void processTerrain(TiXmlElement *XMLNode);
		void processUserDataReference(TiXmlElement *XMLNode);
		void processOctree(TiXmlElement *XMLNode);
		void processLight(TiXmlElement *XMLNode, SceneNode *pParent = 0);
		void processCamera(TiXmlElement *XMLNode, SceneNode *pParent = 0);

		void processNode(TiXmlElement *XMLNode, SceneNode *pParent = 0);
		void processLookTarget(TiXmlElement *XMLNode, SceneNode *pParent);
		void processTrackTarget(TiXmlElement *XMLNode, SceneNode *pParent);
		void processEntity(TiXmlElement *XMLNode, SceneNode *pParent);
		void processParticleSystem(TiXmlElement *XMLNode, SceneNode *pParent);
		void processBillboardSet(TiXmlElement *XMLNode, SceneNode *pParent);
		void processPlane(TiXmlElement *XMLNode, SceneNode *pParent);

		void processFog(TiXmlElement *XMLNode);
		void processSkyBox(TiXmlElement *XMLNode);
		void processSkyDome(TiXmlElement *XMLNode);
		void processSkyPlane(TiXmlElement *XMLNode);
		void processClipping(TiXmlElement *XMLNode);

		void processLightRange(TiXmlElement *XMLNode, Light *pLight);
		void processLightAttenuation(TiXmlElement *XMLNode, Light *pLight);

		void processECS();

		String getAttrib(TiXmlElement *XMLNode, const String &parameter, const String &defaultValue = "");
		Real getAttribReal(TiXmlElement *XMLNode, const String &parameter, Real defaultValue = 0);
		bool getAttribBool(TiXmlElement *XMLNode, const String &parameter, bool defaultValue = false);

		Vector3 parseVector3(TiXmlElement *XMLNode);
		Quaternion parseQuaternion(TiXmlElement *XMLNode);
		ColourValue parseColour(TiXmlElement *XMLNode);


		std::shared_ptr<SceneManager> mSceneMgr;
		SceneNode *mAttachNode;
		String m_sGroupName;
		String m_sPrependNode;


		SceneNode * mNode;
		std::string mName;
		int mId;

		struct PhysicCompData {
				std::shared_ptr<PhysicComponent> mPhysic;
				PhysicSystem::ShapeForm mShapeForm;
				PhysicSystem::ShapeType mShapeType;
				PhysicComponent::PhysicProperties mPhysicProperties;
		};

		struct CameraCompData {
			std::shared_ptr<CameraComponent> mCamera;
			Ogre::Degree fov;
			Ogre::Real aspectRatio;
			Ogre::Real nearDist = -1;
			Ogre::Real farDist = -1;
			ProjectionType projection = PT_PERSPECTIVE;
		};

		struct EntityComp {
			std::shared_ptr<::Entity> mEntity;
			std::shared_ptr<GraphicComponent> mGraphic;
			std::shared_ptr<PhysicCompData> mPhysicData;
			std::shared_ptr<MovableComponent> mMovement;
			std::shared_ptr<CameraCompData> mCameraData;
			std::shared_ptr<string> mAnimationName;
			bool mAi;
			bool navMesh;
			bool navObstacle;
			// This atributes are loaded outside DotSceneLoaded (loaded in conf by entity extra loader
			bool cnv;
			std::shared_ptr<Extra> mInteractionData;
		};

		std::shared_ptr<EntityComp> mEntityComp;
		std::shared_ptr<Extra> mExtra;
		std::vector<std::shared_ptr<EntityComp>> mECS;
		std::vector<std::shared_ptr<Extra>> mExtraVector;

		std::shared_ptr<GraphicSystem> mGraphicSystem;
		std::shared_ptr<PhysicSystem> mPhysicsSystem;
		std::shared_ptr<MovementSystem> mMovementSystem;
		std::shared_ptr<CameraSystem> mCameraSystem;
		std::shared_ptr<AnimationSystem> mAnimationSystem;
		std::shared_ptr<AISystem> mAiSystem;

		std::shared_ptr<GameManager> _gameManager;
		std::shared_ptr<Configuration> _config;
		std::shared_ptr<EntityExtraLoader> _extra;

	};
}

#endif // DOT_SCENELOADER_H
