# Another shadow over Innsmouth #

## Autores ##

** Gonzalo Trigueros Manzanas **					gtriguerosmanzanas@gmail.com
** Jose Antonio Costa de Moya **					joseantonio.costa250@gmail.com
** David Delgado Lizcano **							daviddelgadolizcano@gmail.com

## Tecnologías ##

### Game Engine ###

** Motor gráfico **			Ogre3d  				http://www.ogre3d.org/
** Físicas **				OgreBullet (bullet)  	http://www.ogre3d.org/tikiwiki/OgreBullet
** I/O **					OIS  					http://www.ogre3d.org/tikiwiki/OIS
** GUI **					NOESIS  				http://www.noesisengine.com
** Sonido **				SDL						https://www.libsdl.org/download-2.0.php
** AI **					OgreCrowd 	 			http://www.ogre3d.org/forums/viewtopic.php?f=11&t=69781

## Recursos ##

** Modelos **	
	
Escenario: BlendSwap 		(eracoon)  				https://www.blendswap.com/blends/view/75021
Decoración de escenario: 	Quaternius 				http://quaternius.com/
Modelos: blendSwap 			(PigArt) 				https://www.blendswap.com/blends/view/71843
	
** Música y efectos **		FreeSound				https://www.freesound.org/

- Música día: 				TheBoseDeity  			http://freesound.org/people/TheBoseDeity/sounds/395143/
- Música tarde: 			Burning-mir  			https://freesound.org/people/burning-mir/sounds/155142/
- Música noche:  			Dneproman   			http://freesound.org/people/Dneproman/sounds/234343/
- Efecto de lluvia: 		Bzakirasantjago  		http://freesound.org/people/bzakirasantjago/sounds/167210/
- Efecto de gruñidos: 		Missozzy  				http://freesound.org/people/missozzy/sounds/169985/

** Imagenes **
Imagen de introducción: 	Deviantart (hplhu)  		http://hplhu.deviantart.com/art/Shadow-over-Innsmouth-HD-Wallpaper-341312712
 